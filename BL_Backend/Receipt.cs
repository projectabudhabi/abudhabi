﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Backend
{
    [Serializable]
    public class Receipt
    {
        public int ProductID { get; set; }
        public int Amount { get; set; }
        public double Price { get; set; }
        
        public Receipt(int _productId, int _amount, double _price)
        {
            ProductID = _productId;
            Price = _price;
            Amount =_amount;
        }

        public Receipt()
        {
            ProductID = -1;
            Amount = 0;
            Price = -1;
        }

        public override String ToString()
        {
            return "Product: " + ProductID + ", Price: " + Price + " X" + Amount;
        }

    }
}
