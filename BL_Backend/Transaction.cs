﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Backend
{
    [Serializable]
   public class Transaction
    {
        
        public enum payment
        {
            CreditCard,
            Check,
            Cash
        }

        public int ID { get; set; }
        public DateTime Date { get; set; }
        public bool Is_a_return { get; set; }
        public payment PaymentMethod { get; set; }
        public List<Receipt> ReceiptList { get; set; }
        public double Value { get; set; }

        public Transaction(int _id, bool _is_a_return, String _paymentMethod, DateTime _d)
        {
            ReceiptList = new List<Receipt>();
            Value = 0;
            Is_a_return = _is_a_return;
            switch (_paymentMethod)
            {
                case "Credit Card":
                    PaymentMethod = payment.CreditCard;
                    break;
                case "Cash":
                    PaymentMethod = payment.Cash;
                    break;
                case "Check":
                    PaymentMethod = payment.Check;
                    break;
                default:
                    throw new ArgumentException("Payment Method must be Credit Card or Cash!");
            }
            ID = _id;
            Date = _d;
        }

        public Transaction()
        {
            ID = -1;
            Date = DateTime.Now;
            Is_a_return = false;
            PaymentMethod = payment.Cash; 
            ReceiptList = new List<Receipt>();
        }


        public void addToReceipt(int _id, int _amount, double _price)
        {
            bool alreadyIn = false;
            foreach(Receipt r in ReceiptList)
            {
                if (r.ProductID == _id)
                {
                    r.Amount=r.Amount + _amount;
                    alreadyIn = true;
                }
            }
            if (!alreadyIn)
                ReceiptList.Add(new Receipt(_id, _amount, _price));
            Value = Value + _amount * _price;
        }

        public bool removeFromReceipt(int _id, int _amount)
        {
            for (int i = ReceiptList.Count - 1; i >= 0;i=i-1 )
            {
                if (ReceiptList[i].ProductID == _id && ReceiptList[i].Amount>=_amount)
                {
                    ReceiptList[i].Amount=ReceiptList[i].Amount - _amount;
                    Value = Value - ReceiptList[i].Price * _amount;
                    if(ReceiptList[i].Amount==0)
                    ReceiptList.RemoveAt(i);
                    return true;
                }
            }
            return false;
        }

        public bool removeFromReceipt(int _id)
        {
            for (int i = ReceiptList.Count - 1; i >= 0; i = i - 1)
            {
                if (ReceiptList[i].ProductID == _id)
                {
                    Value = Value - ReceiptList[i].Price * ReceiptList[i].Amount;
                    ReceiptList.RemoveAt(i);
                    return true;
                }
            }
            return false;
        }

        public String GetReceiptAsString()
        {
            String str = "";
            foreach(Receipt r in ReceiptList)
            {
                str = str + r.ToString()+"\n";
            }
            str = str + "\nTotal: " + Value;
            return str;
        }

        public String GetPaymentMethod()
        {
            if (PaymentMethod == payment.Cash) return "Cash";
            if (PaymentMethod == payment.Check) return "Check";
            return "Credit Card";
        }

        public void SetPaymentMethod(String _payment)
        {
            switch (_payment)
            {
                case "Credit Card":
                    PaymentMethod = payment.CreditCard;
                    break;
                case "Cash":
                    PaymentMethod = payment.Cash;
                    break;
                case "Check":
                    PaymentMethod = payment.Check;
                    break;
                default:
                    throw new ArgumentException("Payment Method must be Credit Card or Cash!");
            }
        }

        public override bool Equals(Object o)
        {
            if (o == null || !(o is Transaction)) return false;
            return ((Transaction)o).ID==ID;
        }

        public override String ToString()
        {
            String st = "ID: "+ID + ", " + Date.ToString("MM'/'dd'/'yyyy HH':'mm':'ss.fff") + ": \n" + GetReceiptAsString() + "\n";
            if (Is_a_return) st = st + "retrun ";
            else st = st + "purchase ";
            st = st + "via " + PaymentMethod +"\n";
            return st;
        }
    }
}
