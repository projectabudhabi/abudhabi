﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Backend
{
    [Serializable]
    public class Department
    {
        public int ID { get; set; }
        public String Name { get; set; }

        public Department(int _id, String _name)
        {
            ID = _id;
            Name = _name;
        }

        public Department()
        {
            ID = -1;
            Name = "Name";
        }

        public override bool Equals(Object o)
        {
            if (o == null || !(o is Department)) return false;
            return ((Department)o).ID == ID;
        }

        public override String ToString()
        {
            return "ID: "+ ID + ", " + Name;
        }
    }
}
