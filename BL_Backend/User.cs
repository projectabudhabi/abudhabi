﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Backend
{
    [Serializable]
    public class User
    {
        public enum gender
        {
            Male,
            Female
        }

        public String Username { get; set; }
        public String Password { get; set; }
        public String ID { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public gender Gender { get; set; }
        public DateTime Birthday { get; set; }

        public User(String _username, String _password, String _id, String _firstName, String _lastName, String _gender, DateTime _birthday)
        {
            Username = _username;
            Password = _password;
            ID = _id;
            FirstName = _firstName;
            LastName = _lastName;
            Birthday = _birthday;
            switch (_gender)
            {
                case "Male":
                    Gender = gender.Male;
                    break;
                case "Female":
                    Gender = gender.Female;
                    break;
                default:
                    throw new ArgumentException("Gender must be Male or Female!");
            }
        }

        public User()
        {
            Username = "";
            Password = "";
            ID = "";
            FirstName = "";
            LastName = "";
            Gender = gender.Male;
        }

        public String GetGender()
        {
            if (Gender==gender.Female) return "Female";
            return "Male";
        }

        public void SetGender(String _gender)
        {
            switch (_gender)
            {
                case "Male":
                    Gender = gender.Male;
                    break;
                case "Female":
                    Gender = gender.Female;
                    break;
                default:
                    throw new ArgumentException("Gender must be Male or Female!");
            }
        }

        public override bool Equals(Object o)
        {
            if (o == null || !(o is User)) return false;
            if (((User)o).ID == null) return false;
            return ID.Equals(((User)o).ID);
        }

        public override String ToString()
        {
            return Username + ", " + Password + ", "+ ID+", "+ FirstName +", "+LastName+", "+GetGender();
        }
    }
}
