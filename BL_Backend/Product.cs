﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Backend
{
    [Serializable]
    public class Product
    {

        public enum productType
        {
            Food,
            Electronics,
            Clothes,
            Cosmetics,
            Toys,
            Other
        }

        public int ProductID { get; set; }
        public string Name { get; set; }
        public productType ProType { get; set; }
        public int InStock { get; set; }
        public int LocationID { get; set; }
        public double Price { get; set; }

        public Product(int _id, string _name, string _type, int _locationID, int _inStock, double _price)
        {
            Name = _name;
            LocationID = _locationID;
            Price = _price;
            switch (_type)
            {
                case "Food":
                    ProType = productType.Food;
                    break;
                case "Electronics":
                    ProType = productType.Electronics;
                    break;
                case "Clothes":
                    ProType = productType.Clothes;
                    break;
                case "Cosmetics":
                    ProType = productType.Cosmetics;
                    break;
                case "Toys":
                    ProType = productType.Toys;
                    break;
                default:
                    ProType = productType.Other;
                    break;
            }
            ProductID = _id;
            InStock = _inStock;
        }

        public Product()
        {
            Name = "Name";
            ProductID = -1;
            ProType = productType.Food;
            InStock = -1;
            LocationID = -1;
            Price = -1;
        }

        public String GetProType()
        {
            if (ProType == productType.Food) return "Food";
            if (ProType == productType.Electronics) return "Electronics";
            if (ProType == productType.Toys) return "Toys";
            if (ProType == productType.Clothes) return "Clothes";
            if (ProType == productType.Cosmetics) return "Cosmetics";
            return "Other";
        }

        public void SetProType(String _type)
        {
            if (_type.Equals("Food")) ProType = productType.Food;
            if (_type.Equals("Electronics")) ProType = productType.Electronics;
            if (_type.Equals("Toys")) ProType = productType.Toys;
            if (_type.Equals("Clothes")) ProType = productType.Clothes;
            if (_type.Equals("Cosmetics")) ProType = productType.Cosmetics;
            if (_type.Equals("Other")) ProType = productType.Other;
        }

        public override bool Equals(Object o)
        {
            if (o == null || !(o is Product)) return false;
            return ((Product)o).ProductID == ProductID;
        }

        public override String ToString()
        {
            return "ID: " + ProductID + ", " + Name + ", Type: " + ProType + ", Location: " + LocationID + ", Price: " + Price +", " + InStock + " in stock";
        }

    }
}
