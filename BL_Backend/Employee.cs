﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Backend
{
    [Serializable]
    public class Employee : User
    {

        public enum employeeType
        {
            Worker,
            Manager,
            Administrator
        }

        public int EmpID { get; set; }
        public employeeType Type { get; set; }
        public int DepartmentID { get; set; }
        public int Salary { get; set; }
        public String Supervisor { get; set; }

        public Employee(int _empID, String _type, String _username, String _password, String _id, String _firstName, String _lastName, String _gender, int _departmentID, int _salary, String _supervisor, DateTime _birthday)
            : base(_username,_password, _id, _firstName, _lastName, _gender,_birthday)
        {
            EmpID = _empID;
            DepartmentID = _departmentID;
            Salary = _salary;
            Supervisor = _supervisor;
            switch (_type)
            {
                case "Worker":
                    Type = employeeType.Worker;
                    break;
                case "Manager":
                    Type = employeeType.Manager;
                    break;
                case "Administrator":
                    Type = employeeType.Administrator;
                    break;
            }
        }

        public Employee() : base()
        {
            Salary = 0;
            DepartmentID = -1;
            Supervisor = "";
            Type = employeeType.Worker;
        }

        public String GetType()
        {
            if (Type == employeeType.Administrator) return "Administrator";
            if (Type == employeeType.Manager) return "Manager";
            return "Worker";
        }

        public void SetType(String _type)
        {
            if (_type.Equals("Administrator")) Type = employeeType.Administrator;
            if (_type.Equals("Manager")) Type = employeeType.Manager;
            if (_type.Equals("Worker")) Type = employeeType.Worker;
        }

        public override String ToString()
        {
            return EmpID+": "+base.ToString() + ", Department ID: " + DepartmentID + ", Salary: " + Salary + " Supervisor ID: " + Supervisor;
        }
    }
}
