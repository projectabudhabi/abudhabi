﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Backend
{
    [Serializable]
    public class ClubMember : User
    {

        public int MemberID { get; set; }
        public String CreditCard { get; set; }
        public List<int> Transactions { get; set; }


        public ClubMember(int _cusID, String username, String password, String _id, String _firstName, String _lastName, String _gender, String _creditCard, DateTime _birthday, List<int> _tran)
        : base(username, password, _id,_firstName,_lastName,_gender, _birthday)
        {
            CreditCard = _creditCard;
            MemberID = _cusID;
            Birthday = _birthday;
            Transactions = _tran; 
        }

        public ClubMember()
            : base()
        {
            CreditCard = "";
            Birthday = DateTime.Today;
            Transactions = new List<int>();
            MemberID = -1;
        }

        public override String ToString()
        {
            String st = MemberID+": "+ base.ToString() + ", "+ Birthday.ToString("dd'/'MM'/'yyyy") + ",\n Transactions: ";
            foreach (int num in Transactions)
                st = st + num + " ";
            return st;
        }
    }
}
