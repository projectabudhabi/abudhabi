﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Backend;
using BL;

namespace PL_GUI
{
    /// <summary>
    /// Interaction logic for Page1.xaml
    /// </summary>
    public partial class SeeDepartments : Page
    {

        MainFrame w;
        List<Department> sourceList;

        public SeeDepartments(MainFrame _w)
        {
            w = _w;
            sourceList = w.itsDepartmentBL.GetAll();
            InitializeComponent();
            SearchMethod.ItemsSource =  new String[] { "See All", "Search by ID", "Search by Name" };
            SearchMethod.SelectedIndex = 0;
            if (w.activeUser is Employee && ((Employee)(w.activeUser)).GetType().Equals("Worker"))
                add.IsEnabled = false;
        }

        private void DataGrid_Loaded(object sender, RoutedEventArgs e)
        {
            departments.ItemsSource = sourceList;
        }

        private void Back_Click(object sender, RoutedEventArgs e)
        {
            w.MainMenu();
        }

        private void Add_Click(object sender, RoutedEventArgs e)
        {
            w.AddDepartment();
        }

        private void DataSelectionChanged(object sender, EventArgs e)
        {
            if (departments.SelectedItem != null && w.activeUser is Employee && !(((Employee)(w.activeUser)).GetType().Equals("Worker")))
            {
                edit.Visibility = Visibility.Visible;
                delete.Visibility = Visibility.Visible;
            }
            else
            {
                edit.Visibility = Visibility.Hidden;
                delete.Visibility = Visibility.Hidden;
            }
            
        }

        private void Delete_Click(object sender, RoutedEventArgs e)
        {
            Department temp = (Department)(departments.SelectedItem);
            if (w.itsDepartmentBL.Delete(temp.ID))
            {
                MessageBoxResult r = MessageBox.Show("Department has been deleted!", "Success", MessageBoxButton.OK);
                if (r == MessageBoxResult.OK)
                {
                    sourceList = w.itsDepartmentBL.GetAll();
                    departments.Items.Refresh();
                    SearchMethod.SelectedIndex = 0;
                }
                    
            }
            else
                MessageBox.Show("Department cannot be deleted!", "Success", MessageBoxButton.OK);
        }

        private void Edit_Click(object sender, RoutedEventArgs e)
        {
            Department temp = (Department)(departments.SelectedItem);
            if (temp != null && w.itsDepartmentBL.Exist(temp.ID))
                w.EditDepartment(temp);
            else
                MessageBox.Show("Department does not exist!", "Success", MessageBoxButton.OK);
        }

        private void Search_Click(object sender, RoutedEventArgs e)
        {
            if (SearchMethod.SelectedIndex == 1)
            {
                int idtosearch;
                bool isint = int.TryParse(toSearch.Text, out idtosearch);
                if (isint)
                {
                    departments.ItemsSource =  w.itsDepartmentBL.SearchByID(idtosearch);
                }
                else
                    MessageBox.Show("Invalid input!", "Success", MessageBoxButton.OK);
            }
            if (SearchMethod.SelectedIndex == 2)
            {
                departments.ItemsSource = w.itsDepartmentBL.SearchByName(toSearch.Text);
            }
            departments.Items.Refresh();
            toSearch.Text = "";
        }

        private void SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            toSearch.Text = "";
            if(SearchMethod.SelectedIndex==0)
            {
                departments.ItemsSource = sourceList;
                departments.Items.Refresh();
                toSearch.Visibility = Visibility.Hidden;
                Search.Visibility = Visibility.Hidden;
            }

            if (SearchMethod.SelectedIndex == 1 || SearchMethod.SelectedIndex == 2)
            {
                toSearch.Visibility = Visibility.Visible;
                Search.Visibility = Visibility.Visible;
            }
        }

        private void logout_Click(object sender, RoutedEventArgs e)
        {
            w.Login();
        }

        private void home_Click(object sender, RoutedEventArgs e)
        {
            w.MainMenu();
        }

    }
}
