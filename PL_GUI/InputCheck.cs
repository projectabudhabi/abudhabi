﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PL_GUI
{
    public class InputCheck
    {
        public static bool isID(String s)
        {
            if (s.Length < 9)
                return false;
            foreach(char c in s)
            {
                if (!Char.IsDigit(c))
                    return false;
            }
            return true;
        }

        public static bool isCreditCard(String s)
        {
            if (s.Length < 16)
                return false;
            foreach (char c in s)
            {
                if (!Char.IsDigit(c))
                    return false;
            }
            return true;
        }

        public static bool isValidUser(String s)
        {
            if (s.Length < 4)
                return false;
            foreach (char c in s)
            {
                if (c==' ')
                    return false;
            }
            return true;
        }

        public static bool isValidName(String s)
        {
            if (s.Length == 0)
                return false;
            if (s[0] == ' ' || s[s.Length - 1] == ' ')
                return false;
            foreach (char c in s)
            {
                if (!Char.IsLetter(c) && c!=' ')
                    return false;
            }
            return true;
        }

        public static bool isValidProductName(String s)
        {
            if (s.Length == 0)
                return false;
            if (s[0] == ' ' || s[s.Length - 1] == ' ')
                return false;
            return true;
        }

        public static int changeToInt(String s)
        {
            int num = -1;
            int.TryParse(s, out num);
            return num;
        }

        public static double changeToDouble(String s)
        {
            double num = -1.0;
            double.TryParse(s, out num);
            return num;
        }
    }
}
