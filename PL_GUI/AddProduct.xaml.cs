﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Backend;
using BL;

namespace PL_GUI
{
    /// <summary>
    /// Interaction logic for Page1.xaml
    /// </summary>
    public partial class AddProduct : Page
    {

        MainFrame w;
        List<Department> departments;

        public AddProduct(MainFrame _w)
        {
            InitializeComponent();
            w = _w;
            String[] types = new String[] { "Food", "Electronics", "Clothes", "Cosmetics", "Toys", "Other" };
            TypeComboBox.ItemsSource = types;
            TypeComboBox.SelectedIndex = 0;
            departments = w.itsDepartmentBL.GetAll();
            String[] departmentsSource = new String[departments.Count];
            for (int i = 0; i < departments.Count; i++)
            {
                departmentsSource[i] = departments.ElementAt(i).Name;
            }
            DepartmentComboBox.ItemsSource = departmentsSource;
            DepartmentComboBox.SelectedIndex = 0;
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void TextBox_TextChanged_1(object sender, TextChangedEventArgs e)
        {

        }

        private void Back_Click(object sender, RoutedEventArgs e)
        {
            w.SeeProducts();
        }


        private void add_Click(object sender, RoutedEventArgs e)
        {
            String ans = "";
            int amount = InputCheck.changeToInt(amounttxt.Text);
            double price = InputCheck.changeToDouble(pricetxt.Text);
            int dep;
            if (!InputCheck.isValidProductName(name.Text))
                ans = ans + "Product name is illegal.\n";

            if (amount <= 0)
                ans = ans + "Amount is illegal.\n";
            if (price <= 0)
                ans = ans + "Price is illegal.\n";
            dep = departments.ElementAt(DepartmentComboBox.SelectedIndex).ID;
            if(ans.Length==0)
            {
                ans = w.itsProductBL.Add(name.Text, ""+TypeComboBox.SelectedItem, dep, amount, price);
                
                if (ans.Length == 0)
                {
                    MessageBoxResult result;
                    result = MessageBox.Show("Product added successfully", "Success", MessageBoxButton.OK);
                    if (result == MessageBoxResult.OK)
                    {
                        w.SeeProducts();
                    }
                }
                else
                    MessageBox.Show(ans, "Error", MessageBoxButton.OK);
            }
            else
                MessageBox.Show(ans, "Error", MessageBoxButton.OK);

        }

        private void logout_Click(object sender, RoutedEventArgs e)
        {
            w.Login();
        }

        private void home_Click(object sender, RoutedEventArgs e)
        {
            w.MainMenu();
        }

    }
}
