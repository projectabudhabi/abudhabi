﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Backend;
using BL;

namespace PL_GUI
{
    /// <summary>
    /// Interaction logic for Page1.xaml
    /// </summary>
    public partial class Login : Page
    {
        private MainFrame w;

        public Login(MainFrame _w)
        {
            InitializeComponent();
            w = _w;
            w.activeUser = null;
            w.shopingcart = new List<Receipt>();
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void TextBox_TextChanged_1(object sender, TextChangedEventArgs e)
        {

        }

        private void guest_Click(object sender, RoutedEventArgs e)
        {
            w.activeUser = null;
            w.MainMenu();
        }

        private void loginB_Click(object sender, RoutedEventArgs e)
        {
            w.activeUser = null;
            errors.Content = "";
            ClubMember cm = w.itsClubMemberBL.GetByUser(username.Text, password.Password);
            if (cm != null)
                w.activeUser = cm;
            else
            {
                    Employee em = w.itsEmployeeBL.GetByUser(username.Text, password.Password);
                    if (em != null)
                        w.activeUser = em;
                
            }
            if (w.activeUser != null)
                w.MainMenu();
            else
                errors.Content = "Wrong username or password.";
        }

        private void signup_Click(object sender, RoutedEventArgs e)
        {
            w.SignUp();
        }

    }
}
