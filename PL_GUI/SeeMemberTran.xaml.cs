﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Backend;
using BL;

namespace PL_GUI
{
    /// <summary>
    /// Interaction logic for Page1.xaml
    /// </summary>
    public partial class SeeMemberTran : Page
    {

        MainFrame w;
        ClubMember c;
        List<Transaction> sourceList;
        List<Transaction> tempo;
        int fromPage;

        public SeeMemberTran(MainFrame _w, ClubMember _c, int _fromPage)
        {
            fromPage = _fromPage;
            w = _w;
            c = _c;
            InitializeComponent();
            if(c!=null)
            {
                Headline.Content = "See " + c.Username + "'s Transactions";            }
            else
            {
                Headline.Content = "See shop Transactions";
            }
            List<String> com = new List<string>();
            com.Add("All");
            com.Add("Between dates");
            com.Add("By ID");
            com.Add("By Payment Method");
            if (c == null)
                com.Add("By User name");
            serchmethod.ItemsSource = com;
            serchmethod.SelectedItem = "All";
            List<String> clubn = new List<string>();
            foreach (ClubMember cl in w.itsClubMemberBL.GetAll())
            {
                clubn.Add(cl.Username);
            }
            clubmem.ItemsSource = clubn;
            clubmem.SelectedIndex = 0;
            todate.SelectedDate = DateTime.Today;
            fromdate.SelectedDate = DateTime.Today;
            List<String> pay1 = new List<String>();
            pay1.Add("Credit Card");
            pay1.Add("Check");
            pay1.Add("Cash");
            paymentme.ItemsSource = pay1;
            paymentme.SelectedIndex = 1;
        }
        private void inalisesource()
        {
            if (c != null)
            {
                sourceList = w.itsTransactionBL.GetMemberTran(w.itsTransactionBL.GetAll(),c);
                tempo = w.itsTransactionBL.GetMemberTran(w.itsTransactionBL.GetAll(),c);
            }
                
            else
            {
                sourceList = w.itsTransactionBL.GetAll();
                tempo = w.itsTransactionBL.GetAll();
            }
                
            transactions.ItemsSource = from s in tempo select new { ID = s.ID, Date = s.Date, PaymentMethod = s.PaymentMethod, Value = s.Value, IsAReturn = s.Is_a_return };
        }
        private void serch_SekectedItemChenge(object sender, RoutedEventArgs e)
        {
            String sol = (String)serchmethod.SelectedItem;
            switch (sol){
                case "All":
                    todate.Visibility = Visibility.Hidden;
                    to.Visibility = Visibility.Hidden;
                    from.Visibility = Visibility.Hidden;
                    fromdate.Visibility = Visibility.Hidden;
                    idtext.Visibility = Visibility.Hidden;
                    paymentme.Visibility = Visibility.Hidden;
                    clubmem.Visibility = Visibility.Hidden;
                    search.Visibility = Visibility.Hidden;
                    inalisesource();
                    break;
                case "Between dates":
                    todate.Visibility = Visibility.Visible;
                    to.Visibility = Visibility.Visible;
                    from.Visibility = Visibility.Visible;
                    fromdate.Visibility = Visibility.Visible;
                    idtext.Visibility = Visibility.Hidden;
                    paymentme.Visibility = Visibility.Hidden;
                    clubmem.Visibility = Visibility.Hidden;
                    search.Visibility = Visibility.Visible;

                    break;
                case "By ID":
                    clubmem.Visibility = Visibility.Hidden;
                    paymentme.Visibility = Visibility.Hidden;
                    todate.Visibility = Visibility.Hidden;
                    to.Visibility = Visibility.Hidden;
                    from.Visibility = Visibility.Hidden;
                    fromdate.Visibility = Visibility.Hidden;
                    idtext.Visibility = Visibility.Visible;
                    search.Visibility = Visibility.Visible;
                    break;
                case "By Payment Method":
                    idtext.Visibility = Visibility.Hidden;
                    clubmem.Visibility = Visibility.Hidden;
                    todate.Visibility = Visibility.Hidden;
                    to.Visibility = Visibility.Hidden;
                    from.Visibility = Visibility.Hidden;
                    fromdate.Visibility = Visibility.Hidden;
                    paymentme.Visibility = Visibility.Visible;
                    search.Visibility = Visibility.Hidden;
                    tempo = w.itsTransactionBL.SearchByPaymentMethod(sourceList, (String)paymentme.SelectedItem);
                    transactions.ItemsSource = from s in tempo select new { ID = s.ID, Date = s.Date, PaymentMethod = s.PaymentMethod, Value = s.Value, IsAReturn = s.Is_a_return };
                    break;
                case "By User name":
                    idtext.Visibility = Visibility.Hidden;
                    paymentme.Visibility = Visibility.Hidden;
                    todate.Visibility = Visibility.Hidden;
                    to.Visibility = Visibility.Hidden;
                    from.Visibility = Visibility.Hidden;
                    fromdate.Visibility = Visibility.Hidden;
                    clubmem.Visibility = Visibility.Visible;
                    search.Visibility = Visibility.Hidden;
                    userSelectionChanged1();
                    break;

            }
        }
        private void DataGrid_Loaded(object sender, RoutedEventArgs e)
        {
            inalisesource();
        }

        private void Back_Click(object sender, RoutedEventArgs e)
        {
            if (fromPage==1)
                w.MainMenu();
            if (fromPage == 2)
                w.SeeClubMembers();
            if (fromPage == 3)
                w.EditClubMembers(c, true);
            if(fromPage==4)
                w.EditClubMembers(c, false);
        }
        private void userSelectionChanged(object sender, EventArgs e)
        {
            userSelectionChanged1();
        }
        private void userSelectionChanged1()
        {
            foreach (ClubMember clu in w.itsClubMemberBL.GetAll())
            {
                if (clu.Username.Equals((String)clubmem.SelectedItem))
                {
                    tempo = w.itsTransactionBL.GetMemberTran(sourceList, clu);
                    transactions.ItemsSource = from s in tempo select new { ID = s.ID, Date = s.Date, PaymentMethod = s.PaymentMethod, Value = s.Value, IsAReturn = s.Is_a_return };
                }
            }
        }
        private void paySelectionChanged(object sender, EventArgs e)
        {
            tempo = w.itsTransactionBL.SearchByPaymentMethod(sourceList, (String)paymentme.SelectedItem);
            transactions.ItemsSource = from s in tempo select new { ID = s.ID, Date = s.Date, PaymentMethod = s.PaymentMethod, Value = s.Value, IsAReturn = s.Is_a_return };
        }
        private void DataSelectionChanged(object sender, EventArgs e)
        {
            if(transactions.SelectedItem!=null)
            {
                List<Product> allPro = w.itsProductBL.GetAll();
                List<Receipt> itsReceipt = (tempo[transactions.SelectedIndex]).ReceiptList;
                receipts.ItemsSource = from Pro in allPro join Rec in itsReceipt on Pro.ProductID equals Rec.ProductID select new { Product = Pro.Name, Amount = Rec.Amount, Price = Rec.Price }; 

                receipts.Items.Refresh();
                if (w.activeUser is Employee && !(((Employee)(w.activeUser)).GetType().Equals("Worker")))
                {
                    edit.Visibility = Visibility.Visible;
                    delete.Visibility = Visibility.Visible;
                }   
            }
            else
            {

                List<Product> allPro = w.itsProductBL.GetAll();
                List<Receipt> itsReceipt = new List<Receipt>();
                receipts.ItemsSource = from Pro in allPro join Rec in itsReceipt on Pro.ProductID equals Rec.ProductID select new { Product = Pro.Name, Amount = Rec.Amount, Price = Rec.Price };

                receipts.Items.Refresh();
            }
            
        }

        private void search_Click(object sender, RoutedEventArgs e)
        {
            if(todate.IsVisible)
            {

                DateTime tod = (DateTime)(todate.SelectedDate);
                tod = new DateTime(tod.Year, tod.Month, tod.Day, 23, 59, 59);
                DateTime fromd = (DateTime)(fromdate.SelectedDate);
                tod = new DateTime(tod.Year, tod.Month, tod.Day, 0, 0, 0);
                tempo = w.itsTransactionBL.SearchBetween(sourceList, fromd, tod);
                transactions.ItemsSource = from s in tempo select new { ID = s.ID, Date = s.Date, PaymentMethod = s.PaymentMethod, Value = s.Value, IsAReturn = s.Is_a_return };
            }
            if (idtext.IsVisible)
            {
                int id=-1;
                if (int.TryParse((String)idtext.Text, out id))
                {
                    tempo = w.itsTransactionBL.SearchByID(sourceList, id);
                    transactions.ItemsSource = from s in tempo select new { ID = s.ID, Date = s.Date, PaymentMethod = s.PaymentMethod, Value = s.Value, IsAReturn = s.Is_a_return };
                }
                else
                    MessageBox.Show("Enter only numbers!!", "Error", MessageBoxButton.OK);
                    
            }
        }

        private void Edit_Click(object sender, RoutedEventArgs e)
        {
            Transaction temp = tempo[transactions.SelectedIndex];
            if (temp != null && w.itsTransactionBL.Exist(temp.ID))
                w.EditTransaction(temp, c ,fromPage);
            else
                MessageBox.Show("Transaction does not exist!", "Success", MessageBoxButton.OK);
        }

        private void Delete_Click(object sender, RoutedEventArgs e)
        {
            Transaction temp = tempo[transactions.SelectedIndex];
            if (w.itsTransactionBL.Delete(temp.ID))
            {
                MessageBoxResult r = MessageBox.Show("Transaction has been deleted!", "Success", MessageBoxButton.OK);
                if (r == MessageBoxResult.OK)
                {
                    inalisesource();
                    transactions.Items.Refresh();
                    serchmethod.SelectedIndex = 0;

                }
            }
            else
                MessageBox.Show("Transaction cannot be deleted!", "Success", MessageBoxButton.OK);
        }

        private void logout_Click(object sender, RoutedEventArgs e)
        {
            w.Login();
        }

        private void home_Click(object sender, RoutedEventArgs e)
        {
            w.MainMenu();
        }

    }
}
