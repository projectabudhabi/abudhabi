﻿#pragma checksum "..\..\ShowClubMember.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "0D7A3F5B8C2B9275DDC5FE46EDA52C67"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34014
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace PL_GUI {
    
    
    /// <summary>
    /// ShowClubMember
    /// </summary>
    public partial class ShowClubMember : System.Windows.Controls.Page, System.Windows.Markup.IComponentConnector {
        
        
        #line 13 "..\..\ShowClubMember.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label Headline;
        
        #line default
        #line hidden
        
        
        #line 19 "..\..\ShowClubMember.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox fname;
        
        #line default
        #line hidden
        
        
        #line 20 "..\..\ShowClubMember.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox lname;
        
        #line default
        #line hidden
        
        
        #line 21 "..\..\ShowClubMember.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox username;
        
        #line default
        #line hidden
        
        
        #line 22 "..\..\ShowClubMember.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox password;
        
        #line default
        #line hidden
        
        
        #line 23 "..\..\ShowClubMember.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton male;
        
        #line default
        #line hidden
        
        
        #line 24 "..\..\ShowClubMember.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton female;
        
        #line default
        #line hidden
        
        
        #line 25 "..\..\ShowClubMember.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox id;
        
        #line default
        #line hidden
        
        
        #line 27 "..\..\ShowClubMember.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox bdayDay;
        
        #line default
        #line hidden
        
        
        #line 28 "..\..\ShowClubMember.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox bdayMonth;
        
        #line default
        #line hidden
        
        
        #line 29 "..\..\ShowClubMember.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox bdayYear;
        
        #line default
        #line hidden
        
        
        #line 31 "..\..\ShowClubMember.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox creditCardChecked;
        
        #line default
        #line hidden
        
        
        #line 32 "..\..\ShowClubMember.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label cardNumLabel;
        
        #line default
        #line hidden
        
        
        #line 33 "..\..\ShowClubMember.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox cardNum;
        
        #line default
        #line hidden
        
        
        #line 34 "..\..\ShowClubMember.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button add;
        
        #line default
        #line hidden
        
        
        #line 35 "..\..\ShowClubMember.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button back;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/WpfControlLibrary1;component/showclubmember.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\ShowClubMember.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.Headline = ((System.Windows.Controls.Label)(target));
            return;
            case 2:
            this.fname = ((System.Windows.Controls.TextBox)(target));
            
            #line 19 "..\..\ShowClubMember.xaml"
            this.fname.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.TextBox_TextChanged);
            
            #line default
            #line hidden
            return;
            case 3:
            this.lname = ((System.Windows.Controls.TextBox)(target));
            
            #line 20 "..\..\ShowClubMember.xaml"
            this.lname.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.TextBox_TextChanged);
            
            #line default
            #line hidden
            return;
            case 4:
            this.username = ((System.Windows.Controls.TextBox)(target));
            
            #line 21 "..\..\ShowClubMember.xaml"
            this.username.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.TextBox_TextChanged);
            
            #line default
            #line hidden
            return;
            case 5:
            this.password = ((System.Windows.Controls.TextBox)(target));
            
            #line 22 "..\..\ShowClubMember.xaml"
            this.password.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.TextBox_TextChanged);
            
            #line default
            #line hidden
            return;
            case 6:
            this.male = ((System.Windows.Controls.RadioButton)(target));
            return;
            case 7:
            this.female = ((System.Windows.Controls.RadioButton)(target));
            return;
            case 8:
            this.id = ((System.Windows.Controls.TextBox)(target));
            
            #line 25 "..\..\ShowClubMember.xaml"
            this.id.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.TextBox_TextChanged);
            
            #line default
            #line hidden
            return;
            case 9:
            this.bdayDay = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 10:
            this.bdayMonth = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 11:
            this.bdayYear = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 12:
            this.creditCardChecked = ((System.Windows.Controls.CheckBox)(target));
            
            #line 31 "..\..\ShowClubMember.xaml"
            this.creditCardChecked.Checked += new System.Windows.RoutedEventHandler(this.creditCardChecked_Checked);
            
            #line default
            #line hidden
            
            #line 31 "..\..\ShowClubMember.xaml"
            this.creditCardChecked.Unchecked += new System.Windows.RoutedEventHandler(this.creditCardChecked_UnChecked);
            
            #line default
            #line hidden
            return;
            case 13:
            this.cardNumLabel = ((System.Windows.Controls.Label)(target));
            return;
            case 14:
            this.cardNum = ((System.Windows.Controls.TextBox)(target));
            
            #line 33 "..\..\ShowClubMember.xaml"
            this.cardNum.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.TextBox_TextChanged);
            
            #line default
            #line hidden
            return;
            case 15:
            this.add = ((System.Windows.Controls.Button)(target));
            
            #line 34 "..\..\ShowClubMember.xaml"
            this.add.Click += new System.Windows.RoutedEventHandler(this.add_Click);
            
            #line default
            #line hidden
            return;
            case 16:
            this.back = ((System.Windows.Controls.Button)(target));
            
            #line 35 "..\..\ShowClubMember.xaml"
            this.back.Click += new System.Windows.RoutedEventHandler(this.Back_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

