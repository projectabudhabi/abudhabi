﻿#pragma checksum "..\..\SeeOurStores.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "ECA0E846E6C2763BE1874FF8A4FEBDA0"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34014
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace PL_GUI {
    
    
    /// <summary>
    /// SeeOurStores
    /// </summary>
    public partial class SeeOurStores : System.Windows.Controls.Page, System.Windows.Markup.IComponentConnector {
        
        
        #line 17 "..\..\SeeOurStores.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label Headline;
        
        #line default
        #line hidden
        
        
        #line 20 "..\..\SeeOurStores.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label from;
        
        #line default
        #line hidden
        
        
        #line 21 "..\..\SeeOurStores.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label to;
        
        #line default
        #line hidden
        
        
        #line 22 "..\..\SeeOurStores.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox idtext;
        
        #line default
        #line hidden
        
        
        #line 24 "..\..\SeeOurStores.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox serchmethod;
        
        #line default
        #line hidden
        
        
        #line 25 "..\..\SeeOurStores.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.WebBrowser webBrowser1;
        
        #line default
        #line hidden
        
        
        #line 26 "..\..\SeeOurStores.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button logout;
        
        #line default
        #line hidden
        
        
        #line 31 "..\..\SeeOurStores.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button home;
        
        #line default
        #line hidden
        
        
        #line 37 "..\..\SeeOurStores.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label General2;
        
        #line default
        #line hidden
        
        
        #line 39 "..\..\SeeOurStores.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label Weather2;
        
        #line default
        #line hidden
        
        
        #line 41 "..\..\SeeOurStores.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label Humidity2;
        
        #line default
        #line hidden
        
        
        #line 43 "..\..\SeeOurStores.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label Pressure2;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/WpfControlLibrary1;component/seeourstores.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\SeeOurStores.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.Headline = ((System.Windows.Controls.Label)(target));
            return;
            case 2:
            this.from = ((System.Windows.Controls.Label)(target));
            return;
            case 3:
            this.to = ((System.Windows.Controls.Label)(target));
            return;
            case 4:
            this.idtext = ((System.Windows.Controls.TextBox)(target));
            return;
            case 5:
            this.serchmethod = ((System.Windows.Controls.ComboBox)(target));
            
            #line 24 "..\..\SeeOurStores.xaml"
            this.serchmethod.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.serch_SekectedItemChenge);
            
            #line default
            #line hidden
            return;
            case 6:
            this.webBrowser1 = ((System.Windows.Controls.WebBrowser)(target));
            return;
            case 7:
            this.logout = ((System.Windows.Controls.Button)(target));
            
            #line 26 "..\..\SeeOurStores.xaml"
            this.logout.Click += new System.Windows.RoutedEventHandler(this.logout_Click);
            
            #line default
            #line hidden
            return;
            case 8:
            this.home = ((System.Windows.Controls.Button)(target));
            
            #line 31 "..\..\SeeOurStores.xaml"
            this.home.Click += new System.Windows.RoutedEventHandler(this.home_Click);
            
            #line default
            #line hidden
            return;
            case 9:
            this.General2 = ((System.Windows.Controls.Label)(target));
            return;
            case 10:
            this.Weather2 = ((System.Windows.Controls.Label)(target));
            return;
            case 11:
            this.Humidity2 = ((System.Windows.Controls.Label)(target));
            return;
            case 12:
            this.Pressure2 = ((System.Windows.Controls.Label)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

