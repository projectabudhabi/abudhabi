﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PL_GUI
{
    /// <summary>
    /// Interaction logic for Page1.xaml
    /// </summary>
    public partial class SignUp : Page
    {

        MainFrame w;

        public SignUp(MainFrame _w)
        {
            InitializeComponent();
            w = _w;
            String[] days = new String[]{ "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31" };
            String[] months = new String[] { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };
            String[] years = new String[100];
            for(int i=0;i<100;i++)
            {
                int temp = DateTime.Today.Year - i;
                years[i] = ""+temp;
            }
            bdayDay.ItemsSource = days;
            bdayDay.SelectedIndex = 0;
            bdayMonth.ItemsSource = months;
            bdayMonth.SelectedIndex = 0;
            bdayYear.ItemsSource = years;
            bdayYear.SelectedIndex = 0;
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void TextBox_TextChanged_1(object sender, TextChangedEventArgs e)
        {

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            w.Login();
        }

        private void creditCardChecked_Checked(object sender, RoutedEventArgs e)
        {
            cardNum.Visibility = Visibility.Visible;
            cardNumLabel.Visibility = Visibility.Visible;

        }

        private void creditCardChecked_UnChecked(object sender, RoutedEventArgs e)
        {
            cardNum.Visibility = Visibility.Hidden;
            cardNumLabel.Visibility = Visibility.Hidden;

        }

        private void signup_Click(object sender, RoutedEventArgs e)
        {
            DateTime bday = DateTime.Today;
            String ans = "";
            String creditCard = "";
            String gender = "Male";
            if (!InputCheck.isValidName(fname.Text))
                ans = ans + "First name is illegal.\n";
            if (!InputCheck.isValidName(lname.Text))
                ans = ans + "Last name is illegal.\n";
            if (!InputCheck.isValidUser(username.Text))
                ans = ans + "Username is illegal.\n";
            if (!InputCheck.isValidUser(password.Text))
                ans = ans + "Password is illegal.\n";
            if (!InputCheck.isID(id.Text))
                ans = ans + "ID is illegal.\n";
            int day, year;
            int.TryParse("" + bdayDay.SelectedItem, out day);
            int.TryParse("" + bdayYear.SelectedItem, out year);
            int month = bdayMonth.SelectedIndex + 1;
            bday = new DateTime(year, month, day);
            if (bday >= DateTime.Today)
                ans = ans + "Birthday is illegal.\n";
            if (female.IsChecked == true)
                gender = "Female";
            if(cardNum.Visibility==Visibility.Visible)
            {
                creditCard = cardNum.Text;
                if (!InputCheck.isCreditCard(creditCard))
                    ans = ans + "Credit card num is illegal.\n";
            }
            if(ans.Length==0)
            {
                ans = w.itsClubMemberBL.Add(id.Text, fname.Text, lname.Text, username.Text, password.Text, gender, creditCard, bday);
                
                if (ans.Length == 0)
                {
                    MessageBoxResult result;
                    result = MessageBox.Show("Club member added successfully", "Success", MessageBoxButton.OK);
                    if (result == MessageBoxResult.OK)
                    {
                        w.Login();
                    }
                }
                else
                    MessageBox.Show(ans, "Error", MessageBoxButton.OK);
            }
            else
                MessageBox.Show(ans, "Error", MessageBoxButton.OK);

        }

    }
}
