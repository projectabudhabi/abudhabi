﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Backend;

namespace PL_GUI
{
    /// <summary>
    /// Interaction logic for Page1.xaml
    /// </summary>
    public partial class EditTransaction : Page
    {

        MainFrame w;
        Transaction toEdit;
        ClubMember c;
        int fromPage;

        public EditTransaction(MainFrame _w, Transaction t, ClubMember _c, int _fromPage)
        {
            InitializeComponent();
            fromPage = _fromPage;
            w = _w;
            c = _c;
            toEdit = t;
            String[] str = new String[] {"Cash", "Check", "Credit Card" };
            PaymentComboBox.ItemsSource = str;
            PaymentComboBox.SelectedItem = t.GetPaymentMethod();
            date.SelectedDate = t.Date;
            if(t.Is_a_return)
            {
                tranReturned.IsChecked = true;
                tranReturned.IsEnabled = false;
            }
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void TextBox_TextChanged_1(object sender, TextChangedEventArgs e)
        {

        }


        private void add_Click(object sender, RoutedEventArgs e)
        {
            String ans = "";
            if (date.SelectedDate>DateTime.Today)
                ans = ans + "Date is illegal.\n";
            if(ans.Length==0)
            {
                ans = w.itsTransactionBL.Edit(toEdit, DateTime.Today, ""+PaymentComboBox.SelectedItem, tranReturned.IsChecked==true);
                if (ans.Length == 0)
                {
                    MessageBoxResult result;
                    result = MessageBox.Show("Transaction edited successfully", "Success", MessageBoxButton.OK);
                    if (result == MessageBoxResult.OK)
                    {

                        if (fromPage == 1)
                            w.SeeMemberTran(null, fromPage);
                        else
                            w.SeeMemberTran(c, fromPage);
                    }
                }
                else
                    MessageBox.Show(ans, "Error", MessageBoxButton.OK);
            }
            else
                MessageBox.Show(ans, "Error", MessageBoxButton.OK);

        }

        private void Back_Click(object sender, RoutedEventArgs e)
        {
            if (fromPage == 1)
                w.SeeMemberTran(null, fromPage);
            else
                w.SeeMemberTran(c, fromPage);
        }

        private void logout_Click(object sender, RoutedEventArgs e)
        {
            w.Login();
        }

        private void home_Click(object sender, RoutedEventArgs e)
        {
            w.MainMenu();
        }

    }
}
