﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PL_GUI
{
    public class Location
    {
        public string name;
        public string city;

        public Location(string name, string city)
        {
            this.name = name;
            this.city = city;
        }
    }
}
