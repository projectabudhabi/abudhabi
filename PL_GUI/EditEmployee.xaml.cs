﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Backend;
using BL;

namespace PL_GUI
{
    /// <summary>
    /// Interaction logic for Page1.xaml
    /// </summary>
    public partial class EditEmployee : Page
    {

        MainFrame w;
        List<Employee> supervisors;
        List<Department> departments;
        Employee toEdit;
        bool asProfile;

        public EditEmployee(MainFrame _w, Employee e, bool _asProfile)
        {
            InitializeComponent();
            asProfile = _asProfile;
            w = _w;
            toEdit = e;
            Headline.Content = e.Username + "'s Profile";
            int indexOfSupervisor=0;
            String[] types = new String[] { "Administrator", "Manager", "Worker" };
            TypeComboBox.ItemsSource = types;
            TypeComboBox.SelectedItem = e.GetType();
            supervisors = w.itsEmployeeBL.GetSupervisors();
            supervisors.Remove(toEdit);
            departments = w.itsDepartmentBL.GetAll();
            String[] departmentsSource = new String[departments.Count];
            String[] supervisorsSource = new String[supervisors.Count];
            for(int i=0; i<supervisors.Count;i++)
            {
                supervisorsSource[i] = supervisors.ElementAt(i).ID + ", " + supervisors.ElementAt(i).FirstName + " " + supervisors.ElementAt(i).LastName;
                if (supervisors[i].ID.Equals(e.Supervisor))
                    indexOfSupervisor = i;
            }
            for (int i = 0; i < departments.Count; i++)
            {
                departmentsSource[i] = departments.ElementAt(i).Name;
            }
            Department itsDep = w.itsDepartmentBL.GetByID(toEdit.DepartmentID);
            DepartmentComboBox.ItemsSource = departmentsSource;
            DepartmentComboBox.SelectedItem = itsDep.Name;
            SupervisorComboBox.ItemsSource = supervisorsSource;
            SupervisorComboBox.SelectedIndex = indexOfSupervisor;
            if(!e.GetType().Equals("Worker"))
            {
                SupervisorComboBox.Visibility = Visibility.Hidden;
                SupevisorLabel.Visibility = Visibility.Hidden;
            }
            String[] days = new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31" };
            String[] months = new String[] { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };
            String[] years = new String[100];
            for (int i = 0; i < 100; i++)
            {
                int temp = DateTime.Today.Year - i;
                years[i] = "" + temp;
            }
            bdayDay.ItemsSource = days;
            bdayDay.SelectedIndex = e.Birthday.Day - 1;
            bdayMonth.ItemsSource = months;
            bdayMonth.SelectedIndex = e.Birthday.Month - 1;
            bdayYear.ItemsSource = years;
            bdayYear.SelectedItem = "" + e.Birthday.Year;
            fname.Text = e.FirstName;
            lname.Text = e.LastName;
            username.Text = e.Username;
            password.Text = e.Password;
            id.Text = e.ID;
            Salary.Text = ""+e.Salary;
            if (e.GetGender().Equals("Female"))
                female.IsChecked = true;
            Employee currUser = (Employee)(w.activeUser);
            if (currUser.GetType().Equals("Worker") || (currUser.GetType().Equals("Manager") && currUser.EmpID == toEdit.EmpID))
            {
                DepartmentComboBox.IsEnabled = false;
                TypeComboBox.IsEnabled = false;
                Salary.IsEnabled = false;
                SupervisorComboBox.IsEnabled = false;
            }
            if(currUser.GetType().Equals("Manager") && currUser.EmpID!=toEdit.EmpID)
            {
                DepartmentComboBox.IsEnabled = true;
                TypeComboBox.IsEnabled = false;
                Salary.IsEnabled = true;
                SupervisorComboBox.IsEnabled = false;
            }
            if (toEdit.EmpID==1)
            {
                DepartmentComboBox.IsEnabled = true;
                TypeComboBox.IsEnabled = false;
                Salary.IsEnabled = true;
                SupervisorComboBox.IsEnabled = false;
            }

        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void TextBox_TextChanged_1(object sender, TextChangedEventArgs e)
        {

        }

        private void Back_Click(object sender, RoutedEventArgs e)
        {
            if (!asProfile)
                w.SeeEmployees();
            else
                w.MainMenu();
        }


        private void edit_Click(object sender, RoutedEventArgs e)
        {
            String ans = "";
            String supervisor = "";
            DateTime bday = DateTime.Today;
            int dep,salary=0;
            String gender = "Male";
            if (!InputCheck.isValidName(fname.Text))
                ans = ans + "First name is illegal.\n";
            if (!InputCheck.isValidName(lname.Text))
                ans = ans + "Last name is illegal.\n";
            if (!InputCheck.isValidUser(username.Text))
                ans = ans + "Username is illegal.\n";
            if (!InputCheck.isValidUser(password.Text))
                ans = ans + "Password is illegal.\n";
            if (!InputCheck.isID(id.Text))
                ans = ans + "ID is illegal.\n";
            if (female.IsChecked == true)
                gender = "Female";
            int day, year;
            int.TryParse("" + bdayDay.SelectedItem, out day);
            int.TryParse("" + bdayYear.SelectedItem, out year);
            int month = bdayMonth.SelectedIndex + 1;
            bday = new DateTime(year, month, day);
            if (bday > DateTime.Today)
                ans = ans + "Birthday is illegal.\n";
            salary = InputCheck.changeToInt(Salary.Text);
            if (salary <= 0)
                ans = ans + "Salary is illegal";

            if (SupervisorComboBox.Visibility == Visibility.Visible)
            {
                supervisor = (supervisors.ElementAt(SupervisorComboBox.SelectedIndex)).ID;
            }
            else
                supervisor = id.Text;
            dep = departments.ElementAt(DepartmentComboBox.SelectedIndex).ID;
            if(ans.Length==0)
            {
                ans = w.itsEmployeeBL.Edit(toEdit,""+TypeComboBox.SelectedItem, username.Text, password.Text, id.Text, fname.Text, lname.Text, gender, dep, salary, supervisor, bday);
                
                if (ans.Length == 0)
                {
                    MessageBoxResult result;
                    result = MessageBox.Show("Employee edited successfully", "Success", MessageBoxButton.OK);
                    if (result == MessageBoxResult.OK)
                    {

                        if (!asProfile)
                            w.SeeEmployees();
                        else
                            w.MainMenu();
                    }
                }
                else
                    MessageBox.Show(ans, "Error", MessageBoxButton.OK);
            }
            else
                MessageBox.Show(ans, "Error", MessageBoxButton.OK);

        }

        private void TypeComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if(TypeComboBox.SelectedIndex==0 || TypeComboBox.SelectedIndex==1)
            {
                SupevisorLabel.Visibility = Visibility.Hidden;
                SupervisorComboBox.Visibility = Visibility.Hidden;
            }
            else
            {
                SupevisorLabel.Visibility = Visibility.Visible;
                SupervisorComboBox.Visibility = Visibility.Visible;
            }
        }

        private void logout_Click(object sender, RoutedEventArgs e)
        {
            w.Login();
        }

        private void home_Click(object sender, RoutedEventArgs e)
        {
            w.MainMenu();
        }

    }
}
