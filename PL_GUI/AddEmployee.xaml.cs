﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Backend;
using BL;

namespace PL_GUI
{
    /// <summary>
    /// Interaction logic for Page1.xaml
    /// </summary>
    public partial class AddEmployee : Page
    {

        MainFrame w;
        List<Employee> supervisors;
        List<Department> departments;

        public AddEmployee(MainFrame _w)
        {
            InitializeComponent();
            w = _w;
            String[] types = new String[] { "Administrator", "Manager", "Worker" };
            TypeComboBox.ItemsSource = types;
            TypeComboBox.SelectedIndex = 2;
            supervisors = w.itsEmployeeBL.GetSupervisors();
            departments = w.itsDepartmentBL.GetAll();
            String[] departmentsSource = new String[departments.Count];
            String[] supervisorsSource = new String[supervisors.Count];
            for(int i=0; i<supervisors.Count;i++)
            {
                supervisorsSource[i] = supervisors.ElementAt(i).ID + ", " + supervisors.ElementAt(i).FirstName + " " + supervisors.ElementAt(i).LastName; 
            }
            for (int i = 0; i < departments.Count; i++)
            {
                departmentsSource[i] = departments.ElementAt(i).Name;
            }
            DepartmentComboBox.ItemsSource = departmentsSource;
            DepartmentComboBox.SelectedIndex = 0;
            SupervisorComboBox.ItemsSource = supervisorsSource;
            SupervisorComboBox.SelectedIndex = 0;
            String[] days = new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31" };
            String[] months = new String[] { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };
            String[] years = new String[100];
            for (int i = 0; i < 100; i++)
            {
                int temp = DateTime.Today.Year - i;
                years[i] = "" + temp;
            }
            bdayDay.ItemsSource = days;
            bdayDay.SelectedIndex = 0;
            bdayMonth.ItemsSource = months;
            bdayMonth.SelectedIndex = 0;
            bdayYear.ItemsSource = years;
            bdayYear.SelectedIndex = 0;
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void TextBox_TextChanged_1(object sender, TextChangedEventArgs e)
        {

        }

        private void Back_Click(object sender, RoutedEventArgs e)
        {
            w.SeeEmployees();
        }

        private void home_Click(object sender, RoutedEventArgs e)
        {
            w.MainMenu();
        }

        private void add_Click(object sender, RoutedEventArgs e)
        {
            String ans = "";
            String supervisor = "";
            DateTime bday = DateTime.Today;
            int dep,salary=0;
            String gender = "Male";
            if (!InputCheck.isValidName(fname.Text))
                ans = ans + "First name is illegal.\n";
            if (!InputCheck.isValidName(lname.Text))
                ans = ans + "Last name is illegal.\n";
            if (!InputCheck.isValidUser(username.Text))
                ans = ans + "Username is illegal.\n";
            if (!InputCheck.isValidUser(password.Text))
                ans = ans + "Password is illegal.\n";
            if (!InputCheck.isID(id.Text))
                ans = ans + "ID is illegal.\n";
            if (female.IsChecked == true)
                gender = "Female";
            int day, year;
            int.TryParse("" + bdayDay.SelectedItem, out day);
            int.TryParse("" + bdayYear.SelectedItem, out year);
            int month = bdayMonth.SelectedIndex + 1;
            bday = new DateTime(year, month, day);
            if (bday > DateTime.Today)
                ans = ans + "Birthday is illegal.\n";
            salary = InputCheck.changeToInt(Salary.Text);
            if (salary <= 0)
                ans = ans + "Salary is illegal";

            if (SupervisorComboBox.Visibility == Visibility.Visible)
            {
                supervisor = (supervisors.ElementAt(SupervisorComboBox.SelectedIndex)).ID;
            }
            else
                supervisor = id.Text;
            dep = departments.ElementAt(DepartmentComboBox.SelectedIndex).ID;
            if(ans.Length==0)
            {
                ans = w.itsEmployeeBL.Add(""+TypeComboBox.SelectedItem, username.Text, password.Text, id.Text, fname.Text, lname.Text, gender, dep, salary, supervisor, bday);
                
                if (ans.Length == 0)
                {
                    MessageBoxResult result;
                    result = MessageBox.Show("Employee added successfully", "Success", MessageBoxButton.OK);
                    if (result == MessageBoxResult.OK)
                    {

                        w.SeeEmployees();
                    }
                }
                else
                    MessageBox.Show(ans, "Error", MessageBoxButton.OK);
            }
            else
                MessageBox.Show(ans, "Error", MessageBoxButton.OK);

        }

        private void TypeComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if(TypeComboBox.SelectedIndex==0 || TypeComboBox.SelectedIndex==1)
            {
                SupevisorLabel.Visibility = Visibility.Hidden;
                SupervisorComboBox.Visibility = Visibility.Hidden;
            }
            else
            {
                SupevisorLabel.Visibility = Visibility.Visible;
                SupervisorComboBox.Visibility = Visibility.Visible;
            }
        }

        private void logout_Click(object sender, RoutedEventArgs e)
        {
            w.Login();
        }

    }
}
