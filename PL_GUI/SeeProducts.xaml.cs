﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Backend;
using BL;

namespace PL_GUI
{
    /// <summary>
    /// Interaction logic for Page1.xaml
    /// </summary>
    public partial class SeeProducts : Page
    {

        MainFrame w;
        List<Product> sourceList;
        List<Department> allDep;

        public SeeProducts(MainFrame _w)
        {
            w = _w;
            sourceList = w.itsProductBL.GetAll();
            InitializeComponent();
            SearchMethod.ItemsSource = new String[] { "See All", "Search by ID", "Search by Name", "Search by Type", "Search by Department" };
            SearchMethod.SelectedIndex = 0;
            Type.ItemsSource = new String[] { "Food","Electronics","Clothes","Cosmetics","Toys","Other" };
            Type.SelectedIndex = 0;
            allDep = w.itsDepartmentBL.GetAll();
            String[] depNames = new String[allDep.Count];
            for(int i=0;i<allDep.Count;i++)
            {
                depNames[i]=allDep[i].Name;
            }

            Dep.ItemsSource = depNames;
            Dep.SelectedIndex = 0;
            if(w.activeUser is Employee && (((Employee)(w.activeUser)).GetType().Equals("Worker")))
            {
                add.IsEnabled = false;
            }
        }

        private void DataGrid_Loaded(object sender, RoutedEventArgs e)
        {
            products.ItemsSource = sourceList;
        }

        private void Back_Click(object sender, RoutedEventArgs e)
        {
            w.MainMenu();
        }

        private void Add_Click(object sender, RoutedEventArgs e)
        {
            w.AddProduct();
        }

        private void DataSelectionChanged(object sender, EventArgs e)
        {
            if(products.SelectedItem!=null)
            {
                if(!(((Employee)(w.activeUser)).GetType().Equals("Worker")))
                {
                    edit.Visibility = Visibility.Visible;
                    delete.Visibility = Visibility.Visible;
                }     
            }
            else
            {
                edit.Visibility = Visibility.Hidden;
                delete.Visibility = Visibility.Hidden;
            }
            
        }

        private void Delete_Click(object sender, RoutedEventArgs e)
        {
            Product temp = (Product)(products.SelectedItem);
            if (w.itsProductBL.Delete(temp.ProductID))
            {
                MessageBoxResult r = MessageBox.Show("Product has been deleted!", "Success", MessageBoxButton.OK);
                if (r == MessageBoxResult.OK)
                {
                    sourceList = w.itsProductBL.GetAll();
                    products.Items.Refresh();
                    SearchMethod.SelectedIndex = 0;
                }
                    
            }
            else
                MessageBox.Show("Product cannot be deleted!", "Success", MessageBoxButton.OK);
        }

        private void Edit_Click(object sender, RoutedEventArgs e)
        {
            Product temp = (Product)(products.SelectedItem);
            if (temp != null && w.itsProductBL.Exist(temp.ProductID))
                w.EditProduct(temp);
            else
                MessageBox.Show("Product does not exist!", "Success", MessageBoxButton.OK);
        }

        private void Search_Click(object sender, RoutedEventArgs e)
        {
            if (SearchMethod.SelectedIndex == 1)
            {
                int idtosearch;
                bool isint = int.TryParse(toSearch.Text, out idtosearch);
                if (isint)
                {
                    products.ItemsSource = w.itsProductBL.SearchByID(idtosearch);
                }
                else
                    MessageBox.Show("Invalid input!", "Success", MessageBoxButton.OK);
            }
            if (SearchMethod.SelectedIndex == 2)
            {
                products.ItemsSource = w.itsProductBL.SearchByName(toSearch.Text);
            }
            if (SearchMethod.SelectedIndex == 3)
            {
                products.ItemsSource = w.itsProductBL.SearchByType(""+Type.SelectedItem);
            }
            if (SearchMethod.SelectedIndex == 4)
            {
                products.ItemsSource = w.itsProductBL.SearchByDepartmentID(allDep[Dep.SelectedIndex].ID);
            }
            products.Items.Refresh();
            toSearch.Text = "";
        }

        private void SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            toSearch.Text = "";
            if(SearchMethod.SelectedIndex==0)
            {
                products.ItemsSource = sourceList;
                products.Items.Refresh();
                toSearch.Visibility = Visibility.Hidden;
                Search.Visibility = Visibility.Hidden;
                Type.Visibility = Visibility.Hidden;
                Dep.Visibility = Visibility.Hidden;
            }
            if (SearchMethod.SelectedIndex == 1 || SearchMethod.SelectedIndex == 2)
            {
                toSearch.Visibility = Visibility.Visible;
                Search.Visibility = Visibility.Visible;
                Type.Visibility = Visibility.Hidden;
                Dep.Visibility = Visibility.Hidden;
            }
            if (SearchMethod.SelectedIndex == 3)
            {
                toSearch.Visibility = Visibility.Hidden;
                Search.Visibility = Visibility.Visible;
                Type.Visibility = Visibility.Visible;
                Dep.Visibility = Visibility.Hidden;
            }
            if (SearchMethod.SelectedIndex == 4)
            {
                toSearch.Visibility = Visibility.Hidden;
                Search.Visibility = Visibility.Visible;
                Type.Visibility = Visibility.Hidden;
                Dep.Visibility = Visibility.Visible;
            }
            
        }

        private void logout_Click(object sender, RoutedEventArgs e)
        {
            w.Login();
        }

        private void home_Click(object sender, RoutedEventArgs e)
        {
            w.MainMenu();
        }

    }
}
