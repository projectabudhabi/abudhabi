﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PL_GUI
{
    /// <summary>
    /// Interaction logic for Page1.xaml
    /// </summary>
    public partial class AddDepartment : Page
    {

        MainFrame w;

        public AddDepartment(MainFrame _w)
        {
            InitializeComponent();
            w = _w;
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void TextBox_TextChanged_1(object sender, TextChangedEventArgs e)
        {

        }


        private void add_Click(object sender, RoutedEventArgs e)
        {
            String ans = "";
            if (!InputCheck.isValidName(dname.Text))
                ans = ans + "Department name is illegal.\n";
            if(ans.Length==0)
            {
                ans = w.itsDepartmentBL.Add(dname.Text);
                
                if (ans.Length == 0)
                {
                    MessageBoxResult result;
                    result = MessageBox.Show("Department added successfully", "Success", MessageBoxButton.OK);
                    if (result == MessageBoxResult.OK)
                    {
                        w.SeeDepartments();
                    }
                }
                else
                    MessageBox.Show(ans, "Error", MessageBoxButton.OK);
            }
            else
                MessageBox.Show(ans, "Error", MessageBoxButton.OK);

        }

        private void Back_Click(object sender, RoutedEventArgs e)
        {
            w.SeeDepartments();
        }

        private void logout_Click(object sender, RoutedEventArgs e)
        {
            w.Login();
        }

        private void home_Click(object sender, RoutedEventArgs e)
        {
            w.MainMenu();
        }

    }
}
