﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Backend;
using BL;
using System.Net;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Linq;
using System.IO;
using System.Web;

namespace PL_GUI
{
    public partial class SeeOurStores : Page
    {
        MainFrame w;
        int fromPage;
        private Location l = new Location("location1", "ashdod");
        XmlDocument myXmlDocument = new XmlDocument();
        public SeeOurStores(MainFrame _w, int _fromPage)
        {
            fromPage = _fromPage;
            w = _w;
            InitializeComponent();
            List<String> com = new List<string>();
            com.Add("Beersheba, Israel");
            com.Add("Beijing, China");
            com.Add("Eilat, Israel");
            com.Add("Kyiv, Ukraine");
            com.Add("London, Great-Britain");
            com.Add("Nahariya, Israel");
            com.Add("New-York, Usa");
            com.Add("Paris, France");
            com.Add("Petah-Tikva, Israel");
            com.Add("Rosh-Haayin, Israel");
            serchmethod.ItemsSource = com;
        }

        private void CallMap(string _country, string _city, string _street)
        {
            string str = "<html><body><iframe src='http://www.yourmapmaker.com/preview.php?a=";
            str += _country;
            str += _city;
            str += _street;
            str += "&w=600&h=270&n=&z=14&t=Map' height='270' width='600' scrolling='no' frameborder='0'></iframe></body></html>";
            webBrowser1.NavigateToString(str);
        }
       
        private void serch_SekectedItemChenge(object sender, RoutedEventArgs e)
        {
            string city = "";
                city = l.city;

            String sol = (String)serchmethod.SelectedItem;
            switch (sol)
            {
                case "London, Great-Britain":
                   Weather2.Content=getTemperature_OWM_API("London", "metric");
                   Humidity2.Content = getHumidity("London");
                   General2.Content = getGeneralWeather("London");
                   Pressure2.Content = getPressure("London");
                   CallMap("Great-Britain", "London", "5 Strand");
                     break;

                case "Eilat, Israel":
                     Weather2.Content = getTemperature_OWM_API("Eilat", "metric");
                     Humidity2.Content = getHumidity("Eilat");
                     General2.Content = getGeneralWeather("Eilat");
                     Pressure2.Content = getPressure("Eilat");
                     CallMap("Israel", "Eilat", "30 Kadesh");
                     break;

                case "Kyiv, Ukraine":
                     Weather2.Content = getTemperature_OWM_API("Kyiv", "metric");
                     Humidity2.Content = getHumidity("Kyiv");
                     General2.Content = getGeneralWeather("Kyiv");
                     Pressure2.Content = getPressure("Kyiv");
                    CallMap("Ukraine", "Kyiv", "10 Khreshatyik");
                   
                     break;
                case "Paris, France":
                     Weather2.Content = getTemperature_OWM_API("Paris", "metric");
                     Humidity2.Content = getHumidity("Paris");
                     General2.Content = getGeneralWeather("Paris");
                     Pressure2.Content = getPressure("Paris");
                    CallMap("France", "Paris", "4 Rue des archives");
                   
                     break;

                case "New-York, Usa":
                     Weather2.Content = getTemperature_OWM_API("New-York", "metric");
                     Humidity2.Content = getHumidity("New-York");
                     General2.Content = getGeneralWeather("New-York");
                     Pressure2.Content = getPressure("New-York");
                    CallMap("United States", "New York", "6th avenue");
                   
                     break;

                case "Petah-Tikva, Israel":
                     Weather2.Content = getTemperature_OWM_API("Petah-Tikva", "metric");
                     Humidity2.Content = getHumidity("Petah-tikva");
                     General2.Content = getGeneralWeather("Petah-Tikva");
                     Pressure2.Content = getPressure("Petah-Tikva");
                    CallMap("Israel", "Petah Tikva", "20 Hertsel");
                    
                     break;

                case "Rosh-Haayin, Israel":
                     Weather2.Content = getTemperature_OWM_API("Rosh-Haayin", "metric");
                     Humidity2.Content = getHumidity("Rosh-Haayin");
                     General2.Content = getGeneralWeather("Rosh-Haayin");
                     Pressure2.Content = getPressure("Rosh-Haayin");
                    CallMap("Israel", "Rosh-Haayin", "2 Haneviim ");
                    
                     break;

                case "Beijing, China":
                     Weather2.Content = getTemperature_OWM_API("Beijing", "metric");
                     Humidity2.Content = getHumidity("Beijing");
                     General2.Content = getGeneralWeather("Beijing");
                     Pressure2.Content = getPressure("Beijing");
                    CallMap("China", "Beijing", "10 Chaoyangmen inner");
                  
                     break;

                case "Nahariya, Israel":
                     Weather2.Content = getTemperature_OWM_API("Nahariya", "metric");
                     Humidity2.Content = getHumidity("Nahariya");
                     General2.Content = getGeneralWeather("Nahariya");
                     Pressure2.Content = getPressure("Nahariya");
                    CallMap("Israel", "Nahariya", "68 Wolfson");
                   
                    break;

                case "Beersheba, Israel":
                    Weather2.Content = getTemperature_OWM_API("Beersheba", "metric");
                    Humidity2.Content = getHumidity("Beersheba");
                    General2.Content = getGeneralWeather("Beersheba");
                    Pressure2.Content = getPressure("Beersheba");
                    CallMap("Israel", "Beersheba", "20 Hashalom");

                    break;
            }
        }

        private void logout_Click(object sender, RoutedEventArgs e)
        {
            w.Login();
        }

        private void home_Click(object sender, RoutedEventArgs e)
        {
            w.MainMenu();
        }

        private void Back_Click(object sender, RoutedEventArgs e)
        {
            w.MainMenu();
        }

        private void DataSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
           
        }

        public static string getTemperature_OWM_API(string city, string unit)
        {
            try
            {
                string m_strFilePath = "http://api.openweathermap.org/data/2.5/weather?q=" + city + "&mode=xml&units=" + unit;//URL to API (including request)
                XmlDocument myXmlDocument = new XmlDocument();
                myXmlDocument.Load(m_strFilePath); // load the web service's reply -XML

                XmlNode node = myXmlDocument.DocumentElement.ChildNodes[1];// //look for sub node <temperature/>
                if (node != null)
                    if (node.Attributes != null)
                        return node.Attributes["value"].Value + "°C";//get node's value for the tempurature

                return "";// if failed

            }
            catch (Exception e2)
            {
                Console.WriteLine("could not get the weather");
                return "";
            }
        }

        public static string getHumidity(string city)
        {
            try
            {
                string m_strFilePath = "http://api.openweathermap.org/data/2.5/weather?q=" + city+ "&mode=xml";//URL to API (including request)
                XmlDocument myXmlDocument = new XmlDocument();
                myXmlDocument.Load(m_strFilePath); // load the web service's reply -XML

                XmlNode node = myXmlDocument.DocumentElement.ChildNodes[2];
                if (node != null)
                    if (node.Attributes != null)
                        return node.Attributes["value"].Value + node.Attributes["unit"].InnerText;

                return "";// if failed

            }
            catch (Exception e2)
            {
                Console.WriteLine("could not get the humidity");
                return "";
            }
        }

        public static string getGeneralWeather(string city)
        {
            try
            {
                string m_strFilePath = "http://api.openweathermap.org/data/2.5/weather?q=" + city + "&mode=xml";//URL to API (including request)
                XmlDocument myXmlDocument = new XmlDocument();
                myXmlDocument.Load(m_strFilePath); // load the web service's reply -XML

                XmlNode node = myXmlDocument.DocumentElement.ChildNodes[8];
                if (node != null)
                    if (node.Attributes != null)
                        return node.Attributes["value"].Value;

                return "";// if failed

            }
            catch (Exception e2)
            {
                Console.WriteLine("could not get the general weather");
                return "";
            }
        }

        public static string getPressure(string city)
        {
            try
            {
                string m_strFilePath = "http://api.openweathermap.org/data/2.5/weather?q=" + city + "&mode=xml";//URL to API (including request)
                XmlDocument myXmlDocument = new XmlDocument();
                myXmlDocument.Load(m_strFilePath); // load the web service's reply -XML

                XmlNode node = myXmlDocument.DocumentElement.ChildNodes[3];
                if (node != null)
                    if (node.Attributes != null)
                        return node.Attributes["value"].Value + " " + node.Attributes["unit"].InnerText;

                return "";// if failed

            }
            catch (Exception e2)
            {
                Console.WriteLine("could not get the pressure");
                return "";
            }
        }
        
        
        public static string ParseBetween(string Subject, string Start, string End)
        {
            return Regex.Match(Subject, Regex.Replace(Start, @"[][{}()*+?.\\^$|]", @"\$0") + @"\s*(((?!" + Regex.Replace(Start, @"[][{}()*+?.\\^$|]", @"\$0") + @"|" + Regex.Replace(End, @"[][{}()*+?.\\^$|]", @"\$0") + @").)+)\s*" + Regex.Replace(End, @"[][{}()*+?.\\^$|]", @"\$0"), RegexOptions.IgnoreCase).Value.Replace(Start, "").Replace(End, "");
        }

        public static string getDefinition_DictionaryCom_PageCrawl(string word)
        {
            try
            {
                //start web client and load page HTML
                WebClient myClient = new WebClient();
                string result = myClient.DownloadString("http://dictionary.reference.com/browse/" + word + "?s=ts");

                //search for content of our tags using a REGEX
                string startTag = "meta name=\"description\" content=\"";
                string endTag = " See More";
                string definition = ParseBetween(result, startTag, endTag);
                definition = definition.Substring(0, definition.Length - endTag.Length);//remove the ending "See more"

                return definition;
            }
            catch (Exception e1)
            {
                Console.WriteLine("could not get the definition");
                return "";
            }

        }
    }
}