﻿using Backend;
using PL_GUI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media; 
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Controls.Primitives;
using Microsoft.VisualBasic;



namespace WpfControlLibrary1
{
    /// <summary>
    /// Interaction logic for DragnDropShoping.xaml
    /// </summary>
    public partial class DragnDropShoping : Page
    {
        public int rowIndex = -1;
        public delegate Point GetPosition(IInputElement element);
        private MainFrame w;
        private List<Department> allDep;
        private List<Product> sourceList;
        // tmp;

        public DragnDropShoping(MainFrame _w)
        {
            w = _w;
            sourceList = w.itsProductBL.GetAll();
            InitializeComponent();
            pbStatus.Visibility = Visibility.Hidden;
            v.Visibility = Visibility.Hidden;

            products.PreviewMouseLeftButtonDown += new MouseButtonEventHandler(products_PreviewMouseLeftButtonDown);
            shopingcart.Drop += new DragEventHandler(productsDataGrid_Drop);
           // deletefrom.Visibility = Visibility.Hidden;
            getPrice();
            SearchMethod.ItemsSource = new String[] { "See All", "Search by ID", "Search by Name", "Search by Type", "Search by Department" };
            SearchMethod.SelectedIndex = 0;
            Type.ItemsSource = new String[] { "Food", "Electronics", "Clothes", "Cosmetics", "Toys", "Other" };
            Type.SelectedIndex = 0;
            allDep = w.itsDepartmentBL.GetAll();
            String[] depNames = new String[allDep.Count];
            for (int i = 0; i < allDep.Count; i++)
            {
                depNames[i] = allDep[i].Name;
            }

            Dep.ItemsSource = depNames;
            Dep.SelectedIndex = 0;
          
        }
        public void getPrice()
        {
            if(w.shopingcart.Count==0)
                sumprice.Content = "0";
            else
            {
                double sum = 0;
                foreach (Receipt r in w.shopingcart)
                {
                    sum += (r.Price * r.Amount);
                }
                sumprice.Content = ""+sum;
            }
        }
        public void gridLoadCart(object sender, RoutedEventArgs e)
        {
            var grid = sender as DataGrid;
            List<Receipt> l = w.shopingcart;
            List<Product> k = w.itsProductBL.GetAll();
            var result = from Pro in k join Rec in l on Pro.ProductID equals Rec.ProductID select new { Name = Pro.Name, Amount = Rec.Amount, Price = Rec.Price }; 
            grid.ItemsSource = result;
        }
        public void gridLoadProducts(object sender, RoutedEventArgs e)
        {
            products.ItemsSource =sourceList;
        }

        private void products_PreviewMouseLeftButtonDown(object sender, MouseEventArgs e)
        {
            {
                rowIndex = GetCurrentRowIndex(e.GetPosition);
                if (rowIndex < 0)
                    return;
                products.SelectedIndex = rowIndex;
                Product selectedEmp = products.Items[rowIndex] as Product;
                if (selectedEmp == null)
                    return;
                DragDropEffects dragdropeffects = DragDropEffects.Move;
                if (DragDrop.DoDragDrop(products, selectedEmp, dragdropeffects)
                                    != DragDropEffects.None)
                {
                    products.SelectedItem = selectedEmp;
                }
            }
        }
        private int GetCurrentRowIndex(GetPosition pos)
        {
            int curIndex = -1;
            for (int i = 0; i < products.Items.Count; i++)
            {
                DataGridRow itm = GetRowItem(i);
                if (GetMouseTargetRow(itm, pos))
                {
                    curIndex = i;
                    break;
                }
            }
            return curIndex;
        }
        private bool GetMouseTargetRow(Visual theTarget, GetPosition position)
        {
            if (theTarget != null && position != null)
            {
                Rect rect = VisualTreeHelper.GetDescendantBounds(theTarget);
                Point point = position((IInputElement)theTarget);
                return rect.Contains(point);
            }
            return false;
        }
        private DataGridRow GetRowItem(int index)
        {
            if (products.ItemContainerGenerator.Status
                    != GeneratorStatus.ContainersGenerated)
                return null;
            return products.ItemContainerGenerator.ContainerFromIndex(index)
                                                            as DataGridRow;
        }
        System.Windows.Threading.DispatcherTimer dispatcherTimer;
        private void productsDataGrid_Drop(object sender, DragEventArgs e)
        {
            pbStatus.Visibility = Visibility.Visible;
            v.Visibility = Visibility.Visible;

            dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
            dispatcherTimer.Tick += dispatcherTimer_Tick;
            dispatcherTimer.Interval = new TimeSpan(0, 0, 1);
            dispatcherTimer.Start();
            
            if (rowIndex < 0)
                return;
            Product selectedEmp = products.Items[rowIndex] as Product;
            Receipt temp = new Receipt(selectedEmp.ProductID, 1, selectedEmp.Price);
            
            String k=Interaction.InputBox("Enter quantity", "Quantity", "1", 200, 200);
            int kk=0;
            MessageBoxResult r = MessageBoxResult.OK;
            while((!k.Equals(""))&&(!int.TryParse(k,out kk))&&r==MessageBoxResult.OK){
                r = MessageBox.Show("Enter only numbers", "Error", MessageBoxButton.OK);
                k = Interaction.InputBox("Enter quantity", "Quantity", "1", 200, 200);
               
            }
            
            if(kk>0)
                if (!isThereTheSame(selectedEmp, kk))
                {
                    temp.Amount = kk;
                    if (kk > selectedEmp.InStock)
                    {
                        MessageBox.Show("Amount in stock is not enough", "Error", MessageBoxButton.OK);
                        pbStatus.Visibility = Visibility.Hidden;
                        v.Visibility = Visibility.Hidden;
                    }
                    else
                    {
                        w.shopingcart.Add(temp);
                        List<Receipt> l = w.shopingcart;
                        List<Product> kl = w.itsProductBL.GetAll();
                        var result = from Pro in kl join Rec in l on Pro.ProductID equals Rec.ProductID select new { Name = Pro.Name, Amount = Rec.Amount, Price = Rec.Price };
                        shopingcart.ItemsSource = result;
                        //pbStatus.Visibility = Visibility.Visible;
                        //v.Visibility = Visibility.Visible;

                        //shopingcart.ItemsSource = result;
                    }
                }

            
            getPrice();
          //  shopingcart.Items.Refresh();
            
            
            BackgroundWorker worker = new BackgroundWorker();
            worker.WorkerReportsProgress = true;
            worker.DoWork += worker_DoWork;
            worker.ProgressChanged += worker_ProgressChanged;

            worker.RunWorkerAsync();
            
            }

        
        private bool isThereTheSame(Product t,int amount)
        {
            
            foreach (Receipt g in w.shopingcart)
            {
                if (g.ProductID==t.ProductID)
                {
                    if (g.Amount + amount <= t.InStock)
                    {
                        g.Amount += amount;
                        List<Receipt> l = w.shopingcart;
                        List<Product> kl = w.itsProductBL.GetAll();
                        var result = from Pro in kl join Rec in l on Pro.ProductID equals Rec.ProductID select new { Name = Pro.Name, Amount = Rec.Amount, Price = Rec.Price };
                        shopingcart.ItemsSource = result;
                    }
                    else
                    {
                        MessageBox.Show("Amount in stock is not enough", "Error", MessageBoxButton.OK);
                        pbStatus.Visibility = Visibility.Hidden;
                        v.Visibility = Visibility.Hidden;
                    }
                    return true;
                }
            }
            return false;
        }
        private Receipt isDelete(Receipt t, int amount)
        {

            foreach (Receipt g in w.shopingcart)
            {
                if (g.ProductID == t.ProductID)
                {
                    if (g.Amount - amount >= 0)
                    {
                        g.Amount -= amount;
                        return g;
                    }
                    else
                        MessageBox.Show("Invalid amount", "Error", MessageBoxButton.OK);
                }
            }
            return null;
        }
        private void deletefrom_Click(object sender, RoutedEventArgs e)
        {

            if (shopingcart.SelectedIndex != -1)
            {
                Receipt temp = w.shopingcart.ElementAt(shopingcart.SelectedIndex);
                if (temp.Amount == 1)
                {
                    w.shopingcart.Remove(temp);
                    List<Receipt> l = w.shopingcart;
                    List<Product> kl = w.itsProductBL.GetAll();
                    var result = from Pro in kl join Rec in l on Pro.ProductID equals Rec.ProductID select new { Name = Pro.Name, Amount = Rec.Amount, Price = Rec.Price };
                    shopingcart.ItemsSource = result;
                    //     deletefrom.Visibility = Visibility.Hidden;
                }
                else
                {
                    String k = Interaction.InputBox("Enter quantity to remove from cart", "Quantity", "1", 200, 200);
                    int kk = 0;
                    MessageBoxResult r = MessageBoxResult.OK;
                    while ((!k.Equals("")) && (!int.TryParse(k, out kk)) && r == MessageBoxResult.OK)
                    {
                        r = MessageBox.Show("Enter only numbers", "Error", MessageBoxButton.OK);
                        k = Interaction.InputBox("Enter quantity to remove from cart", "Quantity", "1", 500, 500);
                    }
                    if (kk > 0)
                    {
                        temp = isDelete(temp, kk);
                        if (temp != null)
                            if (temp.Amount == 0)
                            {
                                w.shopingcart.Remove(temp);
                            }
                        List<Receipt> l = w.shopingcart;
                        List<Product> kl = w.itsProductBL.GetAll();
                        var result = from Pro in kl join Rec in l on Pro.ProductID equals Rec.ProductID select new { Name = Pro.Name, Amount = Rec.Amount, Price = Rec.Price };
                        shopingcart.ItemsSource = result;
                    }


                }
            }
            
        //    deletefrom.Visibility = Visibility.Hidden;
            getPrice();
            shopingcart.Items.Refresh();
        }

        private void clearall_Click(object sender, RoutedEventArgs e)
        {
            w.shopingcart = new List<Receipt>();
            List<Receipt> l = w.shopingcart;
            List<Product> kl = w.itsProductBL.GetAll();
            var result = from Pro in kl join Rec in l on Pro.ProductID equals Rec.ProductID select new { Name = Pro.Name, Amount = Rec.Amount, Price = Rec.Price };
            shopingcart.ItemsSource = result;
            shopingcart.Items.Refresh();
        }
        private void selectionChange(object sender, RoutedEventArgs e)
        {
           // deletefrom.Visibility = Visibility.Visible;
        }
        private void selectionChangew(object sender, RoutedEventArgs e)
        {
          //  deletefrom.Visibility = Visibility.Hidden;
        }

        private void buy_Click(object sender, RoutedEventArgs e)
        {
            if (w.shopingcart.Count != 0)
                w.buy();
        }

        private void Search_Click(object sender, RoutedEventArgs e)
        {
            if (SearchMethod.SelectedIndex == 1)
            {
                int idtosearch;
                bool isint = int.TryParse(toSearch.Text, out idtosearch);
                if (isint)
                {
                    products.ItemsSource = w.itsProductBL.SearchByID(idtosearch);
                    
                }
                else
                    MessageBox.Show("Invalid input!", "Success", MessageBoxButton.OK);
            }
            if (SearchMethod.SelectedIndex == 2)
            {
                products.ItemsSource = w.itsProductBL.SearchByName(toSearch.Text);
                
            }
            if (SearchMethod.SelectedIndex == 3)
            {
                products.ItemsSource = w.itsProductBL.SearchByType("" + Type.SelectedItem);
                
            }
            if (SearchMethod.SelectedIndex == 4)
            {
                products.ItemsSource = w.itsProductBL.SearchByDepartmentID(allDep[Dep.SelectedIndex].ID);

            }
            products.Items.Refresh();
            toSearch.Text = "";
        }

        private void SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            toSearch.Text = "";
            if (SearchMethod.SelectedIndex == 0)
            {
                products.ItemsSource = sourceList;
                products.Items.Refresh();
                toSearch.Visibility = Visibility.Hidden;
                Search.Visibility = Visibility.Hidden;
                Type.Visibility = Visibility.Hidden;
                Dep.Visibility = Visibility.Hidden;
            }
            if (SearchMethod.SelectedIndex == 1 || SearchMethod.SelectedIndex == 2)
            {
                toSearch.Visibility = Visibility.Visible;
                Search.Visibility = Visibility.Visible;
                Type.Visibility = Visibility.Hidden;
                Dep.Visibility = Visibility.Hidden;
            }
            if (SearchMethod.SelectedIndex == 3)
            {
                toSearch.Visibility = Visibility.Hidden;
                Search.Visibility = Visibility.Visible;
                Type.Visibility = Visibility.Visible;
                Dep.Visibility = Visibility.Hidden;
            }
            if (SearchMethod.SelectedIndex == 4)
            {
                toSearch.Visibility = Visibility.Hidden;
                Search.Visibility = Visibility.Visible;
                Type.Visibility = Visibility.Hidden;
                Dep.Visibility = Visibility.Visible;
            }
        }

        private void logout_Click(object sender, RoutedEventArgs e)
        {
            w.Login();
        }

        private void home_Click(object sender, RoutedEventArgs e)
        {
            w.MainMenu();
        }

        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            if (pbStatus.Value == 100)
            {
                pbStatus.Value = 0;
                pbStatus.Visibility = Visibility.Hidden;
                v.Visibility = Visibility.Hidden;
                dispatcherTimer.Stop();
            }
        }

        private void back_Click(object sender, RoutedEventArgs e)
        {
            w.MainMenu();
        }

        public DragnDropShoping()
        {
            InitializeComponent();
        }

        void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            for (int i = 0; i <= 100; i++)
            {
                (sender as BackgroundWorker).ReportProgress(i);
                Thread.Sleep(5);
            }
        }

        void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            pbStatus.Value = e.ProgressPercentage;
        }
    }
}
