﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Navigation;
using Backend;
using BL;
using WpfControlLibrary1;

namespace PL_GUI
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class MainFrame : Window
    {

        public ClubMember_BL itsClubMemberBL;
        public Department_BL itsDepartmentBL;
        public Employee_BL itsEmployeeBL;
        public Product_BL itsProductBL;
        public Transaction_BL itsTransactionBL;
        public User activeUser;
        public List<Receipt> shopingcart;

        
        public MainFrame(ClubMember_BL cm, Department_BL d, Employee_BL e, Product_BL p, Transaction_BL t)
        {
            InitializeComponent();
            itsClubMemberBL = cm;
            itsDepartmentBL = d;
            itsEmployeeBL = e;
            itsProductBL = p;
            itsTransactionBL = t;
            activeUser = null;
            shopingcart=new List<Receipt>();
        }
        public void Run()
        {
            Show();
            Login();
        }
        public void Login()
        {
            _mainFrame.Navigate(new Login(this));
        }
        public void MainMenu()
        {
            _mainFrame.Navigate(new MainMenu(this));
        }
        public void SignUp()
        {
            _mainFrame.Navigate(new SignUp(this));
        }
        public void AddClubMember()
        {
            _mainFrame.Navigate(new AddClubMember(this));
        }
        public void AddDepartment()
        {
            _mainFrame.Navigate(new AddDepartment(this));
        }
        public void AddEmployee()
        {
            _mainFrame.Navigate(new AddEmployee(this));
        }
        public void AddProduct()
        {
            _mainFrame.Navigate(new AddProduct(this));
        }
        public void SeeClubMembers()
        {
            _mainFrame.Navigate(new SeeClubMembers(this));
        }
        public void SeeDepartments()
        {
            _mainFrame.Navigate(new SeeDepartments(this));
        }
        public void SeeEmployees()
        {
            _mainFrame.Navigate(new SeeEmployees(this));
        }
        public void SeeProducts()
        {
            _mainFrame.Navigate(new SeeProducts(this));
        }
        public void SeeOurStores(ClubMember c, int fromPage)
        {
            _mainFrame.Navigate(new SeeOurStores(this, fromPage));
        }
        public void EditClubMembers(ClubMember c, bool asProfile)
        {
            _mainFrame.Navigate(new EditClubMember(this,c, asProfile));
        }
        public void EditDepartment(Department d)
        {
            _mainFrame.Navigate(new EditDepartment(this, d));
        }
        public void EditProduct(Product p)
        {
            _mainFrame.Navigate(new EditProduct(this, p));
        }
        public void EditEmployee(Employee e, bool asProfile)
        {
            _mainFrame.Navigate(new EditEmployee(this, e, asProfile));
        }
        public void seeCart()
        {
            _mainFrame.Navigate(new ShopingCart(this));
        }
        public void dragNDrop()
        {
            _mainFrame.Navigate(new DragnDropShoping(this));
        }
        public void SeeMemberTran(ClubMember c, int fromPage)
        {
            _mainFrame.Navigate(new SeeMemberTran(this, c, fromPage));
        }
        public void buy()
        {
            _mainFrame.Navigate(new Buy(this));
        }
        public void EditTransaction(Transaction t, ClubMember c, int fromPage)
        {
            _mainFrame.Navigate(new EditTransaction(this, t, c, fromPage));
        }
    }
}
