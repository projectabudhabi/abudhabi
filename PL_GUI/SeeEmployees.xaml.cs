﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Backend;
using BL;

namespace PL_GUI
{
    /// <summary>
    /// Interaction logic for Page1.xaml
    /// </summary>
    public partial class SeeEmployees : Page
    {

        MainFrame w;
        List<Employee> sourceList;
        List<Department> allDep;

        public SeeEmployees(MainFrame _w)
        {
            w = _w;
            if (((Employee)(w.activeUser)).GetType().Equals("Administrator"))
                sourceList = w.itsEmployeeBL.GetAll();
            else
            {
                sourceList = w.itsEmployeeBL.SearchItsWorkers((Employee)(w.activeUser));
            }

            InitializeComponent();
            if(!(((Employee)(w.activeUser)).GetType().Equals("Administrator")))
                add.IsEnabled = false;
            SearchMethod.ItemsSource = new String[] { "See All", "Search by ID", "Search by EmpID", "Search by First Name", "Search by Last Name", "Search by Username", "Search by Gender", "Search by Department", "Searh by Type" };
            SearchMethod.SelectedIndex = 0;
            Gender.ItemsSource = new String[] { "Male", "Female" };
            Type.ItemsSource = new String[] { "Administrator", "Manager", "Worker" };
            Gender.SelectedIndex = 0;
            Type.SelectedIndex = 0;
            allDep = w.itsDepartmentBL.GetAll();
            String[] depNames = new String[allDep.Count];
            for(int i=0;i<allDep.Count;i++)
            {
                depNames[i]=allDep[i].Name;
            }

            Dep.ItemsSource = depNames;
            Dep.SelectedIndex = 0;
        }

        private void DataGrid_Loaded(object sender, RoutedEventArgs e)
        {
            employees.ItemsSource = sourceList;
        }

        private void Back_Click(object sender, RoutedEventArgs e)
        {
            w.MainMenu();
        }

        private void Add_Click(object sender, RoutedEventArgs e)
        {
            w.AddEmployee();
        }

        private void DataSelectionChanged(object sender, EventArgs e)
        {
            if(employees.SelectedItem!=null)
            {
                edit.Visibility = Visibility.Visible;
                delete.Visibility = Visibility.Visible;
            }
            else
            {
                edit.Visibility = Visibility.Hidden;
                delete.Visibility = Visibility.Hidden;
            }
            
        }

        private void Delete_Click(object sender, RoutedEventArgs e)
        {
            Employee temp = (Employee)(employees.SelectedItem);
            if (temp.EmpID!=((Employee)(w.activeUser)).EmpID && w.itsEmployeeBL.Delete(temp.EmpID))
            {
                MessageBoxResult r = MessageBox.Show("Employee has been deleted!", "Success", MessageBoxButton.OK);
                if (r == MessageBoxResult.OK)
                {
                    if (((Employee)(w.activeUser)).GetType().Equals("Administrator"))
                    {
                        sourceList = w.itsEmployeeBL.GetAll();
                    }
                        
                    else
                    {
                        sourceList = w.itsEmployeeBL.SearchItsWorkers((Employee)(w.activeUser));
                    }
                    employees.ItemsSource = sourceList;
                    employees.Items.Refresh();
                    SearchMethod.SelectedIndex = 0;
                }                
            }
            else
                MessageBox.Show("Employee cannot be deleted!", "Success", MessageBoxButton.OK);
        }

        private void Edit_Click(object sender, RoutedEventArgs e)
        {
            Employee temp = (Employee)(employees.SelectedItem);
            if (temp != null && w.itsEmployeeBL.Exist(temp.ID))
                w.EditEmployee(temp, false);
            else
                MessageBox.Show("Employee does not exist!", "Success", MessageBoxButton.OK);
        }

        private void Search_Click(object sender, RoutedEventArgs e)
        {
            if (SearchMethod.SelectedIndex == 1)
            {
                employees.ItemsSource = w.itsEmployeeBL.SearchByID(sourceList, toSearch.Text);
            }
            if (SearchMethod.SelectedIndex == 2)
            {
                int idtosearch;
                bool isint = int.TryParse(toSearch.Text, out idtosearch);
                if (isint)
                {
                    employees.ItemsSource = w.itsEmployeeBL.SearchByID(sourceList, idtosearch);

                }
                else
                    MessageBox.Show("Invalid input!", "Success", MessageBoxButton.OK);
            }
            if (SearchMethod.SelectedIndex == 3)
            {
                employees.ItemsSource = w.itsEmployeeBL.SearchByFirstName(sourceList, toSearch.Text);
            }
            if (SearchMethod.SelectedIndex == 4)
            {
                employees.ItemsSource = w.itsEmployeeBL.SearchByLastName(sourceList, toSearch.Text);
            }
            if (SearchMethod.SelectedIndex == 5)
            {
                employees.ItemsSource = w.itsEmployeeBL.SearchByUsername(sourceList, toSearch.Text);
            }
            if (SearchMethod.SelectedIndex == 6)
            {
                employees.ItemsSource = w.itsEmployeeBL.SearchByGender(sourceList, "" + Gender.SelectedItem);
            }
            if (SearchMethod.SelectedIndex == 7)
            {
                employees.ItemsSource = w.itsEmployeeBL.SearchByDepartment(sourceList, allDep[Dep.SelectedIndex].ID);
            }
            if (SearchMethod.SelectedIndex == 8)
            {
                employees.ItemsSource = w.itsEmployeeBL.SearchByType(sourceList, "" + Type.SelectedItem);
            }
            employees.Items.Refresh();
            toSearch.Text = "";
        }

        private void SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            toSearch.Text = "";
            if(SearchMethod.SelectedIndex==0)
            {
                employees.ItemsSource = sourceList;
                employees.Items.Refresh();
                toSearch.Visibility = Visibility.Hidden;
                Search.Visibility = Visibility.Hidden;
                Gender.Visibility = Visibility.Hidden;
                Dep.Visibility = Visibility.Hidden;
                Type.Visibility = Visibility.Hidden;
            }

            if (SearchMethod.SelectedIndex == 1 || SearchMethod.SelectedIndex == 2 || SearchMethod.SelectedIndex == 3
                || SearchMethod.SelectedIndex == 4 || SearchMethod.SelectedIndex == 5)
            {
                toSearch.Visibility = Visibility.Visible;
                Search.Visibility = Visibility.Visible;
                Gender.Visibility = Visibility.Hidden;
                Dep.Visibility = Visibility.Hidden;
                Type.Visibility = Visibility.Hidden;
            }

            if (SearchMethod.SelectedIndex == 6)
            {
                toSearch.Visibility = Visibility.Hidden;
                Search.Visibility = Visibility.Visible;
                Gender.Visibility = Visibility.Visible;
                Dep.Visibility = Visibility.Hidden;
                Type.Visibility = Visibility.Hidden;
            }

            if (SearchMethod.SelectedIndex == 7)
            {
                toSearch.Visibility = Visibility.Hidden;
                Search.Visibility = Visibility.Visible;
                Gender.Visibility = Visibility.Hidden;
                Dep.Visibility = Visibility.Visible;
                Type.Visibility = Visibility.Hidden;
            }
            if (SearchMethod.SelectedIndex == 8)
            {
                toSearch.Visibility = Visibility.Hidden;
                Search.Visibility = Visibility.Visible;
                Gender.Visibility = Visibility.Hidden;
                Dep.Visibility = Visibility.Hidden;
                Type.Visibility = Visibility.Visible;
            }
            
        }

        private void logout_Click(object sender, RoutedEventArgs e)
        {
            w.Login();
        }

        private void home_Click(object sender, RoutedEventArgs e)
        {
            w.MainMenu();
        }

    }
}
