﻿using Backend;
using PL_GUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfControlLibrary1
{
    /// <summary>
    /// Interaction logic for Buy.xaml
    /// </summary>
    public partial class Buy : Page
    {
        private MainFrame w;
        private double sumpr;
        public Buy(MainFrame _w)
        {
            w = _w;
            InitializeComponent();
            getPrice();
           
            String[] s = { "Credit Card", "Cash", "Check" };
            payment.ItemsSource = s;
            payment.SelectedItem = "Cash";
            if(w.activeUser is ClubMember)
            {
                craditcard.Text = ((ClubMember)(w.activeUser)).CreditCard;
            }
        }
        public void craditCard(object sender, RoutedEventArgs e)
        {
            if (payment.SelectedItem.Equals("Credit Card"))
            {
                craditcard.Visibility = Visibility.Visible;
                if (w.activeUser is ClubMember)
                    addToAccount.Visibility = Visibility.Visible;
            }
            else
            {
                craditcard.Visibility = Visibility.Hidden;
                addToAccount.Visibility = Visibility.Hidden;
            }
        }
        public void getPrice()
        {
            double sum = 0;
            if (w.shopingcart.Count == 0)
                sumpay.Content = "0";
            else
            {
                foreach (Receipt r in w.shopingcart)
                {
                    sum += (r.Price * r.Amount);
                }
                sumpay.Content = ""+sum;
            }
            sumpr = sum;
        }
        public void gridLoadCart(object sender, RoutedEventArgs e)
        {
            var grid = sender as DataGrid;
            List<Receipt> l=w.shopingcart;
            List<Product> k=w.itsProductBL.GetAll();
            var result = from Pro in k join Rec in l on Pro.ProductID equals Rec.ProductID select new {Name = Pro.Name,Amount=Rec.Amount,Price=Rec.Price}; 
            grid.ItemsSource = result;
        }

        private void buy_Click(object sender, RoutedEventArgs e)
        {
            if(craditcard.Visibility ==Visibility.Hidden ||(craditcard.Visibility== Visibility.Visible && InputCheck.isCreditCard(craditcard.Text)))
            {
                ClubMember cl;
                if (w.activeUser is ClubMember)
                    cl = (ClubMember)w.activeUser;
                else
                    cl = null;
                if (!w.itsTransactionBL.add((String)payment.SelectedItem, w.shopingcart, sumpr, cl))
                    MessageBox.Show("Somthing went wrong", "Error", MessageBoxButton.OK);
                else
                {
                    if(w.activeUser is ClubMember && addToAccount.Visibility == Visibility.Visible && addToAccount.IsChecked==true)
                    {
                        w.itsClubMemberBL.AddCreditCard((ClubMember)(w.activeUser), craditcard.Text);
                    }
                    MessageBox.Show("Payment eccepted", "success", MessageBoxButton.OK);
                    w.shopingcart = new List<Receipt>();
                    w.MainMenu();
                }
            }
            else
                MessageBox.Show("Invalid credit card.", "success", MessageBoxButton.OK);

        }

        private void Back_Click(object sender, RoutedEventArgs e)
        {
            w.dragNDrop();
        }

        private void logout_Click(object sender, RoutedEventArgs e)
        {
            w.Login();
        }

        private void home_Click(object sender, RoutedEventArgs e)
        {
            w.MainMenu();
        }
    }
}
