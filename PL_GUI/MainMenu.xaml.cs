﻿using Backend;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PL_GUI
{
    /// <summary>
    /// Interaction logic for Page1.xaml
    /// </summary>
    public partial class MainMenu : Page
    {

       private MainFrame w;

        public MainMenu(MainFrame _w)
        {
            InitializeComponent();
            w = _w; 
            if (w.activeUser != null)
                userwelcome.Content = "Hello "+ w.activeUser.Username+"!";
            if (w.activeUser == null)
            {
               coustomerenter();
               viewProfile.IsEnabled = false;
            }
            else if (w.activeUser is ClubMember)
            {
                coustomerenter();
            }
            else if (((Employee)w.activeUser).GetType().Equals("Worker"))
            {
                employee.IsEnabled = false;
            }
            Product bestSeller = w.itsProductBL.GetBestSeller();
            if (bestSeller == null)
            {
                itemName.Content = "Sorry!";
                itemPrice.Content="No information yet.";
            }
            else
            {
                itemName.Content = "#"+bestSeller.ProductID+": "+ bestSeller.Name ;
                itemPrice.Content = "Just for " + bestSeller.Price + " NIS!";
                if(bestSeller.GetProType().Equals("Food"))
                {
                    foodPic.Visibility = Visibility.Visible;
                    othersPic.Visibility = Visibility.Hidden;
                    cosmeticsPic.Visibility = Visibility.Hidden;
                    toysPic.Visibility = Visibility.Hidden;
                    electronicsPic.Visibility = Visibility.Hidden;
                    clothesPic.Visibility = Visibility.Hidden;
                }
                if (bestSeller.GetProType().Equals("Other"))
                {
                    foodPic.Visibility = Visibility.Hidden;
                    othersPic.Visibility = Visibility.Visible;
                    cosmeticsPic.Visibility = Visibility.Hidden;
                    toysPic.Visibility = Visibility.Hidden;
                    electronicsPic.Visibility = Visibility.Hidden;
                    clothesPic.Visibility = Visibility.Hidden;
                }
                if (bestSeller.GetProType().Equals("Cosmetics"))
                {
                    foodPic.Visibility = Visibility.Hidden;
                    othersPic.Visibility = Visibility.Hidden;
                    cosmeticsPic.Visibility = Visibility.Visible;
                    toysPic.Visibility = Visibility.Hidden;
                    electronicsPic.Visibility = Visibility.Hidden;
                    clothesPic.Visibility = Visibility.Hidden;
                }
                if (bestSeller.GetProType().Equals("Toys"))
                {
                    foodPic.Visibility = Visibility.Hidden;
                    othersPic.Visibility = Visibility.Hidden;
                    cosmeticsPic.Visibility = Visibility.Hidden;
                    toysPic.Visibility = Visibility.Visible;
                    electronicsPic.Visibility = Visibility.Hidden;
                    clothesPic.Visibility = Visibility.Hidden;
                }
                if (bestSeller.GetProType().Equals("Electronics"))
                {
                    foodPic.Visibility = Visibility.Hidden;
                    othersPic.Visibility = Visibility.Hidden;
                    cosmeticsPic.Visibility = Visibility.Hidden;
                    toysPic.Visibility = Visibility.Hidden;
                    electronicsPic.Visibility = Visibility.Visible;
                    clothesPic.Visibility = Visibility.Hidden;
                }
                if (bestSeller.GetProType().Equals("Clothes"))
                {
                    foodPic.Visibility = Visibility.Hidden;
                    othersPic.Visibility = Visibility.Hidden;
                    cosmeticsPic.Visibility = Visibility.Hidden;
                    toysPic.Visibility = Visibility.Hidden;
                    electronicsPic.Visibility = Visibility.Hidden;
                    clothesPic.Visibility = Visibility.Visible;
                }
            }
        }
        private void coustomerenter()
        {
            clubmembers.IsEnabled = false;
            prodacts.IsEnabled = false;
            department.IsEnabled = false;
            transection.IsEnabled = false;
            employee.IsEnabled = false;
        }

        private void viewProfile_Click(object sender, RoutedEventArgs e)
        {
            if (w.activeUser is ClubMember)
                w.EditClubMembers(((ClubMember)(w.activeUser)), true);
            else
                w.EditEmployee(((Employee)(w.activeUser)),true);
        }

        private void viewOurStores_Click(object sender, RoutedEventArgs e)
        {
            w.SeeOurStores(null,1);
        }

        private void clubmembers_Click(object sender, RoutedEventArgs e)
        {
            w.SeeClubMembers();
        }

        private void department_Click(object sender, RoutedEventArgs e)
        {
            w.SeeDepartments();
        }

        private void employee_Click(object sender, RoutedEventArgs e)
        {
            w.SeeEmployees();
        }

        private void prodacts_Click(object sender, RoutedEventArgs e)
        {
            w.SeeProducts();
        }

        private void dragndrop_Click(object sender, RoutedEventArgs e)
        {
            w.dragNDrop();
        }

        private void viewcart_Click(object sender, RoutedEventArgs e)
        {
            w.seeCart();
        }

        private void transection_Click(object sender, RoutedEventArgs e)
        {
            w.SeeMemberTran(null, 1);
        }

        private void logout_Click(object sender, RoutedEventArgs e)
        {
            w.Login();
        }

        private void home_Click(object sender, RoutedEventArgs e)
        {
            w.MainMenu();
        }

      
    }
}
