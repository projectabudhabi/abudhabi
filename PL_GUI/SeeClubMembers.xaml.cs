﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Backend;
using BL;

namespace PL_GUI
{
    /// <summary>
    /// Interaction logic for Page1.xaml
    /// </summary>
    public partial class SeeClubMembers : Page
    {

        MainFrame w;
        List<ClubMember> sourceList;

        public SeeClubMembers(MainFrame _w)
        {
            w = _w;
            sourceList = w.itsClubMemberBL.GetAll();
            InitializeComponent();
            SearchMethod.ItemsSource =  new String[] { "See All", "Search by ID", "Search by MemberID", "Search by First Name", "Search by Last Name", "Search by Username", "Search by Gender" };
            SearchMethod.SelectedIndex = 0;
            Gender.ItemsSource = new String[] { "Male", "Female" };
            Gender.SelectedIndex = 0;
            if(w.activeUser is Employee && (((Employee)(w.activeUser)).GetType().Equals("Worker")))
            {
                edit.IsEnabled = false;
                delete.IsEnabled = false;
                add.IsEnabled = false;
            }
        }

        private void DataGrid_Loaded(object sender, RoutedEventArgs e)
        {
            clubmembers.ItemsSource = sourceList;
        }

        private void Back_Click(object sender, RoutedEventArgs e)
        {
            w.MainMenu();
        }

        private void Add_Click(object sender, RoutedEventArgs e)
        {
            w.AddClubMember();
        }

        private void DataSelectionChanged(object sender, EventArgs e)
        {
            if(clubmembers.SelectedItem!=null)
            {
                edit.Visibility = Visibility.Visible;
                delete.Visibility = Visibility.Visible;
                transactions.Visibility = Visibility.Visible;
            }
            else
            {
                edit.Visibility = Visibility.Hidden;
                delete.Visibility = Visibility.Hidden;
                transactions.Visibility = Visibility.Hidden;
            }
            
        }

        private void Delete_Click(object sender, RoutedEventArgs e)
        {
            ClubMember temp = (ClubMember)(clubmembers.SelectedItem);
            if (w.itsClubMemberBL.Delete(temp.MemberID))
            {
                MessageBoxResult r = MessageBox.Show("Club member has been deleted!", "Success", MessageBoxButton.OK);
                if (r == MessageBoxResult.OK)
                {
                    sourceList = w.itsClubMemberBL.GetAll();
                    clubmembers.Items.Refresh();
                    SearchMethod.SelectedIndex = 0;
                }
                    
            }
            else
                MessageBox.Show("Club member cannot be deleted!", "Success", MessageBoxButton.OK);
        }

        private void Edit_Click(object sender, RoutedEventArgs e)
        {
            ClubMember temp = (ClubMember)(clubmembers.SelectedItem);
            if (temp != null && w.itsClubMemberBL.Exist(temp.ID))
                w.EditClubMembers(temp, false);
            else
                MessageBox.Show("Club member does not exist!", "Success", MessageBoxButton.OK);
        }

        private void Search_Click(object sender, RoutedEventArgs e)
        {
            if (SearchMethod.SelectedIndex == 1)
            {
                clubmembers.ItemsSource = w.itsClubMemberBL.SearchByID(toSearch.Text);
            }
            if (SearchMethod.SelectedIndex == 2)
            {
                int idtosearch;
                bool isint = int.TryParse(toSearch.Text, out idtosearch);
                if (isint)
                {
                    clubmembers.ItemsSource = w.itsClubMemberBL.SearchByID(idtosearch);
                }
                else
                    MessageBox.Show("Invalid input!", "Success", MessageBoxButton.OK);
            }
            if (SearchMethod.SelectedIndex == 3)
            {
                clubmembers.ItemsSource =w.itsClubMemberBL.SearchByFirstName(toSearch.Text); 
            }
            if (SearchMethod.SelectedIndex == 4)
            {
                clubmembers.ItemsSource =  w.itsClubMemberBL.SearchByLastName(toSearch.Text);
            }
            if (SearchMethod.SelectedIndex == 5)
            {
                clubmembers.ItemsSource =w.itsClubMemberBL.SearchByUsername(toSearch.Text);
            }
            if (SearchMethod.SelectedIndex == 6)
            {
               clubmembers.ItemsSource = w.itsClubMemberBL.SearchByGender(""+Gender.SelectedItem);
            }
            clubmembers.Items.Refresh();
            toSearch.Text = "";
        }

        private void SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            toSearch.Text = "";
            if(SearchMethod.SelectedIndex==0)
            {
                clubmembers.ItemsSource = sourceList;
                clubmembers.Items.Refresh();
                toSearch.Visibility = Visibility.Hidden;
                Search.Visibility = Visibility.Hidden;
                Gender.Visibility = Visibility.Hidden;
            }

            if (SearchMethod.SelectedIndex == 1 || SearchMethod.SelectedIndex == 2 || SearchMethod.SelectedIndex == 3
                || SearchMethod.SelectedIndex == 4 || SearchMethod.SelectedIndex == 5)
            {
                toSearch.Visibility = Visibility.Visible;
                Search.Visibility = Visibility.Visible;
                Gender.Visibility = Visibility.Hidden;
            }

            if (SearchMethod.SelectedIndex == 6)
            {
                toSearch.Visibility = Visibility.Hidden;
                Search.Visibility = Visibility.Visible;
                Gender.Visibility = Visibility.Visible;
            }
            
        }

        private void Tran_Click(object sender, RoutedEventArgs e)
        {
            ClubMember temp = (ClubMember)(clubmembers.SelectedItem);
            if (temp != null && w.itsClubMemberBL.Exist(temp.ID))
                w.SeeMemberTran(temp, 2);
            else
                MessageBox.Show("Club member does not exist!", "Success", MessageBoxButton.OK);
        }

        private void logout_Click(object sender, RoutedEventArgs e)
        {
            w.Login();
        }

        private void home_Click(object sender, RoutedEventArgs e)
        {
            w.MainMenu();
        }

    }
}
