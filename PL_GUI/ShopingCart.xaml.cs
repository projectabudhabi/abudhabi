﻿using Backend;
using PL_GUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.VisualBasic;

namespace WpfControlLibrary1
{
    /// <summary>
    /// Interaction logic for ShopingCart.xaml
    /// </summary>
    public partial class ShopingCart : Page
    {
        private MainFrame w;

        public ShopingCart(MainFrame _w)
        {
            w = _w;
            InitializeComponent();
            if (w.shopingcart.Count == 0)
            {
                buy.Visibility = Visibility.Hidden;
                clearall.Visibility = Visibility.Hidden;
            }
                
            getPrice();
  
        }

        private void clearall_Click(object sender, RoutedEventArgs e)
        {
            w.shopingcart = new List<Receipt>();
            List<Receipt> l = w.shopingcart;
            List<Product> k = w.itsProductBL.GetAll();
            var result = from Pro in k join Rec in l on Pro.ProductID equals Rec.ProductID select new { Name = Pro.Name, Amount = Rec.Amount, Price = Rec.Price };
            cart.ItemsSource = result;
            cart.Items.Refresh();
            getPrice();
        }
        public void gridLoad(object sender, RoutedEventArgs e)
        {
            var grid = sender as DataGrid;
            List<Receipt> l = w.shopingcart;
            List<Product> k = w.itsProductBL.GetAll();
            var result = from Pro in k join Rec in l on Pro.ProductID equals Rec.ProductID select new { Name = Pro.Name, Amount = Rec.Amount, Price = Rec.Price }; 
            grid.ItemsSource = result;
        }
        private void selectionChange(object sender, RoutedEventArgs e)
        {
            delete.Visibility = Visibility.Visible;
        }
        private Receipt isDelete(Receipt t, int amount)
        {

            foreach (Receipt g in w.shopingcart)
            {
                if (g.ProductID == t.ProductID)
                {
                    if (g.Amount - amount >= 0)
                    {
                        g.Amount -= amount;
                        return g;
                    }
                    else
                        MessageBox.Show("enter less amount", "Error", MessageBoxButton.OK);
                }
            }
            return null;
        }
        public void getPrice()
        {
            if (w.shopingcart.Count == 0)
                sumprice.Content = "0";
            else
            {
                double sum = 0;
                foreach (Receipt r in w.shopingcart)
                {
                    sum += (r.Price * r.Amount);
                }
                sumprice.Content = "" + sum;
            }
        }
        private void deleate_Click(object sender, RoutedEventArgs e)
        {
            Receipt temp = w.shopingcart.ElementAt(cart.SelectedIndex);
            if (temp != null)
            {
                if (temp.Amount == 1)
                {
                    w.shopingcart.Remove(temp);
                    List<Receipt> l = w.shopingcart;
                    List<Product> kl = w.itsProductBL.GetAll();
                    var result = from Pro in kl join Rec in l on Pro.ProductID equals Rec.ProductID select new { Name = Pro.Name, Amount = Rec.Amount, Price = Rec.Price };
                    cart.ItemsSource = result;
                    //     deletefrom.Visibility = Visibility.Hidden;
                }
                else
                {
                    String k = Interaction.InputBox("Enter the quntity you dezire to remove from cart", "quntity", "1", 200, 200);
                    int kk = 0;
                    MessageBoxResult r = MessageBoxResult.OK;
                    while ((!k.Equals("")) && (!int.TryParse(k, out kk)) && r == MessageBoxResult.OK)
                    {
                        r = MessageBox.Show("Enter only numbers", "Error", MessageBoxButton.OK);
                        k = Interaction.InputBox("Enter the quntity you dezire to remove from cart", "quntity", "1", 500, 500);
                    }
                    if (kk > 0)
                    {
                        temp = isDelete(temp, kk);
                        if (temp != null)
                            if (temp.Amount == 0)
                                w.shopingcart.Remove(temp);
                        List<Receipt> l = w.shopingcart;
                        List<Product> kl = w.itsProductBL.GetAll();
                        var result = from Pro in kl join Rec in l on Pro.ProductID equals Rec.ProductID select new { Name = Pro.Name, Amount = Rec.Amount, Price = Rec.Price };
                        cart.ItemsSource = result;
                    }


                }
            }

            delete.Visibility = Visibility.Hidden;
            getPrice();
            cart.Items.Refresh();
        }

        private void buy_Click(object sender, RoutedEventArgs e)
        {
            if (w.shopingcart.Count != 0)
                w.buy();
        }

        private void back_Click(object sender, RoutedEventArgs e)
        {
            w.MainMenu();
        }

        private void logout_Click(object sender, RoutedEventArgs e)
        {
            w.Login();
        }

        private void home_Click(object sender, RoutedEventArgs e)
        {
            w.MainMenu();
        }
    }
}
