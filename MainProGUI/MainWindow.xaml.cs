﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PL_GUI;
using DAL;
using BL;
using Backend; 

namespace MainProGUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {

            InitializeComponent();
            SQL_DAL adal = new SQL_DAL();
            Employee_BL ebl = new Employee_BL(adal);
            ClubMember_BL cmbl = new ClubMember_BL(adal);
            Product_BL pbl = new Product_BL(adal);
            Transaction_BL tbl = new Transaction_BL(adal);
            Department_BL dbl = new Department_BL(adal);
            MainFrame m = new MainFrame(cmbl, dbl, ebl, pbl, tbl);
            m.Run();
            Close(); 
        }
    }
}