﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Backend;
using DAL;

namespace BL
{
    public class Product_BL : IBL
    {
        SQL_DAL itsDAL;        

        public Product_BL(SQL_DAL dal)
        {
            itsDAL = dal;
        }

        public String Add(String _name, String _type, int _departmentID, int _inStock, double _price)
        {

            String str = "";
            bool firstCon = (!Exist(_name));
            bool secondCon = false;
            List<Department> allDep = itsDAL.departmentDB;
            if(allDep!=null)
            foreach (Department d in allDep)
            {
                if (d.ID == _departmentID)
                    secondCon = true;
            }
            if (firstCon && secondCon)
            {
                itsDAL.Add(new Product(InitializeID(), _name,_type, _departmentID, _inStock, _price));
            }
            if (!firstCon)
                str = str + "Product with this name already exists!\n";
            if (!secondCon)
                str = str + "Department does not exist!\n";
            return str;
        }

        public String Edit(Product toEdit, String _name, String _type, int _departmentID, int _inStock, double _price)
        {
            String str = "";
            List<Product> temp = itsDAL.productDB;;
            if (temp != null)
                foreach (Product p in temp)
                {
                    if (p.Name.Equals(_name) && p.ProductID != toEdit.ProductID)
                        str = str + "Product with this name already exists!";
                }
            if (str.Length == 0)
            {
                itsDAL.edit(toEdit, _name, _type, _departmentID, _inStock, _price);
            }
            return str;
        }

        private int InitializeID()
        {
            List<Product> temp = itsDAL.productDB;
            if (temp.Count() != 0)
                return temp[temp.Count() - 1].ProductID + 1;
            return 1;
        }

        public bool Exist(String _name)
        {
            List<Product> temp = itsDAL.productDB;
            if (temp.Count != 0)
            {
                var result = from p in temp where p.Name == _name select p;
                return result.Count() > 0;
            }
            return false;
        }

        private Product GetByID(int id)
        {
            List<Product> temp = itsDAL.productDB;
            if(temp!=null)
            foreach (Product p in temp)
            {
                if (p.ProductID == id)
                    return p;
            }
            return null;
        }

        public bool Exist(int _id)
        {
            List<Product> temp = itsDAL.productDB;
            if (temp.Count != 0)
            {
                var result = from p in temp where p.ProductID == _id select p;
                return result.Count() > 0;
            }
            return false;
        }

        public bool Delete(int id)
        {
            Product p = GetByID(id);
            if (p == null)
                return false;
            itsDAL.delete(p);
            return true;
        }

        public List<Product> GetAll()
        {
            return itsDAL.productDB;
        }

        public List<Product> SearchByName(String _name)
        {
            List<Product> ans = new List<Product>();
            List<Product> temp = itsDAL.productDB;
            if (temp.Count != 0)
            {
                var result = from p in temp where p.Name.Equals(_name) select p;
                ans = result.ToList<Product>();
            }
            return ans;
        }

        public List<Product> SearchByID(int _id)
        {
            List<Product> ans = new List<Product>();
            List<Product> temp = itsDAL.productDB;
            if (temp.Count != 0)
            {
                var result = from p in temp where p.ProductID == _id select p;
                ans = result.ToList<Product>();
            }
            return ans;
        }

        public List<Product> SearchByDepartmentID(int id)
        {
            List<Product> ans = new List<Product>();
            List<Product> temp = itsDAL.productDB;
            if (temp.Count != 0)
            {
                var result = from e in temp where e.LocationID == id select e;
                ans = result.ToList<Product>();
            }
            return ans;
        }

        public List<Product> SearchByType(String _type)
        {
            List<Product> ans = new List<Product>();
            List<Product> temp = itsDAL.productDB;
            if (temp.Count != 0)
            {
                var result = from p in temp where p.GetProType().Equals(_type) select p;
                ans = result.ToList<Product>();
            }
            return ans;
        }

        public Product GetBestSeller()
        {
            DateTime toDate = DateTime.Today;
            toDate=toDate.AddDays(+1);
            DateTime fromDate = toDate.AddMonths(-1);
            List<Transaction> allTran = itsDAL.transactionDB;
            List<Receipt> allRec = new List<Receipt>();
            foreach (Transaction t in allTran)
            {
                if (t.Date >= fromDate && t.Date <= toDate)
                    foreach (Receipt r in t.ReceiptList)
                        allRec.Add(r);
            }
            List<Receipt> finalRec = new List<Receipt>();
            foreach (Receipt r1 in allRec)
            {
                bool found = false;
                foreach (Receipt r2 in finalRec)
                {
                    if (r1.ProductID == r2.ProductID)
                    {
                        found = true;
                        r2.Amount = r2.Amount + r1.Amount;
                    }
                }
                if (!found)
                {
                    finalRec.Add(new Receipt(r1.ProductID, r1.Amount, r1.Price));
                }
            }
            int maxProductID = 0;
            int maxAmount = 0;
            foreach (Receipt r in finalRec)
            {
                if (r.Amount > maxAmount)
                {
                    maxAmount = r.Amount;
                    maxProductID = r.ProductID;
                }
            }
            return GetByID(maxProductID);
        }
    }
}
