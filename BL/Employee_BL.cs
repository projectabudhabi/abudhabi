﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using Backend;

namespace BL
{
    public class Employee_BL
    {
        SQL_DAL itsDAL;

        public Employee_BL(SQL_DAL dal)
        {
            itsDAL = dal;
        }

        public List<Employee> GetAll()
        {
            return itsDAL.employeeDB;
        }

        public List<Employee> SearchByFirstName(List<Employee> temp, String _fname)
        {
            List<Employee> ans = new List<Employee>();
            if (temp.Count != 0)
            {
                var result = from e in temp where e.FirstName.Equals(_fname) select e;
                ans = result.ToList<Employee>();
            }
            return ans;
        }

        public List<Employee> SearchByLastName(List<Employee> temp, String _lname)
        {
            List<Employee> ans = new List<Employee>();
            if (temp.Count != 0)
            {
                var result = from e in temp where e.LastName.Equals(_lname) select e;
                ans = result.ToList<Employee>();
            }
            return ans;
        }

        public List<Employee> SearchItsWorkers(Employee manager)
        {
            List<Employee> ans = new List<Employee>();
            List<Employee> temp = itsDAL.employeeDB;
            foreach(Employee e in temp)
            {
                if (e.Supervisor.Equals(manager.ID))
                    ans.Add(e);
            }
            return ans;
        }

        public List<Employee> SearchByUsername(List<Employee> temp, String _uname)
        {
            List<Employee> ans = new List<Employee>();
            if (temp.Count != 0)
            {
                var result = from e in temp where e.Username.Equals(_uname) select e;
                ans = result.ToList<Employee>();
            }
            return ans;
        }

        public Employee GetByUser(String _username, String _password)
        {
            List<Employee> temp = itsDAL.employeeDB;
            if(temp!=null)
            foreach (Employee c in temp)
                if (c.Username.Equals(_username) && c.Password.Equals(_password))
                    return c;
            return null;
        }

        public List<Employee> SearchByID(List<Employee> temp, int _id)
        {
            List<Employee> ans = new List<Employee>();
            if (temp.Count != 0)
            {
                var result = from t in temp where t.EmpID == _id select t;
                ans = result.ToList<Employee>();
            }
            return ans;
        }

        public List<Employee> SearchByID(List<Employee> temp, String _id)
        {
            List<Employee> ans = new List<Employee>();
            if (temp.Count != 0)
            {
                var result = from t in temp where t.ID.Equals(_id) select t;
                ans = result.ToList<Employee>();
            }
            return ans;
        }

        public List<Employee> SearchByDepartment(List<Employee> temp, int id)
        {
            List<Employee> ans = new List<Employee>();
            if (temp.Count != 0)
            {
                var result = from e in temp where e.DepartmentID == id select e;
                ans = result.ToList<Employee>();
            }
            return ans;
        }

        public List<Employee> SearchByGender(List<Employee> temp, String _g)
        {
            List<Employee> ans = new List<Employee>();
            if (temp.Count != 0)
            {
                var result = from c in temp where c.GetGender().Equals(_g) select c;
                ans = result.ToList<Employee>();
            }
            return ans;
        }

        public List<Employee> SearchByType(List<Employee> temp, String _g)
        {
            List<Employee> ans = new List<Employee>();
            if (temp.Count != 0)
            {
                var result = from c in temp where c.GetType().Equals(_g) select c;
                ans = result.ToList<Employee>();
            }
            return ans;
        }

        public List<Employee> GetSupervisors()
        {
            List<Employee> ans = new List<Employee>();
            List<Employee> temp = itsDAL.employeeDB;
            if (temp.Count != 0)
            {
                var result = from c in temp where c.GetType().Equals("Administrator") || c.GetType().Equals("Manager") select c;
                ans = result.ToList<Employee>();
            }
            return ans;
        }

        public Employee GetByID(int id)
        {
            List<Employee> temp = itsDAL.employeeDB;
            if(temp!=null)
            foreach (Employee e in temp)
            {
                if (e.EmpID == id)
                    return e;
            }
            return null;
        }

        public bool Delete(int id)
        {
            if(id!=1)
            {
                Employee e = GetByID(id);
                if (e == null)
                    return false;
                else
                {
                    itsDAL.delete(e);
                    return true;
                }

            }
            return false;
        }

        public String Add(String _type, String _username, String _password, String _id, String _firstName, String _lastName, String _gender, int _departmentID, int _salary, String _supervisor, DateTime _birthday)
        {
            bool canAdd = true;
            bool depExist = false;
            String str = "";
            if (Exist(_id))
            {
                str = str + "Employee with this ID already exists!\n";
                canAdd = false;
            }
            if (UserExist(_username))
            {
                str = str + "This username already exists!\n";
                canAdd = false;
            }
            List<Department> allDep = itsDAL.departmentDB;
            if (allDep!=null)
            foreach (Department d in allDep)
            {
                if (d.ID == _departmentID)
                    depExist = true;
            }
            if(!depExist)
            {
                str = str + "Department does not exist!";
                canAdd = false;
            }
            if (canAdd)
            {
                itsDAL.Add(new Employee(InitializeID(), _type, _username, _password, _id, _firstName, _lastName, _gender, _departmentID, _salary, _supervisor, _birthday));
            }
            return str;
        }

        public String Edit(Employee toEdit, String _type, String _username, String _password, String _id, String _firstName, String _lastName, String _gender, int _departmentID, int _salary, String _supervisor, DateTime _birthday)
        {
            String str = "";
            List<Employee> temp = itsDAL.employeeDB;
            if (temp != null)
                foreach (Employee c in temp)
                {
                    if (c.ID.Equals(_id) && c.EmpID != toEdit.EmpID)
                        str = str + "Employee with such ID already exists!";
                    if (c.Username.Equals(_username) && c.EmpID != toEdit.EmpID)
                        str = str + "Username already exists!";
                }
            List<ClubMember> temp1 = itsDAL.clubmemberDB;
            foreach (ClubMember c in temp1)
            {
                if (c.Username.Equals(_username))
                    str = str + "Username already exists!";
            }
            if (str.Length == 0)
            {
                if(_type.Equals("Worker"))
                {
                    temp = itsDAL.employeeDB;
                    foreach (Employee emp in temp)
                    {
                        if (emp.Supervisor.Equals(toEdit.ID))
                        {
                            emp.Supervisor = temp.ElementAt(0).ID;
                            itsDAL.edit(emp, emp.GetType(), emp.Username, emp.Password, emp.ID, emp.FirstName, emp.LastName , emp.GetGender(), emp.DepartmentID, emp.Salary, emp.Supervisor, emp.Birthday);

                        }
                    }
                }
                else
                {
                    temp = itsDAL.employeeDB;
                    foreach (Employee emp in temp)
                    {
                        if (emp.Supervisor.Equals(toEdit.ID))
                        {
                            emp.Supervisor = _id;
                            itsDAL.edit(emp, emp.GetType(), emp.Username, emp.Password, emp.ID, emp.FirstName, emp.LastName, emp.GetGender(), emp.DepartmentID, emp.Salary, emp.Supervisor, emp.Birthday);

                        }
                    }
                }
                itsDAL.edit(toEdit, _type, _username, _password, _id, _firstName, _lastName, _gender, _departmentID, _salary, _supervisor, _birthday);
                
            }
            return str;
        }

        private int InitializeID()
        {
            List<Employee> temp = itsDAL.employeeDB;
            if (temp.Count() != 0)
                return temp[temp.Count() - 1].EmpID + 1;
            return 1;
        }

        public bool Exist(int _id)
        {
            List<Employee> temp = itsDAL.employeeDB;
            if (temp.Count != 0)
            {
                var result = from e in temp where e.EmpID == _id select e;
                return result.Count() > 0;
            }
            return false;
        }

        public bool Exist(String _id)
        {
            List<Employee> temp = itsDAL.employeeDB;
            if (temp.Count != 0)
            {
                var result = from e in temp where e.ID.Equals(_id) select e;
                return result.Count() > 0;
            }
            return false;
        }

        public bool UserExist(String _username)
        {
            List<ClubMember> temp = itsDAL.clubmemberDB;
            if (temp.Count != 0)
            {
                var result = from u in temp where u.Username.Equals(_username) select u;
                if (result.Count() > 0)
                    return true;
            }
            List<Employee> temp1 = itsDAL.employeeDB;
            if (temp1.Count != 0)
            {
                var result = from u in temp1 where u.Username.Equals(_username) select u;
                if (result.Count() > 0)
                    return true;
            }
            return false;
        }


    }
}
