﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using Backend;

namespace BL
{
    public class ClubMember_BL : IBL
    {
        SQL_DAL itsDAL;

        public ClubMember_BL(SQL_DAL dal)
        {
            itsDAL = dal;
        }

        public bool Delete(int id)
        {
            ClubMember c = GetByID(id);
            if (c == null)
                return false;
            //List<int> memberTran = c.Transactions;
            //List<Transaction> allTran = itsDAL.transactionDB;
            //if (memberTran != null)
            //    foreach (int _id in memberTran)
            //    {
            //        for (int i = allTran.Count - 1; i >= 0; i --)
            //        {
            //            if (allTran[i].ID == _id)
            //                itsDAL.delete(allTran[i]);
            //        }
            //    }
            itsDAL.delete(c);
            return true;
        }

        private int InitializeID()
        {
            List<ClubMember> temp = itsDAL.clubmemberDB;
            if (temp.Count() != 0)
                return temp[temp.Count() - 1].MemberID + 1;
            return 1;
        }

        public String Add(String _id, String _firstName, String _lastName, String _username, String _password, String _gender, String _creditCard, DateTime _birthday)
        {

            bool canAdd = true;
            String str = "";
            if (Exist(_id))
            {
                str = str + "Club Member with this ID already exists!\n";
                canAdd = false;
            }
            if (UserExist(_username))
            {
                str = str + "This username already exists!\n";
                canAdd = false;
            }
            if (canAdd)
            {

                itsDAL.Add(new ClubMember(InitializeID(), _username, _password, _id, _firstName, _lastName, _gender, _creditCard, _birthday, new List<int>()));
            }
            return str;
        }

        public String Edit(ClubMember toEdit, String _id, String _firstName, String _lastName, String _username, String _password, String _gender, String _creditCard, DateTime _birthday)
        {
            String str = "";
            List<ClubMember> temp = itsDAL.clubmemberDB;
            if (temp != null)
                foreach (ClubMember c in temp)
                {
                    if (c.ID.Equals(_id) && c.MemberID != toEdit.MemberID)
                        str = str + "Club Member with such ID already exists!";
                    if (c.Username.Equals(_username) && c.MemberID != toEdit.MemberID)
                        str = str + "Username already exists!";
                }
            List<Employee> temp1 = itsDAL.employeeDB;
            foreach (Employee c in temp1)
            {
                if (c.Username.Equals(_username))
                    str = str + "Username already exists!";
            }
            if (str.Length == 0)
            {
                itsDAL.edit(toEdit, _id, _firstName, _lastName, _username, _password, _gender, _creditCard,_birthday);
            }
            return str;
        }


        public bool Exist(int _id)
        {
            List<ClubMember> temp = itsDAL.clubmemberDB;
            if (temp.Count != 0)
            {
                var result = from c in temp where c.MemberID == _id select c;
                return result.Count() > 0;
            }
            return false;
        }

        public bool Exist(String _id)
        {
            List<ClubMember> temp = itsDAL.clubmemberDB;
            if (temp.Count != 0)
            {
                var result = from c in temp where c.ID.Equals(_id) select c;
                return result.Count() > 0;
            }
            return false;
        }

        private ClubMember GetByID(int id)
        {
            List<ClubMember> temp = itsDAL.clubmemberDB;
            if (temp != null)
                foreach (ClubMember c in temp)
                {
                    if (c.MemberID == id)
                        return c;
                }
            return null;
        }

        public void AddCreditCard(ClubMember toEdit, String creditCard)
        {
            itsDAL.AddCreditCard(toEdit, creditCard);
        }

        public List<ClubMember> GetAll()
        {
            return itsDAL.clubmemberDB;
        }

        public List<ClubMember> SearchByFirstName(String _fname)
        {
            List<ClubMember> ans = new List<ClubMember>();
            List<ClubMember> temp = itsDAL.clubmemberDB;
            if (temp.Count != 0)
            {
                var result = from c in temp where c.FirstName.Equals(_fname) select c;
                ans = result.ToList<ClubMember>();

            }
            return ans;
        }

        public List<ClubMember> SearchByUsername(String _fname)
        {
            List<ClubMember> ans = new List<ClubMember>();
            List<ClubMember> temp = itsDAL.clubmemberDB;
            if (temp.Count != 0)
            {
                var result = from c in temp where c.Username.Equals(_fname) select c;
                ans = result.ToList<ClubMember>();

            }
            return ans;
        }

        public List<ClubMember> SearchByLastName(String _lname)
        {
            List<ClubMember> ans = new List<ClubMember>();
            List<ClubMember> temp = itsDAL.clubmemberDB;
            if (temp.Count != 0)
            {
                var result = from c in temp where c.LastName.Equals(_lname) select c;
                ans = result.ToList<ClubMember>();
            }
            return ans;
        }

        public List<ClubMember> SearchByGender(String _g)
        {
            List<ClubMember> ans = new List<ClubMember>();
            List<ClubMember> temp = itsDAL.clubmemberDB;
            if (temp.Count != 0)
            {
                var result = from c in temp where c.GetGender().Equals(_g) select c;
                ans = result.ToList<ClubMember>();
            }
            return ans;
        }

        public List<ClubMember> SearchByID(String _id)
        {
            List<ClubMember> ans = new List<ClubMember>();
            List<ClubMember> temp = itsDAL.clubmemberDB;
            if (temp.Count != 0)
            {
                var result = from c in temp where c.ID.Equals(_id) select c;
                ans = result.ToList<ClubMember>();
            }
            return ans;
        }

        public List<ClubMember> SearchByID(int _id)
        {
            List<ClubMember> ans = new List<ClubMember>();
            List<ClubMember> temp = itsDAL.clubmemberDB;
            if (temp.Count != 0)
            {
                var result = from c in temp where c.MemberID == _id select c;
                ans = result.ToList<ClubMember>();
            }
            return ans;
        }

        public ClubMember GetByUser(String _username, String _password)
        {
            List<ClubMember> temp = itsDAL.clubmemberDB;
            if (temp != null)
                foreach (ClubMember c in temp)
                    if (c.Username.Equals(_username) && c.Password.Equals(_password))
                        return c;
            return null;
        }

        public bool UserExist(String _username)
        {
            List<ClubMember> temp = itsDAL.clubmemberDB;
            if (temp.Count != 0)
            {
                var result = from u in temp where u.Username.Equals(_username) select u;
                if (result.Count() > 0)
                    return true;
            }
            List<Employee> temp1 = itsDAL.employeeDB;
            if (temp1.Count != 0)
            {
                var result = from u in temp1 where u.Username.Equals(_username) select u;
                if (result.Count() > 0)
                    return true;
            }
            return false;
        }
    }
}