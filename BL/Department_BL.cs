﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using Backend;

namespace BL
{
    public class Department_BL : IBL
    {
        SQL_DAL itsDAL;

        public Department_BL(SQL_DAL dal)
        {
            itsDAL = dal;
        }

        public Department GetByID(int id)
        {
            List<Department> temp = itsDAL.departmentDB;
            if(temp!=null)
            foreach (Department d in temp)
            {
                if (d.ID == id)
                    return d;
            }
            return null;
        }

        public bool Delete(int id)
        {
            if(id!=1)
            {
                Department c = GetByID(id);
                if (c == null)
                    return false;
                itsDAL.delete(c);
                return true;
            }
            return false;
        }

        private int InitializeID()
        {
            List<Department> temp = itsDAL.departmentDB;
            if (temp.Count() != 0)
                return temp[temp.Count() - 1].ID + 1;
            return 1;
        }

        public String Add(String name)
        {
            String str = "";
            if (!Exist(name))
            {
                itsDAL.Add(new Department(InitializeID(), name));
            }
            else
            {
                str = "Department already exists!";
            }
            return str;
        }

        public String EditName(int _id, String _name)
        {
            String str = "";
            List<Department> temp = itsDAL.departmentDB;
            Department toEdit = GetByID(_id);
            if(temp!=null)
            foreach (Department d in temp)
            {
                if (d.Name.Equals(_name) && d.ID != _id)
                    str = str + "Department with such name already exists!";
            }
            if (str.Length == 0)
            {
             //   toEdit.Name =_name;
                itsDAL.edit( toEdit,  _name);
            }
            return str;
        }

        public bool Exist(String _name)
        {
            List<Department> temp = itsDAL.departmentDB;
            if (temp.Count != 0)
            {
                var result = from d in temp where d.Name.Equals(_name) select d;
                return result.Count() > 0;
            }
            return false;
        }

        public bool Exist(int _id)
        {
            List<Department> temp = itsDAL.departmentDB;
            if (temp.Count != 0)
            {
                var result = from d in temp where d.ID == _id select d;
                return result.Count() > 0;
            }
            return false;
        }  

        public List<Department> GetAll()
        {
            return itsDAL.departmentDB;
        }

        public List<Department> SearchByName(String _name)
        {
            List<Department> ans = new List<Department>();
            List<Department> temp = itsDAL.departmentDB;
            if (temp.Count != 0)
            {
                var result = from d in temp where d.Name.Equals(_name) select d;
                ans = result.ToList<Department>();
            }
            return ans;
        }

        public List<Department> SearchByID(int _id)
        {
            List<Department> ans = new List<Department>();
            List<Department> temp = itsDAL.departmentDB;
            if (temp.Count != 0)
            {
                var result = from d in temp where d.ID == _id select d;
                ans = result.ToList<Department>();
            }
            return ans;
        }
    }
}
