﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using Backend;

namespace BL
{
    public class Transaction_BL
    {
        SQL_DAL itsDAL;        

        public Transaction_BL(SQL_DAL dal)
        {
            itsDAL = dal;
        }

        public bool Delete(int id)
        {
            Transaction t = GetByID(id);
            if (t == null)
                return false;
            itsDAL.delete(t);
            return true;
        }

        private int InitializeID()
        {
            List<Transaction> temp = itsDAL.transactionDB;
            if (temp.Count() != 0)
                return temp[temp.Count() - 1].ID + 1;
            return 1;
        }
        public bool add(String s, List<Receipt> Rec, double Value, ClubMember c)
        {

                int id = InitializeID();
                DateTime date = DateTime.Now;
                bool b = false;
                String payment = s;
                Transaction t = new Transaction(id, b, payment, date);
                t.ReceiptList = Rec;
                t.Value = Value;
                itsDAL.Add(t);
                if (c != null)
                {
                    itsDAL.attachtran(c.MemberID,t.ID);
                   
                }
                return true;
            

        }

        

        public String Edit(Transaction toEdit, DateTime newDate, String payment, bool isReturned)
        {
            String str = "";
            toEdit.SetPaymentMethod(payment);
            toEdit.Date = newDate;
            if(toEdit.Is_a_return==false && isReturned)
            {
                returnTransaction(toEdit);
            }
            itsDAL.edit(toEdit, newDate, payment, isReturned);
            return str;
        }

        //public int Add(bool _is_a_return, DateTime _d)
        //{
        //    int id = InitializeID();
        //    itsDAL.Add(new Transaction(id, _is_a_return, "CreditCard", _d));
        //    return id;
        //}

        public bool IsAReturn(int id)
        {
            Transaction t = GetByID(id);
            if(t!=null)
            {
                return t.Is_a_return;
            }
            return false;
        }

        //public String AddToPurchase(int tranID, int proID, int amount)
        //{
        //    string str = "";
        //    bool proExist = false;
        //    bool amountExist = false;
        //    Transaction t = GetByID(tranID);
        //    if (t!=null)
        //    {
        //        List<Product> allPro = itsDAL.productDB;
        //        if(allPro!=null)
        //        foreach (Product p in allPro)
        //        {
        //            if (p.ProductID == proID)
        //                proExist = true;
        //            if (p.ProductID == proID && p.InStock >= amount)
        //            {
        //                amountExist = true;
        //                p.InStock=p.InStock-amount;
        //                itsDAL.edit(p);
        //                t.addToReceipt(proID, amount, p.Price);
        //                itsDAL.edit(t);
        //            }
        //        }
        //        if (!proExist) str = str + "Product does not exist!\n";
        //        if (proExist && !amountExist) str = str + "Product exists but amount in stock is not enough!\n";
        //    }
        //    else
        //        str = str + "Transaction does not exist!\n";
        //    return str;
        //}

        //public String RemoveFromPurchase(int tranID, int proID, int amount)
        //{
        //    string str = "";
        //    bool proExist = false;
        //    Transaction t = GetByID(tranID);
        //    if (t != null)
        //    {
        //        List<Product> allPro = itsDAL.productDB;
        //        if(allPro!=null)
        //        foreach (Product p in allPro)
        //        {
        //            if (p.ProductID == proID)
        //            {
        //                proExist = true;
        //                if (t.removeFromReceipt(proID, amount))
        //                {
        //                    p.InStock=p.InStock + amount;
        //                    itsDAL.edit(p);
        //                    itsDAL.edit(t);
        //                }
        //                else
        //                    str = str + "This amount doesn't exist for this product in this transaction.\n";
        //            }       
        //        }
        //        if (!proExist) str = str + "Product does not exist!\n";
        //    }
        //    else
        //        str = str + "Transaction does not exist!\n";
        //    return str;
        //}

        public bool returnTransaction(Transaction t)
        {
            if (t != null && !t.Is_a_return)
            {
                itsDAL.returnTran(t);
                return true;
            }
            return false;
        }

        //public String AddToReturn(int tranID, int proID, int amount)
        //{
        //    string str = "";
        //    bool proExist = false;
        //    Transaction t = GetByID(tranID);
        //    if (t!=null)
        //    {
        //        List<Product> allPro = itsDAL.productDB;
        //        if(allPro!=null)
        //        foreach (Product p in allPro)
        //        {
        //            if (p.ProductID == proID)
        //            {
        //                proExist = true;
        //                p.InStock=p.InStock+amount;
        //                itsDAL.edit(p);
        //                t.addToReceipt(proID, amount, p.Price);
        //                itsDAL.edit(t);
        //            }
        //        }
        //        if (!proExist) str = str + "Product does not exist!\n";
        //    }
        //    else
        //        str = str + "Transaction does not exist!\n";
        //    return str;
        //}

        //public String RemoveFromReturn(int tranID, int proID, int amount)
        //{
        //    string str = "";
        //    bool proExist = false;
        //    Transaction t = GetByID(tranID);
        //    if (t != null)
        //    {
        //        List<Product> allPro = itsDAL.productDB;
        //        if(allPro!=null)
        //        foreach (Product p in allPro)
        //        {
        //            if (p.ProductID == proID)
        //            {
        //                proExist = true;
        //                if (p.InStock >= amount) 
        //                {
        //                    if (t.removeFromReceipt(proID, amount))
        //                    {
        //                        p.InStock=p.InStock - amount;
        //                        itsDAL.edit(p);
        //                        itsDAL.edit(t);
        //                    }
        //                    else
        //                        str = str + "This amount doesn't exist for this product in this transaction.\n";
        //                }
        //                else
        //                    str = str + "Amount in stock is lower and can't be given back.\n";
        //            }
        //        }
        //        if (!proExist) str = str + "Product does not exist!\n";
        //    }
        //    else
        //        str = str + "Transaction does not exist!\n";
        //    return str;
        //}

        public Transaction GetByID(int id)
        {
            List<Transaction> temp = itsDAL.transactionDB;
            if(temp!=null)
            foreach (Transaction t in temp)
            {
                if (t.ID == id)
                    return t;
            }
            return null;
        }

        //public String GetReceiptAsString(int id)
        //{
        //    List<Transaction> allTrans = itsDAL.transactionDB;
        //    Transaction t = GetByID(id);
        //    if(t!=null)
        //    {
        //        List<Receipt> temp = t.ReceiptList;
        //        List<Product> allPro = itsDAL.productDB;
        //        if (temp.Count > 0 && allPro.Count > 0)
        //        {
        //            String ans = "";
        //            var result = from r in temp join pro in allPro on r.ProductID equals pro.ProductID select new { proID = pro.ProductID, proName = pro.Name, proPrice = r.Price, amount = r.Amount };
        //            if(result!=null)
        //            foreach (var v in result)
        //            {
        //                ans = ans + v.proID + ", " + v.proName + " Price: " + v.proPrice + ",   X" + v.amount + "\n";
        //            }
        //            ans = ans + "TOTAL: " + t.Value;
        //            return ans;
        //        }
        //        return "Transaction is empty!";
        //    }
        //    return "Transaction does not exist!";
        //}

        public bool Exist(int _id)
        {
            List<Transaction> temp = itsDAL.transactionDB;
            if (temp.Count != 0)
            {
                var result = from t in temp where t.ID == _id select t;
                return result.Count() > 0;
            }
            return false;
        }

        public List<Transaction> GetAll()
        {
            return itsDAL.transactionDB;
        }

        public List<Transaction> SearchBetween(List<Transaction> _temp,DateTime d1, DateTime d2)
        {
            List<Transaction> ans = new List<Transaction>();
            List<Transaction> temp = _temp;
            if (temp.Count != 0)
            {
                var result = from t in temp where t.Date >= d1 && t.Date <= d2 select t;
                ans = result.ToList<Transaction>();
            }
            return ans;
        }

        public List<Transaction> SearchByID(List<Transaction> _temp,int _id)
        {
            List<Transaction> ans = new List<Transaction>();
            List<Transaction> temp = _temp;
            if (temp.Count != 0)
            {
                var result = from t in temp where t.ID == _id select t;
                ans = result.ToList<Transaction>();
            }
            return ans;
        }

        public List<Transaction> SearchByPaymentMethod(List<Transaction> _temp,String pm)
        {
            List<Transaction> ans = new List<Transaction>();
            List<Transaction> temp = _temp;
            if (temp.Count != 0)
            {
                var result = from t in temp where t.GetPaymentMethod().Equals(pm) select t;
                ans = result.ToList<Transaction>();
            }
            return ans;
        }

        public List<Transaction> GetMemberTran(List<Transaction> _temp,ClubMember c)
        {
            List<Transaction> ans = new List<Transaction>();
            List<Transaction> alltran = _temp;
            List<int> tran = c.Transactions;
            foreach(int i in tran)
            {
                foreach(Transaction t in alltran)
                {
                    if (t.ID == i)
                        ans.Add(t);
                }
            }
            return ans;
        }

        //public String EditPaymentMethod(int toEditID, String newMethod)
        //{
        //    Transaction toEdit = GetByID(toEditID);
        //    toEdit.SetPaymentMethod(newMethod);
        //    itsDAL.edit(toEdit);
        //    return "";
        //}

        //public String EditDate(int toEditID, DateTime newDate)
        //{
        //    Transaction toEdit = GetByID(toEditID);
        //    toEdit.Date=newDate;
        //    itsDAL.edit(toEdit);
        //    return "";
        //}


    }
}
