﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Backend;
using DAL;

namespace BL
{
    public interface IBL
    {
        bool Delete(int id);
        bool Exist(int _id);
    }
}
