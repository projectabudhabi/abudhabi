﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;

namespace BL
{
    public class AllBL : IBL
    {
        protected LINQ_DAL itsDAL;

        public AllBL(LINQ_DAL _dal) 
        {
            itsDAL = _dal;
          
        }

        public void Add(Object o)
        {
            itsDAL.Add(o);
        }

        //public void Delete(Object o)
        //{
        //    itsDAL.Delete(o);
        //}
    }
}
