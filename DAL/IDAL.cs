﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Backend;

namespace DAL
{
    public interface IDAL
    {
        void fillClubMember();
        void fillTransaction();
        void fillProduct();
        void fillEmployee();
        void fillDepartment();
        void edit(Employee toEdit, String _type, String _username, String _password, String _id, String _firstName, String _lastName, String _gender, int _departmentID, int _salary, String _supervisor, DateTime _birthday);
        void edit(ClubMember toEdit, String _id, String _firstName, String _lastName, String _username, String _password, String _gender, String _creditCard, DateTime _birthday);
        void edit(Product toEdit, String _name, String _type, int _departmentID, int _inStock, double _price);
        void edit(Department toEdit, String _name);
        void edit(Transaction toEdit, DateTime newDate, String payment, bool isReturned);
        void Add(Object o);
        void delete(Object o);


    }
}
