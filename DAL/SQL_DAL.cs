﻿using Backend;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Security.Cryptography;
using System.Data.Linq;
using System.Data.Linq.Mapping;
namespace DAL
{
    public class SQL_DAL : IDAL
    {

        public DataClasses1DataContext db { get; private set; }
        public List<Product> productDB { get; private set; }
        public List<ClubMember> clubmemberDB { get; private set; }
        public List<Department> departmentDB { get; private set; }
        public List<Employee> employeeDB { get; private set; }
        public List<Transaction> transactionDB { get; private set; }
        //private MasterKey master;

        public SQL_DAL()
        {
            db = new DataClasses1DataContext();
            productDB = new List<Product>();
            clubmemberDB = new List<ClubMember>();
            departmentDB = new List<Department>();
            employeeDB = new List<Employee>();
            transactionDB = new List<Transaction>();
            fillClubMember();
            fillDepartment();
            fillEmployee();
            fillProduct();
            fillTransaction();
            if (departmentDB.Count() == 0)
            {
                Department d = new Department(1, "Default");
                Add(d);
            }
            if (employeeDB.Count() == 0)
            { 
            Employee e = new Employee(1, "Administrator", "admin123", "admin123", "000000000", "Admin", "Admin", "Male", 1, 1, "000000000", DateTime.Today);
            Add(e);
             }
        }
        private int InitializeID()
        {

            if (db.ReceiptsTables.Count() != 0)
            {
                int i = 0;
                foreach (ReceiptsTable r in db.ReceiptsTables)
                {
                    if (i == db.ReceiptsTables.Count() - 1)
                        return r.ID + 1;
                    i++;
                }
            }
            return 1;
        }
        public void fillClubMember()
        {
            clubmemberDB.Clear();
            foreach (ClubMembersTable c in db.ClubMembersTables)
            {
               
                List<int> gopro = new List<int>();
                foreach (TransactionsTable r in db.TransactionsTables)
                {
                    if (r.ClubMemberID == c.MemberID)
                    {
                        gopro.Add(r.ID);
                    }
                }
                ClubMember temp = new ClubMember(c.MemberID, c.Username, c.Password, c.ID, c.FirstName, c.LastName, c.Gender, c.CreditCard, c.Birthday, gopro);
                clubmemberDB.Add(temp);

            }
        }
        public void fillTransaction()
        {
            transactionDB.Clear();
            foreach (TransactionsTable c in db.TransactionsTables)
            {
                Transaction temp=new Transaction(c.ID,c.Is_a_Return,c.PaymentMethod,c.Date);
                temp.Value = c.Value;
                List<Receipt> gopro = new List<Receipt>();
                foreach (ReceiptsTable r in db.ReceiptsTables)
                {
                    if (r.TransactionID == c.ID)
                    {
                        gopro.Add(new Receipt(r.ProductID, r.Amount, r.Price));
                    }
                }
                temp.ReceiptList=gopro;
                transactionDB.Add(temp);
               
            }
        }
        public void fillProduct()
        {
            productDB.Clear();
            foreach(ProductsTable c in db.ProductsTables)
                {
                  
                    productDB.Add(new Product(c.ID,c.Name,c.Type,c.LocationID,c.InStock,c.Price));
                }
        }
         public void fillEmployee()
        {
            employeeDB.Clear();
            foreach (EmployeesTable c in db.EmployeesTables)
            {

                employeeDB.Add(new Employee(c.EmpID,c.Type,c.Username,c.Password,c.ID,c.FirstName,c.LastName,c.Gender,c.DepartmentID,c.Salary,c.Supervisor,c.Birthday));
            }
        }
        public void fillDepartment()
        {
            departmentDB.Clear();
            foreach (DepartmentTable c in db.DepartmentTables)
            {

                departmentDB.Add(new Department(c.ID,c.Name));
            }
        }

        public void AddCreditCard(ClubMember toEdit, String creditCard)
        {
           foreach (ClubMembersTable c in db.ClubMembersTables)
           {
               if (toEdit.MemberID == c.MemberID)
               {
                   c.CreditCard = creditCard;
               }
           }
           save();
           toEdit.CreditCard = creditCard;
          // fillClubMember();

        }

        public void edit(Employee toEdit, String _type, String _username, String _password, String _id, String _firstName, String _lastName, String _gender, int _departmentID, int _salary, String _supervisor, DateTime _birthday)
        {
            foreach (EmployeesTable t in db.EmployeesTables)
            {
                if (t.EmpID == toEdit.EmpID)
                {
                    t.Birthday = _birthday;
                    t.DepartmentID = _departmentID;
                    t.FirstName = _firstName;
                    t.Gender = _gender;
                    t.ID = _id;
                    t.LastName = _lastName;
                    t.Password = _password;
                    t.Salary = _salary;
                    t.Supervisor = _supervisor;
                    t.Type = _type;
                    t.Username = _username;
                }
            }
            save();

            toEdit.Birthday = _birthday;
            toEdit.DepartmentID = _departmentID;
            toEdit.FirstName = _firstName;
            toEdit.SetGender(_gender);
            toEdit.ID = _id;
            toEdit.LastName = _lastName;
            toEdit.Password = _password;
            toEdit.Salary = _salary;
            toEdit.Supervisor = _supervisor;
            toEdit.SetType(_type);
            toEdit.Username = _username;
          //  fillEmployee();
        }

        public void edit(ClubMember toEdit, String _id, String _firstName, String _lastName, String _username, String _password, String _gender, String _creditCard, DateTime _birthday)
        {
            foreach(ClubMembersTable c in db.ClubMembersTables)
            {
                if(toEdit.MemberID==c.MemberID)
                {
                    c.ID = _id;
                    c.FirstName = _firstName;
                    c.LastName = _lastName;
                    c.Username = _username;
                    c.Password = _password;
                    c.Gender = _gender;
                    c.CreditCard = _creditCard;
                    c.Birthday = _birthday;
                }
            }
            save();

            toEdit.ID = _id;
            toEdit.FirstName = _firstName;
            toEdit.LastName = _lastName;
            toEdit.Username = _username;
            toEdit.Password = _password;
            toEdit.SetGender(_gender);
            toEdit.CreditCard = _creditCard;
            toEdit.Birthday = _birthday;

         //   fillClubMember();
        }

        public void edit(Product toEdit, String _name, String _type, int _departmentID, int _inStock, double _price)
        {
            foreach(ProductsTable p in db.ProductsTables)
            {
                if(toEdit.ProductID == p.ID)
                {
                    p.Name = _name;
                    p.Type = _type;
                    p.LocationID = _departmentID;
                    p.InStock = _inStock;
                    p.Price = _price;
                }
            }
            save();

            toEdit.Name = _name;
            toEdit.SetProType(_type);
            toEdit.LocationID = _departmentID;
            toEdit.InStock = _inStock;
            toEdit.Price = _price;
        //    fillProduct();

        }
        public void edit(Department toEdit, String _name)
        {
            foreach (DepartmentTable t in db.DepartmentTables)
            {
                if (t.ID == toEdit.ID)
                    t.Name = _name;
            }
            save();

            toEdit.Name = _name;
        //    fillDepartment();
        }
        public void edit(Transaction toEdit, DateTime newDate, String payment, bool isReturned)
        {
            foreach (TransactionsTable t in db.TransactionsTables)
            {
                if (toEdit.ID == t.ID)
                {
                    t.Date = newDate;
                    t.PaymentMethod = payment;
                    t.Is_a_Return = isReturned;
                }
            }
            save();

            toEdit.Date = newDate;
            toEdit.SetPaymentMethod(payment);
            toEdit.Is_a_return = isReturned;
        //    fillTransaction();
        }

        private void save()
        {
            db.SubmitChanges();
        }
        public void attachtran(int idc,int idt)
        {
            foreach(TransactionsTable t in db.TransactionsTables)
            {
                if (t.ID == idt)
                    t.ClubMemberID = idc;
            }
            save();
            foreach(ClubMember c in clubmemberDB)
            {
                if (c.MemberID == idc)
                    c.Transactions.Add(idt);
            }
         //   fillClubMember();
         //   fillTransaction();
        }
        public void Add(Object o)
        {
            if (o is ClubMember)
            {
                ClubMember tempo = (ClubMember)o;
                ClubMembersTable temp = new ClubMembersTable();
                temp.Birthday = tempo.Birthday;
                temp.CreditCard = tempo.CreditCard;
                temp.FirstName = tempo.FirstName;
                temp.Gender = tempo.GetGender();
                temp.ID = tempo.ID;
                temp.LastName = tempo.LastName;
                temp.MemberID = tempo.MemberID;
                temp.Password = tempo.Password;
                temp.Username = tempo.Username;
                db.ClubMembersTables.InsertOnSubmit(temp);
                save();
                clubmemberDB.Add(tempo);
             //   fillClubMember();
            }
            else if (o is Department)
            {
                Department tempo = (Department)o;
                DepartmentTable temp = new DepartmentTable();
                temp.ID = tempo.ID;
                temp.Name = tempo.Name;
                db.DepartmentTables.InsertOnSubmit(temp);
                save();
                departmentDB.Add(tempo);
            //    fillDepartment();
            }
            else if (o is Employee)
            {
                Employee tempo = (Employee)o;
                EmployeesTable temp = new EmployeesTable();
                temp.Birthday = tempo.Birthday;
                temp.DepartmentID = tempo.DepartmentID;
                temp.Salary = tempo.Salary;
                temp.Supervisor = tempo.Supervisor;
                temp.Type = tempo.GetType();
                temp.FirstName = tempo.FirstName;
                temp.Gender = tempo.GetGender();
                temp.ID = tempo.ID;
                temp.LastName = tempo.LastName;
                temp.EmpID = tempo.EmpID;
                temp.Password = tempo.Password;
                temp.Username = tempo.Username;
                db.EmployeesTables.InsertOnSubmit(temp);
                save();
                employeeDB.Add(tempo);
           //     fillEmployee();
            }
            else if (o is Product)
            {
                Product tempo = (Product)o;
                ProductsTable temp = new ProductsTable();
                temp.ID = tempo.ProductID;
                temp.InStock = tempo.InStock;
                temp.LocationID = tempo.LocationID;
                temp.Name = tempo.Name;
                temp.Price = tempo.Price;
                temp.Type = tempo.GetProType();
                db.ProductsTables.InsertOnSubmit(temp);
                save();
                productDB.Add(tempo);
            //    fillProduct();
            }
            else if (o is Transaction)
            {

                Transaction tempo = (Transaction)o;
                TransactionsTable temp = new TransactionsTable();
                temp.Date = tempo.Date;
                temp.Value = tempo.Value;
                temp.Is_a_Return = tempo.Is_a_return;
                temp.PaymentMethod = tempo.GetPaymentMethod();
                temp.ID = tempo.ID;
                temp.ClubMemberID = -1;
                foreach(Receipt r in tempo.ReceiptList)
                {
                    ReceiptsTable recTemp = new ReceiptsTable();
                    recTemp.ID = InitializeID();
                    recTemp.TransactionID = temp.ID;
                    recTemp.Price = r.Price;
                    recTemp.Amount = r.Amount;
                    recTemp.ProductID = r.ProductID;
                    foreach (ProductsTable p in db.ProductsTables)
                    {
                        if (p.ID == r.ProductID)
                            p.InStock -= r.Amount;
                    }
                    db.ReceiptsTables.InsertOnSubmit(recTemp);
                    save();
                }
                db.TransactionsTables.InsertOnSubmit(temp);
                save();
                transactionDB.Add(tempo);
                foreach (Receipt r in tempo.ReceiptList)
                {
                    foreach (Product p in productDB)
                    {
                        if (p.ProductID == r.ProductID)
                            p.InStock -= r.Amount;
                    }
                }
             //   fillTransaction();
             //   fillClubMember();
             //   fillProduct();
            }
           
        }

        

        public void returnTran(Transaction t1)
        {
            TransactionsTable temp = null;
            foreach (TransactionsTable t2 in db.TransactionsTables)
            {
                if (t2.ID == t1.ID)
                {
                    temp = t2;
                }
            }
            temp.Is_a_Return = true;
            foreach(ReceiptsTable r in db.ReceiptsTables)
            {
                if(r.TransactionID==temp.ID)
                {
                    foreach(ProductsTable p in db.ProductsTables)
                    {
                        if (p.ID == r.ProductID)
                            p.InStock += r.Amount;
                    }
                }
            }
            save();

            t1.Is_a_return = true;
            foreach(Receipt r in t1.ReceiptList)
            {
                foreach(Product p in productDB)
                {
                    if (p.ProductID == r.ProductID)
                        p.InStock += r.Amount;
                }
            }
         //   fillProduct();
         //   fillTransaction();
        }

        public void delete(Object o)
        {
            try
            {
                if (o is ClubMember)
                {
                    ClubMember tempo = (ClubMember)o;
                    ClubMembersTable temp = new ClubMembersTable();
                    foreach(ClubMembersTable c in db.ClubMembersTables)
                    {
                        if (c.MemberID == tempo.MemberID)
                            temp = c;
                    }
                    foreach (TransactionsTable t in db.TransactionsTables)
                    {
                        if (t.ClubMemberID == temp.MemberID)
                            t.ClubMemberID = -1;
                    }
                    db.ClubMembersTables.DeleteOnSubmit(temp);
                    save();
                    db = new DataClasses1DataContext();
                    clubmemberDB.Remove(tempo);
                //    fillTransaction();
                //    fillClubMember();
                }
                    
                else if (o is Department)
                {
                    Department tempo = (Department)o;
                    DepartmentTable temp = new DepartmentTable();
                    foreach (DepartmentTable c in db.DepartmentTables)
                    {
                        if (c.ID == tempo.ID)
                            temp = c;
                    }
                    
                    db.DepartmentTables.DeleteOnSubmit(temp);
                    save();
                    db = new DataClasses1DataContext();

                    foreach (Product p in productDB)
                    {
                        if (tempo.ID == p.LocationID)
                            p.LocationID = 1;
                    }
                    foreach (Employee e in employeeDB)
                    {
                        if (e.DepartmentID == tempo.ID)
                            e.DepartmentID = 1;
                    }
                    departmentDB.Remove(tempo);
               //     fillDepartment();
               //     fillProduct();
               //     fillEmployee();
                }
                else if (o is Employee)
                {
                    Employee tempo = (Employee)o;
                    EmployeesTable temp = new EmployeesTable();
                    foreach (EmployeesTable c in db.EmployeesTables)
                    {
                        if (c.EmpID == tempo.EmpID)
                            temp = c;
                    }
                    foreach (EmployeesTable k in db.EmployeesTables)
                    {
                        if (k.Supervisor.Equals(temp.ID))
                            k.Supervisor = employeeDB.ElementAt(0).ID;
                    }
                    db.EmployeesTables.DeleteOnSubmit(temp);
                    save();
                    db = new DataClasses1DataContext();

                    foreach(Employee e in employeeDB)
                    {
                        if (e.Supervisor.Equals(tempo.ID))
                            e.Supervisor = employeeDB.ElementAt(0).ID;
                    }
                    employeeDB.Remove(tempo);
                //    fillEmployee();
                }
                else if (o is Product)
                {
                    Product tempo = (Product)o;
                    ProductsTable temp = new ProductsTable();
                    foreach (ProductsTable c in db.ProductsTables)
                    {
                        if (c.ID == tempo.ProductID)
                            temp = c;
                    }
                    for (int t = transactionDB.Count - 1; t >= 0;t-- )
                    {
                        for (int r = transactionDB.ElementAt(t).ReceiptList.Count-1;r>=0;r--)
                        {
                            if (transactionDB.ElementAt(t).ReceiptList.ElementAt(r).ProductID == tempo.ProductID)
                            {
                                transactionDB.ElementAt(t).Value -= transactionDB.ElementAt(t).ReceiptList.ElementAt(r).Price * transactionDB.ElementAt(t).ReceiptList.ElementAt(r).Amount;
                                transactionDB.ElementAt(t).ReceiptList.RemoveAt(r);
                            }
                        }
                        if (transactionDB.ElementAt(t).Value == 0)
                        {
                            foreach (ClubMember c in clubmemberDB)
                            {
                                if (c.Transactions.Contains(transactionDB.ElementAt(t).ID))
                                    c.Transactions.Remove(transactionDB.ElementAt(t).ID);
                            }
                            transactionDB.RemoveAt(t);
                        }
                    }
                    foreach(TransactionsTable t in db.TransactionsTables)
                    {
                        foreach(ReceiptsTable r in db.ReceiptsTables)
                        {
                            if (r.TransactionID == t.ID && r.ProductID == tempo.ProductID)
                                t.Value -= r.Price * r.Amount; 
                        }
                        if (t.Value == 0)
                            db.TransactionsTables.DeleteOnSubmit(t);
                        
                    }
                    db.ProductsTables.DeleteOnSubmit(temp);
                    save();
                    db = new DataClasses1DataContext();
                    productDB.Remove(tempo);

                    //fillProduct();
                    //fillTransaction();
                }
                else if (o is Transaction)
                {
                    Transaction tempo = (Transaction)o;
                    TransactionsTable temp = new TransactionsTable();
                    foreach(ReceiptsTable r in db.ReceiptsTables)
                    {
                        if(r.TransactionID==tempo.ID)
                            db.ReceiptsTables.DeleteOnSubmit(r);

                    }
                    save();
                    foreach (TransactionsTable c in db.TransactionsTables)
                    {
                        if (c.ID == tempo.ID)
                            temp = c;
                    }
                    db.TransactionsTables.DeleteOnSubmit(temp);
                    save();
                    db = new DataClasses1DataContext();

                    foreach (ClubMember c in clubmemberDB)
                    {
                        if (c.Transactions.Contains(tempo.ID))
                            c.Transactions.Remove(tempo.ID);
                    }
                    transactionDB.Remove(tempo);
                  //  fillTransaction();
                  //  fillClubMember();
                }
            }
            catch (NullReferenceException nullRefEx)
            {
                Console.WriteLine(nullRefEx.Message);
            }
        }
    }
}
