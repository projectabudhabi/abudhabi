﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using BL;
using DAL;
using Backend;
using System.Transactions;

namespace Test
{
    [TestFixture]
    public class Test_EmpBL
    {
        private SQL_DAL l;
        private Employee_BL e;
        private TransactionScope b;

        [SetUp]
        public void init()
        {
            b = new TransactionScope();
            l = new SQL_DAL();
            e = new Employee_BL(l);
            e.Add("Worker", "eyal123", "eyal123", "204010666", "Dani", "Aviv", "Male", 1, 1000, "000000000", DateTime.Today);
        }
        [TearDown]
        public void TearDown()
        {
            b.Dispose();
        }
        [Test]
        public void TestUserExist()
        {
            Assert.AreEqual(true, e.UserExist("eyal123"));
        }
        [Test]
        public void TestExistID()
        {
            Assert.AreEqual(true, e.Exist(2));
        }
        [Test]
        public void TestExist()
        {
            Assert.AreEqual(true, e.Exist("204010666"));
        }
        [Test]
        public void testEdit()
        {
            Employee temp = e.GetByUser("eyal123", "eyal123");
            if (temp != null)
            {
                e.Edit(temp, "Worker", "eye123", "eyal123", "204010666", "Dani", "Aviv", "Male", 1, 1000, "000000000", DateTime.Today);
                Assert.AreEqual(true, e.UserExist("eye123"));
            }
            else
                Assert.AreEqual(true, true);
        }
    }
}