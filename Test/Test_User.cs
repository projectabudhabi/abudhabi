﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Backend;

namespace Test
{
    [TestFixture]
    public class Test_Users
    {
        private User a;
        private User b;
        [SetUp]
        public void init()
        {
            DateTime d = new DateTime(1991, 3, 31);
            a = new User("eli", "eli", "99", "eli", "5466", "Male", d);
            b = new User("eli1", "eli", "95", "eli", "5466", "Male", d);
        }
        [Test]
        public void testEquals()
        {
            Assert.AreEqual(false, a.Equals(b));
        }
        [Test]
        public void testToString()
        {
            Assert.AreEqual("eli, eli, 99, eli, 5466, Male", a.ToString());
        }
        [Test]
        public void testGetGender()
        {
            Assert.AreEqual("Male", a.GetGender());
        }
        [Test]
        public void testSetGender()
        {
            a.SetGender("Female");
            Assert.AreEqual("Female", a.GetGender());
        }
    }
}
