﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using BL;
using DAL;
using Backend;
using System.Transactions;

namespace Test
{
    [TestFixture]
    public class Test_Department_BL
    {
        private SQL_DAL l;
        private Department_BL d;
        private List<Department> r;
        private TransactionScope b;
        [SetUp]
        public void init()
        {
            b = new TransactionScope();
            l = new SQL_DAL();
            d = new Department_BL(l);
            d.Add("parparim");
            d.Add("kokilida");
            r = d.GetAll();
        }
        [TearDown]
        public void TearDown()
        {
            b.Dispose();

        }
        [Test]
        public void testExistbyName()
        {

            Assert.AreEqual("True", "" + d.Exist("parparim"));
        }
        [Test]
        public void testDelete()
        {
            d.Delete(2);
            Assert.AreEqual(false, d.Exist(2));
        }
        [Test]
        public void testEditName()
        {
            Department g = new Department();
            foreach (Department k in r)
            {
                if (k.Name.Equals("parparim"))
                    g = k;
            }
            d.EditName(1, "lopilo");
            Assert.AreEqual(true, d.Exist("lopilo"));
        }
    }
}
