﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using BL;
using DAL;
using Backend;

namespace Test
{
    [TestFixture]
    public class Test_Backend
    {
        private Department d;
        private Product p;
        private Receipt r;
        private Employee e;
        private ClubMember c;
        private Transaction t;
        [SetUp]
        public void init()
        {
            d = new Department(4,"Shimon");
            p = new Product(7,"Oleg","Food",10,3,40);
            r = new Receipt(1023,10,150);
            e = new Employee(9, "Worker", "Chelsea", "123", "7654321", "Moshe", "Cohen", "Male", 5, 6000, "311765432", new DateTime(1992, 2, 15));
            c = new ClubMember(1, "Dnipro","kjh","1234567","Maor","Cohen","Male","9876123423456789",new DateTime(1992, 2, 15),new List<int>());
            t = new Transaction(123, true, "Credit Card", new DateTime(2010, 3, 16, 18,30,01,023));   
        }

        [Test]
        public void testDepartmentString()
        {
            Assert.AreEqual("ID: 4, Shimon",d.ToString());
        }

        [Test]
        public void testProductString()
        {
            Assert.AreEqual("ID: 7, Oleg, Type: Food, Location: 10, Price: 40, 3 in stock", p.ToString());
        }
        [Test]
        public void testReceiptString()
        {
            Assert.AreEqual("Product: 1023, Price: 150 X10", r.ToString());
        }
        [Test]
        public void testEmployeeString()
        {
            Assert.AreEqual("9: Chelsea, 123, 7654321, Moshe, Cohen, Male, Department ID: 5, Salary: 6000 Supervisor ID: 311765432", e.ToString());
        }
        [Test]
        public void testClubMemberString()
        {
            Assert.AreEqual("1: Dnipro, kjh, 1234567, Maor, Cohen, Male, 15/02/1992,\n Transactions: ", c.ToString());
        }
        [Test]
        public void testTransactionString()
        {
            Assert.AreEqual("ID: 123, 03/16/2010 18:30:01.023: \n"+t.GetReceiptAsString()+"\nretrun via CreditCard\n", t.ToString());
        }
    }
}

