﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using Backend;
//using BL;

//namespace PL
//{
//    public class PL_CLI : IPL
//    {
//        private User_BL itsUserBL;
//        private Product_BL itsProductBL;
//        private Department_BL itsDepartmentBL;
//        private ClubMember_BL itsClubMemberBL;
//        private Employee_BL itsEmployeeBL;
//        private Transaction_BL itsTransactionBL;


//        public PL_CLI(User_BL ubl, Employee_BL ebl, ClubMember_BL cbl, Product_BL pbl, Transaction_BL tbl, Department_BL dbl)
//        {
//            itsUserBL = ubl;
//            itsEmployeeBL = ebl;
//            itsClubMemberBL = cbl;
//            itsProductBL =pbl;
//            itsTransactionBL = tbl;
//            itsDepartmentBL = dbl;
//        }


//        private void SignUp()
//        {
//            string cmd;
//            while (true) 
//            {
//               Console.Clear();
//               Console.WriteLine();
//               Console.WriteLine(String.Format("{0," + Console.WindowWidth / 2 + "}", "***SIGN UP***"));
//               Console.WriteLine();
//               Console.WriteLine("Enter PIN code or press 1 for entrance menu");
//               cmd = Console.ReadLine();
//               Console.Clear();
//               switch (cmd)
//                {
//                    case "1":
//                       return;
//                       break;

//                    case "1234":                       
//                       while(true)
//                       {
//                           Console.Clear();
//                           Console.WriteLine("\nEnter username and password of at least 3 letters or numbers with a ',' between\nor press 1 to return to entrance menu\n(example: UserName,Password)\n");
//                           cmd = Console.ReadLine();
//                           Console.Clear();
//                           string[] temp = cmd.Split(',');
//                           switch (cmd)
//                           {
//                               case "1":
//                                   return;
//                                   break;

//                               default:
//                                   if (temp.Length != 2 || !IsValidString(temp[0]) || !IsValidString(temp[1]))
//                                   {
//                                       Console.WriteLine("\nInvalid input. Press enter to try again.");
//                                       Console.ReadLine();
//                                   }
//                                   else
//                                   {
//                                       String ans = itsUserBL.Add(temp[0], temp[1]);
//                                       if (ans.Length == 0)
//                                       {
//                                           Console.WriteLine("\nUser added! :) Press enter to return to entrance menu");
//                                           Console.ReadLine();
//                                           return;
//                                       }
//                                       else
//                                       {
//                                           Console.WriteLine("\n" + ans + "\nPress enter to try again.");
//                                           Console.ReadLine();
//                                       }
//                                   }
//                                   break;
//                           }
//                       }    
                        
//                 default:
//                       Console.WriteLine("\nInvalid input. Press enter to try again.");
//                     Console.ReadLine();
//                     break;
//               }
//            }
//        }

//        private void login()
//        {
//            string cmd;
//            while (true)
//            {
//                Console.Clear();
//                Console.WriteLine();
//                Console.WriteLine(String.Format("{0," + Console.WindowWidth / 2 + "}", "***LOGIN***"));
//                Console.WriteLine();
//                Console.WriteLine("\nEnter username and password of at least 3 letters or numbers with a ',' between\nor press 1 to return to entrance menu\n (example: UserName,Password)\n");
//                cmd = Console.ReadLine();
//                Console.Clear();
//                string[] temp = cmd.Split(',');
//                switch (cmd)
//                {
//                   case "1":
//                      return;
//                      break;

//                   default:
//                      if (temp.Length != 2)
//                      {
//                        Console.WriteLine("\nInvalid input. Press enter to try again.");
//                        Console.ReadLine();
//                      }
//                      else
//                      {
//                        if (itsUserBL.Exist(temp[0], temp[1]))
//                           Menu();
//                        else
//                        {
//                          Console.WriteLine("\nThis user doesn't exist! Press enter to try again.");
//                          Console.ReadLine();
//                        }
//                       }
//                       break;
//                }
//            }    
//        }

//        private void Menu()
//        {
//            string cmd;
//            while (true)
//            {
//                Console.Clear(); 
//                Console.WriteLine();
//                Console.WriteLine(String.Format("{0," + Console.WindowWidth / 2 + "}", "***MAIN MENU***"));
//                Console.WriteLine();

//                Console.WriteLine("Please select an option:");
//                Console.WriteLine("\t1. Club Member");
//                Console.WriteLine("\t2. Department");
//                Console.WriteLine("\t3. Employee");
//                Console.WriteLine("\t4. Product");
//                Console.WriteLine("\t5. Transaction");
//                Console.WriteLine("\t6. User");
//                Console.WriteLine("\t7. Back to login");
//                cmd = Console.ReadLine();
//                Console.Clear();
//                switch (cmd)
//                {
//                    case "1":
//                        ViewClubMember();
//                        break;

//                    case "2":
//                        ViewDepartment();
//                        break;

//                    case "3":
//                        ViewEmployee();
//                        break;

//                    case "4":
//                        ViewProduct();
//                        break;

//                    case "5":
//                        ViewTransaction();
//                        break;

//                    case "6":
//                        ViewUser();
//                        break;

//                    case "7":
//                        return;
//                        break;

//                    default:
//                        Console.WriteLine("\nThat was an invalid command. Press enter to continue.\n\n");
//                        Console.ReadLine();
//                        break;
//                }
//            }
//         }

//        private void ViewUser()
//        {
//            string cmd;
//            while(true)
//            {
//                Console.Clear(); 
//                Console.WriteLine();
//                Console.WriteLine(String.Format("{0," + Console.WindowWidth / 2 + "}", "***User***"));
//                Console.WriteLine();

//                Console.WriteLine("Please select an option:");
//                Console.WriteLine("\t1. Add");
//                Console.WriteLine("\t2. Edit");
//                Console.WriteLine("\t3. Delete");
//                Console.WriteLine("\t4. Search");
//                Console.WriteLine("\t5. See all Users");
//                Console.WriteLine("\t6. Back to main menu");

//                cmd = Console.ReadLine();
//                Console.Clear(); //clear the screen 

//                switch (cmd)
//                {
//                    case "1":
//                        AddUser();
//                        break;

//                    case "2":
//                        EditUser();
//                        break;

//                    case "3":
//                        DeleteUser();
//                        break;

//                    case "4":
//                        SearchUser();
//                        break;

//                    case "5":
//                        SeeAllUser();
//                        break;

//                    case "6":
//                        Console.Clear();
//                        return;
//                        break;

//                    default:
//                        Console.WriteLine("\nThat was an invalid command. Press enter to continue.\n\n");
//                        Console.ReadLine();
//                        break;
//                }
//            }
//        }

//        private void SearchUser()
//        {
//            string cmd;
//            while(true)
//            {
//              Console.Clear();
//              Console.WriteLine();
//              Console.WriteLine(String.Format("{0," + Console.WindowWidth / 2 + "}", "***Search User***"));
//              Console.WriteLine();
 
//              Console.WriteLine("Please select an option:");
//              Console.WriteLine("\t1. Search by ID");
//              Console.WriteLine("\t2. Search by Username");
//              Console.WriteLine("\t3. Back to 'User's menu'");
//              string ans = "";
//              bool found = false;
//              cmd = Console.ReadLine();
//              Console.Clear();

//                  switch (cmd)
//                  {
//                      case "2":
//                          while (!found)
//                          {
//                              Console.Clear();
//                              Console.WriteLine("\nEnter username to search or press 0 to return:");
//                              cmd = Console.ReadLine();
//                              Console.Clear();
//                              if (cmd.Equals("0"))
//                                  return;
//                              else
//                              {
//                                  ans = itsUserBL.SearchByUsername(cmd);
//                                  if (ans.Length == 0)
//                                  {
//                                      Console.WriteLine("\nSuch user does not exist! Press enter to try again.");
//                                      Console.ReadLine();
//                                  }
//                                  else
//                                  {
//                                      found = true;
//                                  }
//                              }
//                          }
//                          break;

//                      case "1":
//                          while (!found)
//                          {
//                              Console.Clear();
//                              Console.WriteLine("\nEnter ID to search or press 0 to return:");
//                              cmd = Console.ReadLine();
//                              Console.Clear();
//                              if (cmd.Equals("0"))
//                                  return;
//                              else
//                              {
//                                  int idSearch;
//                                  if (int.TryParse(cmd, out idSearch))
//                                  {
//                                      ans = itsUserBL.SearchByID(idSearch);
//                                      if (ans.Length == 0)
//                                      {
//                                          Console.WriteLine("\nSuch user does not exist! Press enter to try again.");
//                                          Console.ReadLine();
//                                      }
//                                      else
//                                      {
//                                          found = true;
//                                      }
//                                  }

//                                  else
//                                  {
//                                      Console.WriteLine("\nInvalid input. Press enter to try again.");
//                                      Console.ReadLine();
//                                  }
//                               }           
//                          }
//                          break;

//                      case "3":
//                          return;
//                          break;

//                      default:
//                          Console.WriteLine("\nInvalid input. Press enter to try again.");
//                          Console.ReadLine();
//                          break;
//                  }
//                if(found)
//                {
//                    found = false;
//                    while(!found)
//                    {
//                        Console.Clear();
//                        Console.WriteLine("\n"+ans);
//                        Console.WriteLine("\nChoose an ID to edit or delete, or press 0 to return:");
//                        cmd = Console.ReadLine();
//                        Console.Clear();
//                        if(cmd.Equals("0"))
//                            return;
//                        int id = 0;
//                        if(int.TryParse(cmd, out id) && itsUserBL.Exist(id))
//                        {
//                            Console.WriteLine("\nChoose an option:");
//                            Console.WriteLine("\t1. Edit");
//                            Console.WriteLine("\t2. Delete");
//                            cmd = Console.ReadLine();
//                            Console.Clear();
//                            if(cmd.Equals("1"))
//                            {
//                                found=true;
//                                EditUser(id);
//                            }
//                            else if(cmd.Equals("2"))
//                            {
//                                found = true;
//                                itsUserBL.Delete(id);
//                                Console.WriteLine("\nUser deleted! Press enter to continue.\n");
//                                Console.ReadLine();
//                            }
//                            else{
//                                Console.WriteLine("\nInvalid input. Press enter to try again.");
//                                Console.ReadLine();
//                            }

//                        }
//                        else
//                        {
//                            Console.WriteLine("\nInvalid input. Press enter to try again.");
//                            Console.ReadLine();
//                        }
//                   }
//                }
//            }
//        }

//        private void AddUser()
//        {
//            while (true)
//            {
//                string cmd;
//                string[] temp = new string[2];
//                bool firstCondition = false;
//                while (!firstCondition)
//                {
//                    Console.Clear();
//                    Console.WriteLine();
//                    Console.WriteLine(String.Format("{0," + Console.WindowWidth / 2 + "}", "***Add User***"));
//                    Console.WriteLine();
//                    Console.WriteLine("\nEnter information of at least 3 chars in the following format, or press 1 to return:");
//                    Console.WriteLine("Username,Password");
//                    cmd = Console.ReadLine();
//                    Console.Clear();
//                    if (cmd.Equals("1"))
//                        return;
//                    temp = cmd.Split(',');
//                    if (temp.Length == 2 && IsValidString(temp[0]) && IsValidString(temp[1]))
//                    {
//                            firstCondition = true;
//                    }
//                    else
//                    {
//                        Console.WriteLine("\nInvalid input. Press enter to try again.");
//                        Console.ReadLine();
//                    }
//                }
//                String ans = itsUserBL.Add(temp[0], temp[1]);
//                if (ans.Length == 0)
//                {
//                    Console.WriteLine("\nUser added! Press enter to continue.");
//                    Console.ReadLine();
//                }
//                else
//                {
//                    Console.WriteLine("\n"+ans + "\nPress enter to try again");
//                    Console.ReadLine();
//                }
//            }
//        }

//        private void EditUser()
//        {
//            while (true)
//            {
//                Console.Clear();
//                Console.WriteLine();
//                Console.WriteLine(String.Format("{0," + Console.WindowWidth / 2 + "}", "***Edit User***"));
//                Console.WriteLine();
//                String cmd;
//                Console.WriteLine("Enter id to edit or press 0 to return:");
//                cmd = Console.ReadLine();
//                Console.Clear();
//                if (cmd.Equals("0"))
//                    return;
//                else
//                {
//                    int toEdit;
//                    if (int.TryParse(cmd, out toEdit))
//                    {
//                        if (!itsUserBL.Exist(toEdit))
//                        {
//                            Console.WriteLine("\nSuch user does not exist. Press enter to try again.");
//                            Console.ReadLine();
//                        }
//                        else
//                        {
//                            EditUser(toEdit);
//                        }
//                    }
//                    else
//                    {
//                        Console.WriteLine("\nInvalid input. Press enter to try again.\n");
//                        Console.ReadLine();
//                    }
//                }
//            }
//        }

//        private void EditUser(int toEdit)
//        {
//                            while (true)
//                            {
//                                string cmd;
//                                Console.Clear();
//                                Console.WriteLine("\n"+itsUserBL.SearchByID(toEdit));
//                                Console.WriteLine("\nSelect an option:");
//                                Console.WriteLine("\t1. Change Username");
//                                Console.WriteLine("\t2. Change Password");
//                                Console.WriteLine("\t3. Return");
//                                cmd = Console.ReadLine();
//                                Console.Clear();
//                                switch (cmd)
//                                {
//                                    case "3":
//                                        return;
//                                        break;

//                                    case "1":
//                                        Console.WriteLine("\nEnter new name:");
//                                        cmd = Console.ReadLine();
//                                        Console.Clear();
//                                        if (IsValidString(cmd))
//                                        {
//                                            String ans = itsUserBL.EditName(toEdit, cmd);
//                                            if (ans.Length == 0)
//                                            {
//                                                Console.WriteLine("\nChange accepted! Press enter to continue.");
//                                                Console.ReadLine();
//                                            }
//                                            else
//                                            {
//                                                Console.WriteLine("\n" + ans + "\nPress enter to try again.");
//                                                Console.ReadLine();
//                                            }
//                                        }
//                                        else
//                                        {
//                                            Console.WriteLine("\nUsername must have at least 3 letters or numbers. Press enter to try again.");
//                                            Console.ReadLine();
//                                        }
//                                        break;

//                                    case "2":
//                                        Console.WriteLine("\nEnter new password:");
//                                        cmd = Console.ReadLine();
//                                        Console.Clear();
//                                        if (IsValidString(cmd))
//                                        {
//                                            String ans = itsUserBL.EditPassword(toEdit, cmd);
//                                            if (ans.Length == 0)
//                                            {
//                                                Console.WriteLine("\nChange accepted! Press enter to continue.");
//                                                Console.ReadLine();
//                                            }
//                                            else
//                                            {
//                                                Console.WriteLine("\n" + ans + "\n Press enter to try again.");
//                                                Console.ReadLine();
//                                            }
//                                        }
//                                        else
//                                        {
//                                            Console.WriteLine("\nPassword must have at least 3 letters or numbers. Press enter to try again.");
//                                            Console.ReadLine();
//                                        }
//                                        break;

//                                    default:
//                                        {
//                                            Console.WriteLine("\nIvalid input. Press enter to try again.");
//                                            Console.ReadLine();
//                                        }
//                                        break;
//                                }
//                            }
//        }

//        private void DeleteUser()
//        {
//            while (true)
//            {
//                Console.Clear();
//                Console.WriteLine();
//                Console.WriteLine(String.Format("{0," + Console.WindowWidth / 2 + "}", "***Delete User***"));
//                Console.WriteLine();
//                String cmd;
//                Console.WriteLine("Enter id to delete or press 0 to return:");
//                cmd = Console.ReadLine();
//                Console.Clear();
//                if (cmd.Equals("0"))
//                    return;
//                else
//                {
//                    int toDelete;
//                    if (int.TryParse(cmd, out toDelete) && (itsUserBL.Delete(toDelete)))
//                    {
//                        Console.WriteLine("\nUser deleted! Press enter to continue.\n");
//                        Console.ReadLine();
//                    }
//                    else
//                    {
//                        Console.WriteLine("\nInvalid input. Press enter to try again.");
//                        Console.ReadLine();
//                    }
//                }
//            }
            
//        }

//        private void SeeAllUser()
//        {
//            Console.Clear();
//            Console.WriteLine();
//            Console.WriteLine(String.Format("{0," + Console.WindowWidth / 2 + "}", "***See All Users***"));
//            Console.WriteLine();
//            Console.WriteLine(itsUserBL.GetAll());
//            Console.WriteLine("\nPress enter to return.");
//            Console.ReadLine();
//            return;
//        }
        
//        private void ViewTransaction()
//        {
//            string cmd;
//            while (true)
//            {
//                Console.Clear(); //clear the screen 
//                Console.WriteLine();
//                Console.WriteLine(String.Format("{0," + Console.WindowWidth / 2 + "}", "***Transaction***"));
//                Console.WriteLine();

//                Console.WriteLine("Please select an option:");
//                Console.WriteLine("\t1. Add");
//                Console.WriteLine("\t2. Edit");
//                Console.WriteLine("\t3. Delete");
//                Console.WriteLine("\t4. Search");
//                Console.WriteLine("\t5. See all Transactions");
//                Console.WriteLine("\t6. Back to main menu");

//                cmd = Console.ReadLine();
//                Console.Clear(); //clear the screen 

//                switch (cmd)
//                {
//                    case "1":
//                        AddTransaction();
//                        break;

//                    case "2":
//                        EditTransaction();
//                        break;

//                    case "3":
//                        DeleteTransaction();
//                        break;

//                    case "4":
//                        SearchTransaction();
//                        break;

//                    case "5":
//                        SeeAllTransaction();
//                        break;

//                    case "6":
//                        Console.Clear();
//                        return;
//                        break;

//                    default:
//                        Console.WriteLine("\nThat was an invalid command. Press enter to continue.\n\n");
//                        Console.ReadLine();
//                        break;
//                }
//            }
//        }

//        private void SearchTransaction()
//        {
//            string cmd;
//            while (true)
//            {
//                Console.Clear();
//                Console.WriteLine();
//                Console.WriteLine(String.Format("{0," + Console.WindowWidth / 2 + "}", "***Searrch Transaction***"));
//                Console.WriteLine();

//                Console.WriteLine("Please select an option:");
//                Console.WriteLine("\t1. Search by ID");
//                Console.WriteLine("\t2. Search by Payment Method");
//                Console.WriteLine("\t3. Search by periods of dates");
//                Console.WriteLine("\t4. Back to 'Transaction's menu'");

//                cmd = Console.ReadLine();
//                Console.Clear();
//                bool found = false;
//                string ans = "";
//                    switch (cmd)
//                    {
//                        case "1":
//                            while (!found)
//                          {
//                              Console.Clear();
//                              Console.WriteLine("\nEnter ID to search or press 0 to return:");
//                              cmd = Console.ReadLine();
//                              Console.Clear();
//                              if (cmd.Equals("0"))
//                                  return;
//                              else
//                              {
//                                  int idSearch;
//                                  if (int.TryParse(cmd, out idSearch))
//                                  {
//                                      ans = itsTransactionBL.SearchByID(idSearch);
//                                      if (ans.Length == 0)
//                                      {
//                                          Console.WriteLine("\nSuch transaction does not exist! Press enter to try again.");
//                                          Console.ReadLine();
//                                      }
//                                      else
//                                      {
//                                          found = true;
//                                      }
//                                  }

//                                  else
//                                  {
//                                      Console.WriteLine("\nInvalid input. Press enter to try again.");
//                                      Console.ReadLine();
//                                  }
//                               }           
//                          }
//                          break;
//                            break;

//                        case "2":
//                            while (!found)
//                            {
//                                Console.Clear();
//                                Console.WriteLine("\nEnter payment method to search or press 0 to return:");
//                                cmd = Console.ReadLine();
//                                Console.Clear();
//                                if (cmd.Equals("0"))
//                                    return;
//                                else
//                                {
//                                    ans = itsTransactionBL.SearchByPaymentMethod(cmd);
//                                    if (ans.Length == 0)
//                                    {
//                                        Console.WriteLine("\nSuch transacation does not exist! Press enter to try again.");
//                                        Console.ReadLine();
//                                    }
//                                    else
//                                    {
//                                        found = true;
//                                    }
//                                }
//                            }
//                            break;

//                        case "3":
//                        while (!found)
//                        {
//                            Console.Clear();    
//                            DateTime date1 = DateTime.Now;
//                            DateTime date2 = DateTime.Now;
//                            Console.WriteLine("\nEnter dates to search betwwen in the following format or press 0 to return:");
//                            Console.WriteLine("d1/m1/y1,d2/m2/y2");
//                            cmd = Console.ReadLine();
//                            Console.Clear();
//                            if (cmd.Equals("0"))
//                                return;
//                            else
//                            {
//                              string[] temp = cmd.Split(',');
//                              bool firstCondition = false;
//                              bool secondCondition = false;
//                              if(temp.Length==2)
//                              {
//                                  try
//                                  {
//                                      date1 = DateTime.Parse(temp[0], new System.Globalization.CultureInfo("fr-FR", true), System.Globalization.DateTimeStyles.AssumeLocal);
//                                      firstCondition = true;
//                                  }
//                                  catch
//                                  { }
//                                  try
//                                  {
//                                      date2 = DateTime.Parse(temp[1], new System.Globalization.CultureInfo("fr-FR", true), System.Globalization.DateTimeStyles.AssumeLocal);
//                                      secondCondition = true;
//                                  }
//                                  catch
//                                  { }
//                                  if(firstCondition&&secondCondition)
//                                  {
//                                      ans = itsTransactionBL.SearchBetween(date1, date2);
//                                      if (ans.Length==0)
//                                      {
//                                          Console.WriteLine("\nSuch transaction does not exist! Press enter to try again.");
//                                          Console.ReadLine();
//                                      }
//                                      else
//                                      {
//                                          found = true;
//                                      }
//                                  }
//                                  else
//                                  {
//                                      Console.WriteLine("\nInvalid input. Press enter to try again.");
//                                      Console.ReadLine();
//                                  }
//                              }
//                              else
//                              {
//                                  Console.WriteLine("\nInvalid input. Press enter to try again.");
//                                  Console.ReadLine();
//                              }
//                          }
//                        }
//                        break;


//                        case "4":
//                            return;
//                            break;

//                        default:
//                            Console.WriteLine("\nInvalid input. Press enter to try again.");
//                            Console.ReadLine();
//                            break;
//                    }
//                    if (found)
//                    {
//                        found = false;
//                        while (!found)
//                        {
//                            Console.Clear();
//                            Console.WriteLine("\n" + ans);
//                            Console.WriteLine("\nChoose an ID to edit or delete, or press 0 to return:");
//                            cmd = Console.ReadLine();
//                            Console.Clear();
//                            if (cmd.Equals("0"))
//                                return;
//                            int id = 0;
//                            if (int.TryParse(cmd, out id) && itsTransactionBL.Exist(id))
//                            {
//                                Console.WriteLine("\nChoose an option:");
//                                Console.WriteLine("\t1. Edit");
//                                Console.WriteLine("\t2. Delete");
//                                cmd = Console.ReadLine();
//                                Console.Clear();
//                                if (cmd.Equals("1"))
//                                {
//                                    found = true;
//                                    EditTransaction(id);
//                                }
//                                else if (cmd.Equals("2"))
//                                {
//                                    found = true;
//                                    itsTransactionBL.Delete(id);
//                                    Console.WriteLine("\nTransaction deleted! Press enter to continue.\n");
//                                    Console.ReadLine();
//                                }
//                                else
//                                {
//                                    Console.WriteLine("\nInvalid input. Press enter to try again.");
//                                    Console.ReadLine();
//                                }

//                            }
//                            else
//                            {
//                                Console.WriteLine("\nInvalid input. Press enter to try again.");
//                                Console.ReadLine();
//                            }
//                        }
//                    }
                
//            }
//        }

//        private void AddTransaction()
//        {
//            while (true)
//            {
//                string cmd;
//                bool firstCondition = false;
//                bool secondCondition = false;
//                bool thirdCondition = false;
//                DateTime date = DateTime.Now;
//                bool is_a_return = false;
//                int count=0;
//                while (!firstCondition)
//                {
//                    Console.Clear();
//                    Console.WriteLine();
//                    Console.WriteLine(String.Format("{0," + Console.WindowWidth / 2 + "}", "***Add Transaction***"));
//                    Console.WriteLine();
//                    Console.WriteLine("Enter p for a purchase or r for a return. Press 1 to go back to Transaction menu.");
//                    cmd = Console.ReadLine();
//                    Console.Clear();
//                    if (cmd.Equals("1"))
//                        return;
//                    else if(cmd.Equals("p"))
//                        firstCondition = true;
//                    else if (cmd.Equals("r"))
//                    {
//                        firstCondition = true;
//                        is_a_return = true;
//                    }
//                    else
//                    {
//                        Console.WriteLine("\nInvalid input. Press enter to try again.\n");
//                        Console.ReadLine();
//                    }
//                }
//                while (!secondCondition)
//                {
//                    Console.Clear();
//                    Console.WriteLine("\nEnter date of transaction in the following format, or 'c' for current date. Press 1 to return:");
//                    Console.WriteLine("day/month/year");
//                    cmd = Console.ReadLine();
//                    Console.Clear();
//                    if (cmd.Equals("1"))
//                        return;
//                    if (cmd.Equals("c"))
//                        secondCondition = true;
//                    else try
//                    {
//                        date = DateTime.Parse(cmd, new System.Globalization.CultureInfo("fr-FR", true), System.Globalization.DateTimeStyles.AssumeLocal);
//                        secondCondition = true;
//                    }
//                    catch
//                    {
//                        Console.WriteLine("\nInvalid input. Press enter to try again.");
//                        Console.ReadLine();
//                    }
//                }
//                int id = itsTransactionBL.Add(is_a_return, date);
//                while (!thirdCondition)
//                {
//                    Console.Clear();
//                    Console.WriteLine("\nEnter ProductID,Amount or 0 to end transaction.");
//                    cmd = Console.ReadLine();
//                    Console.Clear();
//                    if (cmd.Equals("0"))
//                        thirdCondition = true;
//                    else
//                    {
//                        String[] temp = cmd.Split(',');
//                        int proId=0;
//                        int amount =0;
//                        if (temp.Length == 2 && int.TryParse(temp[0], out proId) && int.TryParse(temp[1], out amount) && amount > 0)
//                        {
//                            String comments = "";
//                            if (is_a_return)
//                                comments = itsTransactionBL.AddToReturn(id, proId, amount);
//                            else
//                                comments = itsTransactionBL.AddToPurchase(id, proId, amount);
//                            if (comments.Length == 0)
//                                count = count + 1;
//                            else
//                            {
//                                Console.WriteLine("\n" + comments + "\nPress enter to try again.");
//                                Console.ReadLine();
//                            }
//                        }
//                        else
//                        {
//                            Console.WriteLine("\nInvalid input. Press enter to try again.");
//                            Console.ReadLine();
//                        }
//                    }
//                }
//                if(count==0)
//                {
//                    Console.WriteLine("\nNo product has been chosen. Transaction will be deleted. Press enter to return.");
//                    itsTransactionBL.Delete(id);
//                    Console.ReadLine();
//                    return;
//                }
//                else
//                {
//                    Console.WriteLine("\n"+itsTransactionBL.GetReceiptAsString(id));
//                    Console.WriteLine("\nEnter 'Cash', 'Check' or 'Credit Card' as payment method:");
//                    cmd = Console.ReadLine();
//                    Console.Clear();
//                    while(!cmd.Equals("Credit Card") && !cmd.Equals("Cash") && !cmd.Equals("Check"))
//                    {
//                        Console.WriteLine("\nInvalid input. Please try again.\nEnter 'Cash', 'Check' or 'Credit Card' as payment method:");
//                        cmd = Console.ReadLine();
//                        Console.Clear();
//                    }
//                    itsTransactionBL.EditPaymentMethod(id, cmd);
//                    while (true)
//                    {
//                        Console.Clear();
//                        Console.WriteLine("\nTransaction added!\nEnter club member ID to attach, or 0 to return to Transaction menu.");
//                        cmd = Console.ReadLine();
//                        Console.Clear();
//                        if (cmd.Equals("0"))
//                            return;
//                        else
//                        {
//                            int cm;
//                            if (int.TryParse(cmd, out cm))
//                            {
//                                String str = itsClubMemberBL.AttachTransaction(cm, id);
//                                if (str.Length == 0)
//                                {
//                                    Console.WriteLine("\nTransaction attached! Press enter to return.");
//                                    Console.ReadLine();
//                                    return;
//                                }
//                                else
//                                {
//                                    Console.WriteLine("\n" + str + "\nPress enter to try again.");
//                                    Console.ReadLine();
//                                }
//                            }
//                            else
//                            {
//                                Console.WriteLine("\nInvalid input. Press enter to try again.");
//                                Console.ReadLine();
//                            }
//                        }
//                    }
//                }
//            }
//        }

//        private void EditTransaction()
//        {
//            while (true)
//            {
//                Console.Clear();
//                Console.WriteLine();
//                Console.WriteLine(String.Format("{0," + Console.WindowWidth / 2 + "}", "***Edit Transaction***"));
//                Console.WriteLine();
//                String cmd;
//                Console.WriteLine("Enter id to edit or press 0 to return:");
//                cmd = Console.ReadLine();
//                Console.Clear();
//                if (cmd.Equals("0"))
//                    return;
//                else
//                {
//                    int toEdit;
//                    if (int.TryParse(cmd, out toEdit))
//                    {
//                        if (!itsTransactionBL.Exist(toEdit))
//                        {
//                            Console.WriteLine("\nSuch transaction does not exist! Press enter to try again.");
//                            Console.ReadLine();
//                        }
//                        else
//                        {
//                            EditTransaction(toEdit);
//                        }
//                    }
//                    else
//                    {
//                        Console.WriteLine("\nInvalid input. Press enter to try again.");
//                        Console.ReadLine();
//                    }
//                }
//            }
//        }

//        private void EditTransaction(int toEdit)
//        {
//                while (true)
//                {
//                    string cmd;
//                    Console.Clear();
//                    Console.WriteLine("\n"+itsTransactionBL.SearchByID(toEdit));
//                    Console.WriteLine("\nSelect an option:");
//                    Console.WriteLine("\t1. Change Payment Method");
//                    Console.WriteLine("\t2. Change Date");
//                    Console.WriteLine("\t3. Add item to transaction");
//                    Console.WriteLine("\t4. Remove item from transaction");
//                    Console.WriteLine("\t5. Return");
//                    cmd = Console.ReadLine();
//                    Console.Clear();
//                    switch (cmd)
//                    {
//                        case "5":
//                            return;
//                            break;

//                        case "2":
//                            Console.WriteLine("\nEnter new Date in the format of dd/mm/yyyy or 'c' for cuurent date:");
//                            cmd = Console.ReadLine();
//                            Console.Clear();
//                            if (cmd.Equals("c"))
//                            {
//                                itsTransactionBL.EditDate(toEdit, DateTime.Now);
//                                Console.WriteLine("\nChange accepted! Press enter to continue.");
//                                Console.ReadLine();
//                            }
//                            else
//                            {
//                                try
//                                {
//                                    DateTime date = DateTime.Parse(cmd, new System.Globalization.CultureInfo("fr-FR", true), System.Globalization.DateTimeStyles.AssumeLocal);
//                                    String ans = itsTransactionBL.EditDate(toEdit, date);
//                                    if (ans.Length == 0)
//                                            {
//                                                Console.WriteLine("\nChange accepted! Press enter to continue.");
//                                                Console.ReadLine();
//                                            }
//                                            else
//                                            {
//                                                Console.WriteLine("\n" + ans + "\nPress enter to try again.");
//                                                Console.ReadLine();
//                                            }
//                                }
//                                catch
//                                {
//                                    Console.WriteLine("\nInvalid input. Press enter to try again.");
//                                    Console.ReadLine();
//                                }
//                            }
//                            break;

//                        case "1":
//                            Console.WriteLine("\nEnter new Payment Method ('Cash', 'Check', 'Credit Card'):");
//                            cmd = Console.ReadLine();
//                            Console.Clear();
//                            if (cmd.Equals("Cash") || cmd.Equals("Credit Card") || cmd.Equals("Check"))
//                            {
//                                String ans = itsTransactionBL.EditPaymentMethod(toEdit, cmd);
//                                if (ans.Length == 0)
//                                            {
//                                                Console.WriteLine("\nChange accepted! Press enter to continue.");
//                                                Console.ReadLine();
//                                            }
//                                            else
//                                            {
//                                                Console.WriteLine("\n" + ans + "\nPress enter to try again.");
//                                                Console.ReadLine();
//                                            }
//                            }
//                            else
//                                {
//                                    Console.WriteLine("\nInvalid input. Press enter to try again.");
//                                    Console.ReadLine();
//                                }
//                            break;

//                        case "3":
//                            Console.WriteLine("\nEnter ProductID,Amount to add to transaction.");
//                            cmd = Console.ReadLine();
//                            Console.Clear();
//                            String[] temp = cmd.Split(',');
//                            int proId = 0;
//                            int amount = 0;
//                            if (temp.Length == 2 && int.TryParse(temp[0], out proId) && int.TryParse(temp[1], out amount) && amount > 0)
//                            {
//                                String comments = "";
//                                if (itsTransactionBL.IsAReturn(toEdit))
//                                    comments = itsTransactionBL.AddToReturn(toEdit, proId, amount);
//                                else
//                                    comments = itsTransactionBL.AddToPurchase(toEdit, proId, amount);
//                                if (comments.Length == 0)
//                                    {
//                                            Console.WriteLine("\nChange accepted! Press enter to continue.");
//                                            Console.ReadLine();
//                                    }
//                                else
//                                    {
//                                                Console.WriteLine("\n" + comments + "\nPress enter to try again.");
//                                                Console.ReadLine();
//                                    }
//                            }
//                            else
//                                {
//                                    Console.WriteLine("\nInvalid input. Press enter to try again.");
//                                    Console.ReadLine();
//                                }
//                            break;

//                        case "4":
//                            Console.WriteLine("\nEnter ProductID,Amount to remove from transaction.");
//                            cmd = Console.ReadLine();
//                            Console.Clear();
//                            String[] temp1 = cmd.Split(',');
//                            int proId1 = 0;
//                            int amount1 = 0;
//                            if (temp1.Length == 2 && int.TryParse(temp1[0], out proId1) && int.TryParse(temp1[1], out amount1) && amount1 > 0)
//                            {
//                                String comments = "";
//                                if (itsTransactionBL.IsAReturn(toEdit))
//                                    comments = itsTransactionBL.RemoveFromReturn(toEdit, proId1, amount1);
//                                else
//                                    comments = itsTransactionBL.RemoveFromPurchase(toEdit, proId1, amount1);
//                                if (comments.Length == 0)
//                                    {
//                                            Console.WriteLine("\nChange accepted! Press enter to continue.");
//                                            Console.ReadLine();
//                                    }
//                                else
//                                    {
//                                                Console.WriteLine("\n" + comments + "\nPress enter to try again.");
//                                                Console.ReadLine();
//                                    }
//                            }
//                            else
//                                {
//                                    Console.WriteLine("\nInvalid input. Press enter to try again.");
//                                    Console.ReadLine();
//                                }
//                            break;

//                        default:
//                            {
//                                    Console.WriteLine("\nInvalid input. Press enter to try again.");
//                                    Console.ReadLine();
//                                }
//                            break;
//                    }               
//            }
//        }

//        private void DeleteTransaction()
//        {
//            while (true)
//            {
//                Console.Clear();
//                Console.WriteLine();
//                Console.WriteLine(String.Format("{0," + Console.WindowWidth / 2 + "}", "***Delete Transaction***"));
//                Console.WriteLine();
//                String cmd;
//                Console.WriteLine("*Note that all related information will be deleted.");
//                Console.WriteLine("Enter id to delete or press 0 to return:");
//                cmd = Console.ReadLine();
//                Console.Clear();
//                if (cmd.Equals("0"))
//                    return;
//                else
//                {
//                    int toDelete;
//                    if (int.TryParse(cmd, out toDelete) && (itsTransactionBL.Delete(toDelete)))
//                    {
//                        Console.WriteLine("\nTransaction deleted! Press enter to continue.");
//                        Console.ReadLine();
//                    }
//                    else
//                    {
//                        Console.WriteLine("\nInvalid input. Please try again.");
//                        Console.ReadLine();
//                    }
//                }
//            }
//        }

//        private void SeeAllTransaction()
//        {
//            Console.Clear();
//            Console.WriteLine();
//            Console.WriteLine(String.Format("{0," + Console.WindowWidth / 2 + "}", "***See All Transactions***"));
//            Console.WriteLine();
//            Console.WriteLine(itsTransactionBL.GetAll());
//            Console.WriteLine("\nPress enter key to return.");
//            Console.ReadLine();
//            return;
//        }
        
//        private void ViewProduct()
//        {
//            string cmd;
//            while(true)
//            {
//                Console.Clear();
//                Console.WriteLine();
//                Console.WriteLine(String.Format("{0," + Console.WindowWidth / 2 + "}", "***Product***"));
//                Console.WriteLine();

//                Console.WriteLine("Please select an option:");
//                Console.WriteLine("\t1. Add");
//                Console.WriteLine("\t2. Edit");
//                Console.WriteLine("\t3. Delete");
//                Console.WriteLine("\t4. Search");
//                Console.WriteLine("\t5. See all Products");
//                Console.WriteLine("\t6. Back to main menu");

//                cmd = Console.ReadLine();
//                Console.Clear();
//                switch (cmd)
//                {
//                    case "1":
//                        AddProduct();
//                        break;

//                    case "2":
//                        EditProduct();
//                        break;

//                    case "3":
//                        DeleteProduct();
//                        break;

//                    case "4":
//                        SearchProduct();
//                        break;

//                    case "5":
//                        SeeAllProduct();
//                        break;

//                    case "6":
//                        Console.Clear();
//                        return;
//                        break;

//                    default:
//                        Console.WriteLine("\nThat was an invalid command. Press enter to continue.\n\n");
//                        Console.ReadLine();
//                        break;
//                }
//            }
//        }

//        private void SearchProduct()
//        {
//            string cmd;
//            while (true)
//            {
//                Console.Clear();
//                Console.WriteLine();
//                Console.WriteLine(String.Format("{0," + Console.WindowWidth / 2 + "}", "***Search Product***"));
//                Console.WriteLine();

//                Console.WriteLine("Please select an option:");
//                Console.WriteLine("\t1. Search by ID");
//                Console.WriteLine("\t2. Search by Name");
//                Console.WriteLine("\t3. Search by Type");
//                Console.WriteLine("\t4. Search by Department ID");
//                Console.WriteLine("\t5. Back to 'Product's menu'");
//                String ans = "";
//                bool found = false;
//                cmd = Console.ReadLine();
//                Console.Clear();
//                    switch (cmd)
//                    {
//                        case "1":
//                            while (!found)
//                          {
//                              Console.Clear();
//                              Console.WriteLine("\nEnter ID to search or press 0 to return:");
//                              cmd = Console.ReadLine();
//                              Console.Clear();
//                              if (cmd.Equals("0"))
//                                  return;
//                              else
//                              {
//                                  int idSearch;
//                                  if (int.TryParse(cmd, out idSearch))
//                                  {
//                                      ans = itsProductBL.SearchByID(idSearch);
//                                      if (ans.Length == 0)
//                                      {
//                                          Console.WriteLine("\nSuch product does not exist! Press enter to try again.");
//                                          Console.ReadLine();
//                                      }
//                                      else
//                                      {
//                                          found = true;
//                                      }
//                                  }

//                                  else
//                                  {
//                                      Console.WriteLine("\nInvalid input. Press enter to try again.");
//                                      Console.ReadLine();
//                                  }
//                               }           
//                          }
//                            break;

//                        case "2":
//                            while (!found)
//                          {
//                              Console.Clear();
//                              Console.WriteLine("\nEnter name to search or press 0 to return:");
//                              cmd = Console.ReadLine();
//                              Console.Clear();
//                              if (cmd.Equals("0"))
//                                  return;
//                              else
//                              {
//                                  ans = itsProductBL.SearchByName(cmd);
//                                  if (ans.Length == 0)
//                                  {
//                                      Console.WriteLine("\nSuch product does not exist! Press enter to try again.");
//                                      Console.ReadLine();
//                                  }
//                                  else
//                                  {
//                                      found = true;
//                                  }
//                              }
//                          }
//                            break;

//                        case "3":
//                            while (!found)
//                          {
//                              Console.Clear();
//                              Console.WriteLine("\nEnter type to search or press 0 to return:");
//                              cmd = Console.ReadLine();
//                              Console.Clear();
//                              if (cmd.Equals("0"))
//                                  return;
//                              else
//                              {
//                                  ans = itsProductBL.SearchByType(cmd);
//                                  if (ans.Length == 0)
//                                  {
//                                      Console.WriteLine("\nSuch product does not exist! Press enter to try again.");
//                                      Console.ReadLine();
//                                  }
//                                  else
//                                  {
//                                      found = true;
//                                  }
//                              }
//                          }
//                            break;

//                        case "4":
//                            while (!found)
//                            {
//                                Console.Clear();
//                                Console.WriteLine("\nEnter department ID to search or press 0 to return:");
//                                cmd = Console.ReadLine();
//                                Console.Clear();
//                                if (cmd.Equals("0"))
//                                    return;
//                                else
//                                {
//                                    int idSearch;
//                                    if (int.TryParse(cmd, out idSearch))
//                                    {
//                                        ans = itsProductBL.SearchByDepartmentID(idSearch);
//                                        if (ans.Length == 0)
//                                        {
//                                            Console.WriteLine("\nSuch product does not exist! Press enter to try again.");
//                                            Console.ReadLine();
//                                        }
//                                        else
//                                        {
//                                            found = true;
//                                        }
//                                    }

//                                    else
//                                    {
//                                        Console.WriteLine("\nInvalid input. Press enter to try again.");
//                                        Console.ReadLine();
//                                    }
//                                }
//                            }
//                            break;
                        
//                        case "5":
//                            return;
//                            break;

//                        default:
//                            Console.WriteLine("\nInvalid input. Press enter to try again.");
//                            Console.ReadLine();
//                            break;
//                }
                
//                if (found)
//                {
//                    found = false;
//                    while (!found)
//                    {
//                        Console.Clear();
//                        Console.WriteLine("\n" + ans);
//                        Console.WriteLine("\nChoose an ID to edit or delete, or press 0 to return:");
//                        cmd = Console.ReadLine();
//                        Console.Clear();
//                        if (cmd.Equals("0"))
//                            return;
//                        int id = 0;
//                        if (int.TryParse(cmd, out id) && itsProductBL.Exist(id))
//                        {
//                            Console.WriteLine("\nChoose an option:");
//                            Console.WriteLine("\t1. Edit");
//                            Console.WriteLine("\t2. Delete");
//                            cmd = Console.ReadLine();
//                            Console.Clear();
//                            if (cmd.Equals("1"))
//                            {
//                                found = true;
//                                EditProduct(id);
//                            }
//                            else if (cmd.Equals("2"))
//                            {
//                                found = true;
//                                itsProductBL.Delete(id);
//                                Console.WriteLine("\nProduct deleted! Press enter to continue.\n");
//                                Console.ReadLine();
//                            }
//                            else
//                            {
//                                Console.WriteLine("\nInvalid input. Press enter to try again.");
//                                Console.ReadLine();
//                            }

//                        }
//                        else
//                        {
//                            Console.WriteLine("\nInvalid input. Press enter to try again.");
//                            Console.ReadLine();
//                        }
//                    }
//                }
//            }
//        }

//        private void AddProduct()
//        {
//            while (true)
//            {
//                string cmd;
//                string[] temp = new string[4];
//                bool condition = false;
//                int depID = 0;
//                int inStock = 0;
//                double price = 0.0;
//                while(!condition)
//                {
//                    Console.Clear();
//                    Console.WriteLine();
//                    Console.WriteLine(String.Format("{0," + Console.WindowWidth / 2 + "}", "***Add Product***"));
//                    Console.WriteLine();
//                    Console.WriteLine("*Note that you can only add new products here. To updte old product's amount in stock, see Edit Product.");
//                    Console.WriteLine("*Product type can be one of the followings: Food, Clothes, Electronics, Toys, Cosmetics, Other.");
//                    Console.WriteLine("Enter information in the following format, or press 1 to return:");
//                    Console.WriteLine("Name,Type,DepartmentD,AmountInStock,Price");
//                    cmd = Console.ReadLine();
//                    Console.Clear();
//                    if (cmd.Equals("1"))
//                        return;
//                    temp = cmd.Split(',');
//                    if (temp.Length == 5)
//                    {
//                        String comments = "";
//                        if (!IsValidString(temp[0])) comments = comments + "Product name must contain at least 3 letters or numbers.\n";
//                        if (!temp[1].Equals("Food") && !temp[1].Equals("Clothes") && !temp[1].Equals("Other")
//                            && !temp[1].Equals("Cosmetics") && !temp[1].Equals("Toys") && !temp[1].Equals("Electronics"))
//                            comments = comments + "Type can only be one of the followings: Food, Clothes, Electronics, Toys, Cosmetics, Other.\n";
//                        if (!int.TryParse(temp[2], out depID)) comments = comments + "Department ID must be a number.\n";
//                        if (!int.TryParse(temp[3], out inStock) || inStock < 0) comments = comments + "Amount must be bigger or equal to 0.\n";
//                        if (!double.TryParse(temp[4], out price) || price <= 0) comments = comments + "Price must be bigger than 0.\n";
//                        if (comments.Length == 0)
//                            condition = true;
//                        else
//                        {
//                            Console.WriteLine("\n" + comments + "\nPress enter to try again");
//                            Console.ReadLine();
//                        }
//                    }
//                    else
//                    {
//                        Console.WriteLine("\nInvalid input. Press enter to try again.");
//                        Console.ReadLine();
//                    }
//                }
//                String ans = itsProductBL.Add(temp[0], temp[1], depID, inStock, price);
//                if (ans.Length == 0)
//                {
//                    Console.WriteLine("\nProduct added! Press enter to continue.");
//                    Console.ReadLine();
//                }
//                else
//                {
//                    Console.WriteLine("\n"+ans + "\nPress enter to try again");
//                    Console.ReadLine();
//                }
//            }
//        }

//        private void EditProduct()
//        {
//            while (true)
//            {
//                Console.Clear();
//                Console.WriteLine();
//                Console.WriteLine(String.Format("{0," + Console.WindowWidth / 2 + "}", "***Edit Product***"));
//                Console.WriteLine();
//                String cmd;
//                Console.WriteLine("Enter id to edit or press 0 to return:");
//                cmd = Console.ReadLine();
//                Console.Clear(); 
//                if (cmd.Equals("0"))
//                    return;
//                else
//                {
//                    int toEdit;
//                    if (int.TryParse(cmd, out toEdit))
//                    {
//                        if (!itsProductBL.Exist(toEdit))
//                        {
//                            Console.WriteLine("\nSuch product does not exist! Press enter to try again.");
//                            Console.ReadLine();
//                        }
//                        else
//                        {
//                            EditProduct(toEdit);
//                        }
//                    }
//                    else
//                    {
//                        Console.WriteLine("\nInvalid input. Press enter to try again.");
//                        Console.ReadLine();
//                    }
//                }
//            }
//        }

//        private void EditProduct(int toEdit)
//        {
//            while (true)
//            {
//                Console.Clear();
//                Console.WriteLine("\n"+itsProductBL.SearchByID(toEdit));
//                string cmd;
//                Console.WriteLine("Select an option:");
//                Console.WriteLine("\t1. Change Name");
//                Console.WriteLine("\t2. Change Type");
//                Console.WriteLine("\t3. Change Department");
//                Console.WriteLine("\t4. Change Amount");
//                Console.WriteLine("\t5. Change Price");
//                Console.WriteLine("\t6. Back to Product menu");
//                cmd = Console.ReadLine();
//                Console.Clear();
//                switch (cmd)
//                {
//                    case "6":
//                        return;
//                        break;

//                    case "1":
//                        Console.WriteLine("\nEnter new name:");
//                        cmd = Console.ReadLine();
//                        Console.Clear();
//                        if (IsValidString(cmd))
//                        {
//                            String ans = itsProductBL.EditName(toEdit, cmd);
//                            if (ans.Length == 0)
//                            {
//                                Console.WriteLine("\nChange accepted! Press enter to continue.");
//                                Console.ReadLine();
//                            }
//                            else
//                            {
//                                Console.WriteLine("\n" + ans + "\nPress enter to try again.");
//                                Console.ReadLine();
//                            }
//                        }
//                        else
//                        {
//                            Console.WriteLine("\nProduct name must conatin at least 3 letters or numbers. Press enter to try again.\n");
//                            Console.ReadLine();
//                        }
//                        break;

//                    case "2":
//                        Console.WriteLine("\nEnter new type:");
//                        cmd = Console.ReadLine();
//                        Console.Clear();
//                        if (cmd.Equals("Food") || cmd.Equals("Clothes") || cmd.Equals("Other")
//                              || cmd.Equals("Cosmetics") || cmd.Equals("Toys") || cmd.Equals("Electronics"))
//                        {
//                            String ans = itsProductBL.EditType(toEdit, cmd);
//                            if (ans.Length == 0)
//                            {
//                                Console.WriteLine("\nChange accepted! Press enter to continue.");
//                                Console.ReadLine();
//                            }
//                            else
//                            {
//                                Console.WriteLine("\n" + ans + "\nPress enter to try again.");
//                                Console.ReadLine();
//                            }
//                        }
//                        else
//                        {
//                            Console.WriteLine("\nType can only be one of the followings: Food, Clothes, Electronics, Toys, Cosmetics, Other. Press enter to try again.");
//                            Console.ReadLine();
//                        }
//                        break;    

//                    case "4":
//                        Console.WriteLine("\nEnter new amount in stock:");
//                        cmd = Console.ReadLine();
//                        Console.Clear();
//                        int amount = 0;
//                        if (int.TryParse(cmd, out amount) && amount >= 0)
//                        {
//                            String ans = itsProductBL.EditAmount(toEdit, amount);
//                            if (ans.Length == 0)
//                            {
//                                Console.WriteLine("\nChange accepted! Press enter to continue.");
//                                Console.ReadLine();
//                            }
//                            else
//                            {
//                                Console.WriteLine("\n" + ans + "\nPress enter to try again.");
//                                Console.ReadLine();
//                            }
//                        }
//                        else
//                        {
//                            Console.WriteLine("\nAmount must be bigger or equal to 0. Press enter to try again.");
//                            Console.ReadLine();
//                        }
//                        break;

//                    case "5":
//                        Console.WriteLine("\nEnter new price:");
//                        cmd = Console.ReadLine();
//                        Console.Clear();
//                        double price = 0.0;
//                        if (double.TryParse(cmd, out price) && price > 0)
//                        {
//                            String ans = itsProductBL.EditPrice(toEdit, price);
//                            if (ans.Length == 0)
//                            {
//                                Console.WriteLine("\nChange accepted! Press enter to continue.");
//                                Console.ReadLine();
//                            }
//                            else
//                            {
//                                Console.WriteLine("\n" + ans + "\nPress enter to try again.");
//                                Console.ReadLine();
//                            }
//                        }
//                        else
//                        {
//                            Console.WriteLine("\nPrice must be a number bigger than 0. Press enter to try again.");
//                            Console.ReadLine();
//                        }
//                        break;

//                    case "3":
//                        Console.WriteLine("\nEnter new Department ID:");
//                        cmd = Console.ReadLine();
//                        Console.Clear();
//                        int dID = 0;
//                        if (int.TryParse(cmd, out dID))
//                        {
//                            String ans = itsProductBL.EditDepartment(toEdit, dID);
//                            if (ans.Length == 0)
//                             {
//                                Console.WriteLine("\nChange accepted! Press enter to continue.");
//                                Console.ReadLine();
//                            }
//                            else
//                            {
//                                Console.WriteLine("\n" + ans + "\nPress enter to try again.");
//                                Console.ReadLine();
//                            }
//                        }
//                        else
//                         {
//                            Console.WriteLine("\nDepartment ID must be a number. Press enter to try again.");
//                            Console.ReadLine();
//                        }
//                        break;

//                    default:
//                        {
//                            Console.WriteLine("\nInvalid input. Press enter to try again.");
//                            Console.ReadLine();
//                        }
//                        break;
//                }
//            }
//        }

//        private void DeleteProduct()
//        {
//            while (true)
//            {
//                Console.Clear();
//                Console.WriteLine();
//                Console.WriteLine(String.Format("{0," + Console.WindowWidth / 2 + "}", "***Delete Product***"));
//                Console.WriteLine();
//                String cmd;
//                Console.WriteLine("*Note that all related information will be deleted.");
//                Console.WriteLine("Enter id to delete or press 0 to return:");
//                cmd = Console.ReadLine();
//                Console.Clear();
//                if (cmd.Equals("0"))
//                    return;
//                else
//                {
//                    int toDelete;
//                    if (int.TryParse(cmd, out toDelete) && (itsProductBL.Delete(toDelete)))
//                    {
//                        Console.WriteLine("\nProduct deleted! Press enter to continue.");
//                        Console.ReadLine();
//                    }
//                    else
//                    {
//                        Console.WriteLine("\nInvalid input. Please try again.");
//                        Console.ReadLine();
//                    }
//                }
//            }
//        }

//        private void SeeAllProduct()
//        {
//            Console.Clear();
//            Console.WriteLine();
//            Console.WriteLine(String.Format("{0," + Console.WindowWidth / 2 + "}", "***See All Products***"));
//            Console.WriteLine();
//            Console.WriteLine(itsProductBL.GetAll());
//            Console.WriteLine("\nPress enter to return.");
//            Console.ReadLine();
//            return;
//        }
        
//        private void ViewEmployee()
//        {
//            string cmd;
//            while (true)
//            {
//                Console.Clear();
//                Console.WriteLine();
//                Console.WriteLine(String.Format("{0," + Console.WindowWidth / 2 + "}", "***Employee***"));
//                Console.WriteLine();

//                Console.WriteLine("Please select an option:");
//                Console.WriteLine("\t1. Add");
//                Console.WriteLine("\t2. Edit");
//                Console.WriteLine("\t3. Delete");
//                Console.WriteLine("\t4. Search");
//                Console.WriteLine("\t5. See all Employees");
//                Console.WriteLine("\t6. Back to main menu");

//                cmd = Console.ReadLine();
//                Console.Clear(); //clear the screen 

//                switch (cmd)
//                {
//                    case "1":
//                        AddEmployee();
//                        break;

//                    case "2":
//                        EditEmployee();
//                        break;

//                    case "3":
//                        DeleteEmployee();
//                        break;

//                    case "4":
//                        SearchEmployee();
//                        break;

//                    case "5":
//                        SeeAllEmployee();
//                        break;

//                    case "6":
//                        Console.Clear();
//                        return;
//                        break;

//                    default:
//                        Console.WriteLine("\nThat was an invalid command. Press enter to continue.\n\n");
//                        Console.ReadLine();
//                        break;
//                }
//            }
//        }

//        private void SearchEmployee()
//        {
//            string cmd;
//            while (true)
//            {
//                Console.Clear();
//                Console.WriteLine();
//                Console.WriteLine(String.Format("{0," + Console.WindowWidth / 2 + "}", "***Search Employee***"));
//                Console.WriteLine();

//                Console.WriteLine("Please select an option:");
//                Console.WriteLine("\t1. Search by ID");
//                Console.WriteLine("\t2. Search by First Name");
//                Console.WriteLine("\t3. Search by Last Name");
//                Console.WriteLine("\t4. Search by Gender");
//                Console.WriteLine("\t5. Search by Department ID");
//                Console.WriteLine("\t6. Back to 'Employee's menu'");
//                string ans = "";
//                bool found = false;
//                cmd = Console.ReadLine();
//                Console.Clear();

//                switch (cmd)
//                {
//                    case "2":
//                        while (!found)
//                        {
//                            Console.Clear();
//                            Console.WriteLine("\nEnter first name to search or press 0 to return:");
//                            cmd = Console.ReadLine();
//                            Console.Clear();
//                            if (cmd.Equals("0"))
//                                return;
//                            else
//                            {
//                                ans = itsEmployeeBL.SearchByFirstName(cmd);
//                                if (ans.Length == 0)
//                                {
//                                    Console.WriteLine("\nSuch employee does not exist! Press enter to try again.");
//                                    Console.ReadLine();
//                                }
//                                else
//                                {
//                                    found = true;
//                                }
//                            }
//                        }
//                        break;

//                    case "3":
//                        while (!found)
//                        {
//                            Console.Clear();
//                            Console.WriteLine("\nEnter last name to search or press 0 to return:");
//                            cmd = Console.ReadLine();
//                            Console.Clear();
//                            if (cmd.Equals("0"))
//                                return;
//                            else
//                            {
//                                ans = itsEmployeeBL.SearchByLastName(cmd);
//                                if (ans.Length == 0)
//                                {
//                                    Console.WriteLine("\nSuch employee does not exist! Press enter to try again.");
//                                    Console.ReadLine();
//                                }
//                                else
//                                {
//                                    found = true;
//                                }
//                            }
//                        }
//                        break;

//                    case "4":
//                        while (!found)
//                        {
//                            Console.Clear();
//                            Console.WriteLine("\nEnter gender to search or press 0 to return:");
//                            cmd = Console.ReadLine();
//                            Console.Clear();
//                            if (cmd.Equals("0"))
//                                return;
//                            else
//                            {
//                                ans = itsEmployeeBL.SearchByGender(cmd);
//                                if (ans.Length == 0)
//                                {
//                                    Console.WriteLine("\nSuch employee does not exist! Press enter to try again.");
//                                    Console.ReadLine();
//                                }
//                                else
//                                {
//                                    found = true;
//                                }
//                            }
//                        }
//                        break;

//                    case "1":
//                        while (!found)
//                        {
//                            Console.Clear();
//                            Console.WriteLine("\nEnter ID to search or press 0 to return:");
//                            cmd = Console.ReadLine();
//                            Console.Clear();
//                            if (cmd.Equals("0"))
//                                return;
//                            else
//                            {
//                                int idSearch;
//                                if (int.TryParse(cmd, out idSearch))
//                                {
//                                    ans = itsEmployeeBL.SearchByID(idSearch);
//                                    if (ans.Length == 0)
//                                    {
//                                        Console.WriteLine("\nSuch employee does not exist! Press enter to try again.");
//                                        Console.ReadLine();
//                                    }
//                                    else
//                                    {
//                                        found = true;
//                                    }
//                                }
//                                else
//                                {
//                                    Console.WriteLine("\nInvalid input. Press enter to try again.");
//                                    Console.ReadLine();
//                                }
//                            }
//                        }
//                        break;

//                    case "5":
//                        while (!found)
//                        {
//                            Console.Clear();
//                            Console.WriteLine("\nEnter department ID to search or press 0 to return:");
//                            cmd = Console.ReadLine();
//                            Console.Clear();
//                            if (cmd.Equals("0"))
//                                return;
//                            else
//                            {
//                                int idSearch;
//                                if (int.TryParse(cmd, out idSearch))
//                                {
//                                    ans = itsEmployeeBL.SearchByDepartmentID(idSearch);
//                                    if (ans.Length == 0)
//                                    {
//                                        Console.WriteLine("\nSuch employee does not exist! Press enter to try again.");
//                                        Console.ReadLine();
//                                    }
//                                    else
//                                    {
//                                        found = true;
//                                    }
//                                }
//                                else
//                                {
//                                    Console.WriteLine("\nInvalid input. Press enter to try again.");
//                                    Console.ReadLine();
//                                }
//                            }
//                        }
//                        break;

//                    case "6":
//                        return;
//                        break;

//                    default:
//                        Console.WriteLine("\nInvalid input. Press enter to try again.");
//                        Console.ReadLine();
//                        break;
//                }
//                if (found)
//                {
//                    found = false;
//                    while (!found)
//                    {
//                        Console.Clear();
//                        Console.WriteLine("\n" + ans);
//                        Console.WriteLine("\nChoose an ID to edit or delete, or press 0 to return:");
//                        cmd = Console.ReadLine();
//                        Console.Clear();
//                        if (cmd.Equals("0"))
//                            return;
//                        int id = 0;
//                        if (int.TryParse(cmd, out id) && itsEmployeeBL.Exist(id))
//                        {
//                            Console.WriteLine("\nChoose an option:");
//                            Console.WriteLine("\t1. Edit");
//                            Console.WriteLine("\t2. Delete");
//                            cmd = Console.ReadLine();
//                            Console.Clear();
//                            if (cmd.Equals("1"))
//                            {
//                                found = true;
//                                EditEmployee(id);
//                            }
//                            else if (cmd.Equals("2"))
//                            {
//                                found = true;
//                                itsEmployeeBL.Delete(id);
//                                Console.WriteLine("\nEmployee deleted! Press enter to continue.\n");
//                                Console.ReadLine();
//                            }
//                            else
//                            {
//                                Console.WriteLine("\nInvalid input. Press enter to try again.");
//                                Console.ReadLine();
//                            }

//                        }
//                        else
//                        {
//                            Console.WriteLine("\nInvalid input. Press enter to try again.");
//                            Console.ReadLine();
//                        }
//                    }
//                }
//            }
//        }

//        private void AddEmployee()
//        {
//            while (true)
//            {
//                string cmd;
//                string[] temp = new string[4];
//                bool firstCondition = false;
//                int depID = 0;
//                int salary = 0;
//                while (!firstCondition)
//                {
//                    Console.Clear();
//                    Console.WriteLine();
//                    Console.WriteLine(String.Format("{0," + Console.WindowWidth / 2 + "}", "***Add Employee***"));
//                    Console.WriteLine();
//                    Console.WriteLine("Enter information in the following format, or press 1 to return:");
//                    Console.WriteLine("ID,FirstName,LastName,Gender,DepartmentID,Salary,SupervisorID");
//                    cmd = Console.ReadLine();
//                    Console.Clear();
//                    if (cmd.Equals("1"))
//                        return;
//                    temp = cmd.Split(',');
//                    if (temp.Length == 7)
//                    {
//                        String comments = "";
//                        if (!IsID(temp[0]) || !IsID(temp[6])) comments = comments + "ID must contain 9 digits.\n";
//                        if (!IsName(temp[1]) || !IsName(temp[2])) comments = comments + "Name must contain only letters.\n";
//                        if (!temp[3].Equals("Male") && !temp[3].Equals("Female")) comments = comments + "Gender must be Male or Female.\n";
//                        if (!int.TryParse(temp[4], out depID)) comments = comments + "Department ID must be a number.\n";
//                        if (!int.TryParse(temp[5], out salary) || salary<=0) comments = comments + "Salary must be a positive number.\n";
//                        if (comments.Length == 0)
//                            firstCondition = true;
//                        else
//                        {
//                            Console.WriteLine("\n" + comments + "\nPress enter to try again.");
//                            Console.ReadLine();
//                        }
//                    }
//                    else
//                    {
//                        Console.WriteLine("\nInvalid input. Press enter to try again.");
//                        Console.ReadLine();
//                    }
//                }
//                String ans = itsEmployeeBL.Add(temp[0], temp[1], temp[2], depID, salary, temp[3], temp[6]);
//                if (ans.Length == 0)
//                {
//                    Console.WriteLine("\nEmployee added! Press enter to continue.");
//                    Console.ReadLine();
//                }
//                else
//                {
//                    Console.WriteLine("\n"+ans + "\nPress enter to try again");
//                    Console.ReadLine();
//                }
//            }
//        }

//        private void EditEmployee()
//        {
//            while (true)
//            {
//                Console.Clear();
//                Console.WriteLine();
//                Console.WriteLine(String.Format("{0," + Console.WindowWidth / 2 + "}", "***Edit Employee***"));
//                Console.WriteLine();
//                String cmd;
//                Console.WriteLine("Enter id to edit or press 0 to return:");
//                cmd = Console.ReadLine();
//                Console.Clear();
//                if (cmd.Equals("0"))
//                    return;
//                else
//                {
//                    int toEdit;
//                    if (int.TryParse(cmd, out toEdit))
//                    {
//                        if (!itsEmployeeBL.Exist(toEdit))
//                        {
//                            Console.WriteLine("\nSuch employee does not exist! Press enter to try again.");
//                            Console.ReadLine();
//                        }
//                        else
//                        {
//                            EditEmployee(toEdit);
//                        }
//                    }
//                    else
//                    {
//                        Console.WriteLine("\nInvalid input. Press enter to try again.");
//                        Console.ReadLine();
//                    }
//                }
//            }
//        }

//        private void EditEmployee(int toEdit)
//        {
//            while (true)
//            {
//                Console.Clear();
//                Console.WriteLine("\n"+itsEmployeeBL.SearchByID(toEdit));
//                string cmd;
//                Console.WriteLine("Select an option:");
//                Console.WriteLine("\t1. Change ID");
//                Console.WriteLine("\t2. Change First Name");
//                Console.WriteLine("\t3. Change Last Name");
//                Console.WriteLine("\t4. Change Gender");
//                Console.WriteLine("\t5. Change Salary");
//                Console.WriteLine("\t6. Change Supervisor ID");
//                Console.WriteLine("\t7. Change Department ID");
//                Console.WriteLine("\t8. Back to Employee menu");
//                cmd = Console.ReadLine();
//                Console.Clear();
//                switch (cmd)
//                {
//                    case "8":
//                        return;
//                        break;

//                    case "1":
//                        Console.WriteLine("\nEnter new ID:");
//                        cmd = Console.ReadLine();
//                        Console.Clear();
//                        if (IsID(cmd))
//                        {
//                            String ans = itsEmployeeBL.EditID(toEdit, cmd);
//                            if (ans.Length == 0)
//                            {
//                                Console.WriteLine("\nChange accepted! Press enter to continue.");
//                                Console.ReadLine();
//                            }
//                            else
//                            {
//                                Console.WriteLine("\n" + ans + "\nPress enter to try again.");
//                                Console.ReadLine();
//                            }
//                        }
//                        else
//                        {
//                            Console.WriteLine("\nID must contain 9 digits. Press enter to try again.");
//                            Console.ReadLine();
//                        }
//                        break;

//                    case "2":
//                        Console.WriteLine("\nEnter new First Name:");
//                        cmd = Console.ReadLine();
//                        Console.Clear();
//                        if (IsName(cmd))
//                        {
//                            String ans = itsEmployeeBL.EditFName(toEdit, cmd);
//                            if (ans.Length == 0)
//                            {
//                                Console.WriteLine("\nChange accepted! Press enter to continue.");
//                                Console.ReadLine();
//                            }
//                            else
//                            {
//                                Console.WriteLine("\n" + ans + "\nPress enter to try again.");
//                                Console.ReadLine();
//                            }
//                        }
//                        else
//                        {
//                            Console.WriteLine("\nName must conatain only letters. Press enter to try again.");
//                            Console.ReadLine();
//                        }
//                        break;

//                    case "3":
//                        Console.WriteLine("\nEnter new Last Name:");
//                        cmd = Console.ReadLine();
//                        Console.Clear();
//                        if (IsName(cmd))
//                        {
//                            String ans = itsEmployeeBL.EditLName(toEdit, cmd);
//                            if (ans.Length == 0)
//                            {
//                                Console.WriteLine("\nChange accepted! Press enter to continue.");
//                                Console.ReadLine();
//                            }
//                            else
//                            {
//                                Console.WriteLine("\n" + ans + "\nPress enter to try again.");
//                                Console.ReadLine();
//                            }
//                        }
//                        else
//                        {
//                            Console.WriteLine("\nName must conatain only letters. Press enter to try again.");
//                            Console.ReadLine();
//                        }
//                        break;

//                    case "4":
//                        Console.WriteLine("\nEnter new Gender:");
//                        cmd = Console.ReadLine();
//                        Console.Clear();
//                        if (cmd.Equals("Male") || cmd.Equals("Female"))
//                        {
//                            String ans = itsEmployeeBL.EditGender(toEdit, cmd);
//                            if (ans.Length == 0)
//                            {
//                                Console.WriteLine("\nChange accepted! Press enter to continue.");
//                                Console.ReadLine();
//                            }
//                            else
//                            {
//                                Console.WriteLine("\n" + ans + "\nPress enter to try again.");
//                                Console.ReadLine();
//                            }
//                        }
//                        else
//                        {
//                            Console.WriteLine("\nGender must be 'Male' or 'Female'. Press enter to try again.");
//                            Console.ReadLine();
//                        }
//                        break;

//                    case "5":
//                        Console.WriteLine("\nEnter new Salary:");
//                        cmd = Console.ReadLine();
//                        Console.Clear();
//                        int salary = 0;
//                        if (int.TryParse(cmd, out salary) && salary > 0)
//                        {
//                            String ans = itsEmployeeBL.EditSalary(toEdit, salary);
//                            if (ans.Length == 0)
//                            {
//                                Console.WriteLine("\nChange accepted! Press enter to continue.");
//                                Console.ReadLine();
//                            }
//                            else
//                            {
//                                Console.WriteLine("\n" + ans + "\nPress enter to try again.");
//                                Console.ReadLine();
//                            }
//                        }
//                        else
//                        {
//                            Console.WriteLine("\nSalary must be a number bigger than 0. Press enter to try again.");
//                            Console.ReadLine();
//                        }
//                        break;

//                    case "6":
//                        Console.WriteLine("\nEnter new Supervisor ID:");
//                        cmd = Console.ReadLine();
//                        Console.Clear();
//                        if (IsID(cmd))
//                        {
//                            String ans = itsEmployeeBL.EditSupervisor(toEdit, cmd);
//                            if (ans.Length == 0)
//                            {
//                                Console.WriteLine("\nChange accepted! Press enter to continue.");
//                                Console.ReadLine();
//                            }
//                            else
//                            {
//                                Console.WriteLine("\n" + ans + "\nPress enter to try again.");
//                                Console.ReadLine();
//                            }
//                        }
//                        else
//                        {
//                            Console.WriteLine("\nID must contain 9 digits. Press enter to try again.");
//                            Console.ReadLine();
//                        }
//                        break;

//                    case "7":
//                        Console.WriteLine("\nEnter new Department ID:");
//                        cmd = Console.ReadLine();
//                        Console.Clear();
//                        int dID = 0;
//                        if (int.TryParse(cmd, out dID))
//                        {
//                            String ans = itsEmployeeBL.EditDepartment(toEdit, dID);
//                            if (ans.Length == 0)
//                            {
//                                Console.WriteLine("\nChange accepted! Press enter to continue.");
//                                Console.ReadLine();
//                            }
//                            else
//                            {
//                                Console.WriteLine("\n" + ans + "\nPress enter to try again.");
//                                Console.ReadLine();
//                            }
//                        }
//                        else
//                        {
//                            Console.WriteLine("\nDepartment must be a number. Press enter to try again.");
//                            Console.ReadLine();
//                        }
//                        break;

//                    default:
//                        {
//                            Console.WriteLine("\nInvalid input. Press enter to try again.");
//                            Console.ReadLine();
//                        }
//                        break;
//                }
//            }
//        }

//        private void DeleteEmployee()
//        {
//            while (true)
//            {
//                Console.Clear();
//                Console.WriteLine();
//                Console.WriteLine(String.Format("{0," + Console.WindowWidth / 2 + "}", "***Delete Employee***"));
//                Console.WriteLine();
//                String cmd;
//                Console.WriteLine("*Note that all related information will be deleted.");
//                Console.WriteLine("Enter id to delete or press 0 to return:");
//                cmd = Console.ReadLine();
//                Console.Clear();
//                if (cmd.Equals("0"))
//                    return;
//                else
//                {
//                    int toDelete;
//                    if (int.TryParse(cmd, out toDelete) && (itsEmployeeBL.Delete(toDelete)))
//                    {
//                        Console.WriteLine("\nEmployee deleted! Press enter to continue.");
//                        Console.ReadLine();
//                    }
//                    else
//                    {
//                        Console.WriteLine("\nInvalid input. Please try again.");
//                        Console.ReadLine();
//                    }
//                }
//            }
//        }

//        private void SeeAllEmployee()
//        {
//            Console.Clear();
//            Console.WriteLine();
//            Console.WriteLine(String.Format("{0," + Console.WindowWidth / 2 + "}", "***See All Employees***"));
//            Console.WriteLine();
//            Console.WriteLine(itsEmployeeBL.GetAll());
//            Console.WriteLine("\nPress enter to return.");
//            Console.ReadLine();
//            return;
//        }
        
//        private void ViewDepartment()
//        {
//            string cmd;
//            while (true)
//            {
//                Console.Clear();
//                Console.WriteLine();
//                Console.WriteLine(String.Format("{0," + Console.WindowWidth / 2 + "}", "***Department***"));
//                Console.WriteLine();

//                Console.WriteLine("Please select an option:");
//                Console.WriteLine("\t1. Add");
//                Console.WriteLine("\t2. Edit");
//                Console.WriteLine("\t3. Delete");
//                Console.WriteLine("\t4. Search");
//                Console.WriteLine("\t5. See all Departments");
//                Console.WriteLine("\t6. Back to main menu");

//                cmd = Console.ReadLine();
//                Console.Clear(); //clear the screen 

//                switch (cmd)
//                {
//                    case "1":
//                        AddDepartment();
//                        break;

//                    case "2":
//                        EditDepartment();
//                        break;

//                    case "3":
//                        DeleteDepartment();
//                        break;

//                    case "4":
//                        SearchDepartment();
//                        break;

//                    case "5":
//                        SeeAllDepartment();
//                        break;

//                    case "6":
//                        Console.Clear();
//                        return;
//                        break;

//                    default:
//                        Console.WriteLine("\nThat was an invalid command. Press enter to continue.\n\n");
//                        Console.ReadLine();
//                        break;
//                }
//            }
//        }

//        private void SearchDepartment()
//        {
//            string cmd;
//            while (true)
//            {
//                Console.Clear();
//                Console.WriteLine();
//                Console.WriteLine(String.Format("{0," + Console.WindowWidth / 2 + "}", "***Search Department***"));
//                Console.WriteLine();

//                Console.WriteLine("Please select an option:");
//                Console.WriteLine("\t1. Search by ID");
//                Console.WriteLine("\t2. Search by Name");
//                Console.WriteLine("\t3. Back to 'Department's menu'");
//                string ans = "";
//                bool found = false;
//                cmd = Console.ReadLine();
//                Console.Clear();

//                switch (cmd)
//                {
//                    case "2":
//                        while (!found)
//                        {
//                            Console.Clear();
//                            Console.WriteLine("\nEnter name to search or press 0 to return:");
//                            cmd = Console.ReadLine();
//                            Console.Clear();
//                            if (cmd.Equals("0"))
//                                return;
//                            else
//                            {
//                                ans = itsDepartmentBL.SearchByName(cmd);
//                                if (ans.Length == 0)
//                                {
//                                    Console.WriteLine("\nSuch department does not exist! Press enter to try again.");
//                                    Console.ReadLine();
//                                }
//                                else
//                                {
//                                    found = true;
//                                }
//                            }
//                        }
//                        break;

//                    case "1":
//                        while (!found)
//                        {
//                            Console.Clear();
//                            Console.WriteLine("\nEnter ID to search or press 0 to return:");
//                            cmd = Console.ReadLine();
//                            Console.Clear();
//                            if (cmd.Equals("0"))
//                                return;
//                            else
//                            {
//                                int idSearch;
//                                if (int.TryParse(cmd, out idSearch))
//                                {
//                                    ans = itsDepartmentBL.SearchByID(idSearch);
//                                    if (ans.Length == 0)
//                                    {
//                                        Console.WriteLine("\nSuch department does not exist! Press enter to try again.");
//                                        Console.ReadLine();
//                                    }
//                                    else
//                                    {
//                                        found = true;
//                                    }
//                                }
//                                else
//                                {
//                                    Console.WriteLine("\nInvalid input. Press enter to try again.");
//                                    Console.ReadLine();
//                                }
//                            }
//                        }
//                        break;

//                    case "3":
//                        return;
//                        break;

//                    default:
//                        Console.WriteLine("\nInvalid input. Press enter to try again.");
//                        Console.ReadLine();
//                        break;
//                }
//                if (found)
//                {
//                    found = false;
//                    while (!found)
//                    {
//                        Console.Clear();
//                        Console.WriteLine("\n" + ans);
//                        Console.WriteLine("\nChoose an ID to edit or delete, or press 0 to return:");
//                        cmd = Console.ReadLine();
//                        Console.Clear();
//                        if (cmd.Equals("0"))
//                            return;
//                        int id = 0;
//                        if (int.TryParse(cmd, out id) && itsDepartmentBL.Exist(id))
//                        {
//                            Console.WriteLine("\nChoose an option:");
//                            Console.WriteLine("\t1. Edit");
//                            Console.WriteLine("\t2. Delete");
//                            cmd = Console.ReadLine();
//                            Console.Clear();
//                            if (cmd.Equals("1"))
//                            {
//                                found = true;
//                                EditDepartment(id);
//                            }
//                            else if (cmd.Equals("2"))
//                            {
//                                found = true;
//                                itsDepartmentBL.Delete(id);
//                                Console.WriteLine("\nDepartment deleted! Press enter to continue.\n");
//                                Console.ReadLine();
//                            }
//                            else
//                            {
//                                Console.WriteLine("\nInvalid input. Press enter to try again.");
//                                Console.ReadLine();
//                            }

//                        }
//                        else
//                        {
//                            Console.WriteLine("\nInvalid input. Press enter to try again.");
//                            Console.ReadLine();
//                        }
//                    }
//                }
//            }
//        }

//        private void AddDepartment()
//        {
//            while (true)
//            {
//                string cmd;
//                string name="";
//                while (name.Length == 0)
//                {
//                    Console.Clear();
//                    Console.WriteLine();
//                    Console.WriteLine(String.Format("{0," + Console.WindowWidth / 2 + "}", "***Add Department***"));
//                    Console.WriteLine("\nEnter name of at least 3 chars or press 1 to return:");
//                    cmd = Console.ReadLine();
//                    Console.Clear();
//                    if (cmd.Equals("1"))
//                        return;
//                    else
//                    {
//                        if (IsValidString(cmd))
//                            name = cmd;
//                        else
//                        {
//                            Console.WriteLine("\nDepartment name must have at least 3 letters or numbers. Press enter to try again.");
//                            Console.ReadLine();
//                        }
                            
//                    }                   
//                }
//                String ans = itsDepartmentBL.Add(name);
//                if (ans.Length == 0)
//                {
//                    Console.WriteLine("\nDepartment added! Press enter to continue.");
//                    Console.ReadLine();
//                }
//                else
//                {
//                    Console.WriteLine("\n"+ans + "\nPress enter to try again.");
//                    Console.ReadLine();
//                }
//            }
//        }

//        private void EditDepartment()
//        {
//            while (true)
//            {
//                Console.Clear();
//                Console.WriteLine();
//                Console.WriteLine(String.Format("{0," + Console.WindowWidth / 2 + "}", "***Edit Department***"));
//                Console.WriteLine();
//                String cmd;
//                Console.WriteLine("Enter id to edit or press 0 to return:");
//                cmd = Console.ReadLine();
//                Console.Clear();
//                if (cmd.Equals("0"))
//                    return;
//                else
//                {
//                    int toEdit;
//                    if (int.TryParse(cmd, out toEdit))
//                    {
//                        if (!itsDepartmentBL.Exist(toEdit))
//                        {
//                            Console.WriteLine("\nSuch department does not exist! Press enter to try again.");
//                            Console.ReadLine();
//                        }
//                        else
//                        {
//                            EditDepartment(toEdit);
//                        }
//                    }
//                    else
//                    {
//                        Console.WriteLine("\nInvalid input. Press enter to try again.");
//                        Console.ReadLine();
//                    }
//                }
//            }
//        }

//        private void EditDepartment(int toEdit)
//        {
//            while (true)
//            {
//                string cmd;
//                Console.Clear();
//                Console.WriteLine("\n"+itsDepartmentBL.SearchByID(toEdit));
//                Console.WriteLine("Select an option:");
//                Console.WriteLine("\t1. Change Name");
//                Console.WriteLine("\t2. Back to Department menu");
//                cmd = Console.ReadLine();
//                Console.Clear();
//                switch (cmd)
//                {
//                    case "2":
//                        return;
//                        break;

//                    case "1":
//                        Console.WriteLine("\nEnter new name:");
//                        cmd = Console.ReadLine();
//                        Console.Clear();
//                        if (IsValidString(cmd))
//                        {
//                            String ans = itsDepartmentBL.EditName(toEdit, cmd);
//                            if (ans.Length == 0)
//                            {
//                                Console.WriteLine("\nChange accepted! Press enter to continue.");
//                                Console.ReadLine();
//                            }
//                            else
//                            {
//                                Console.WriteLine("\n" + ans + "\nPress enter to try again.");
//                                Console.ReadLine();
//                            }
//                        }
//                        else
//                        {
//                            Console.WriteLine("\nDepartment name must have at least 3 letters or numbers. Press enter to try again.");
//                            Console.ReadLine();
//                        }

//                        break;

//                    default:
//                        {
//                            Console.WriteLine("\nInvalid input. Press enter to try again.");
//                            Console.ReadLine();
//                        }
//                        break;
//                }
//            }
//        }

//        private void DeleteDepartment()
//        {
//            while (true)
//            {
//                Console.Clear();
//                Console.WriteLine();
//                Console.WriteLine(String.Format("{0," + Console.WindowWidth / 2 + "}", "***Delete Department***"));
//                Console.WriteLine();
//                String cmd;
//                Console.WriteLine("*Note that all related information will be deleted.");
//                Console.WriteLine("Enter id to delete or press 0 to return:");
//                cmd = Console.ReadLine();
//                Console.Clear();
//                if (cmd.Equals("0"))
//                    return;
//                else
//                {
//                    int toDelete;
//                    if (int.TryParse(cmd, out toDelete) && (itsDepartmentBL.Delete(toDelete)))
//                    {
//                        Console.WriteLine("\nDepartment deleted! Press enter to continue.");
//                        Console.ReadLine();
//                    }
//                    else
//                    {
//                        Console.WriteLine("\nInvalid input. Please try again.");
//                        Console.ReadLine();
//                    }
//                }
//            }
//        }

//        private void SeeAllDepartment()
//        {
//            Console.Clear();
//            Console.WriteLine();
//            Console.WriteLine(String.Format("{0," + Console.WindowWidth / 2 + "}", "***See All Departments***"));
//            Console.WriteLine();
//            Console.WriteLine(itsDepartmentBL.GetAll());
//            Console.WriteLine("\nPress enter to return.");
//            Console.ReadLine();
//            return;
//        }

//        private void ViewClubMember()
//        {
//            string cmd;
//            while(true)
//            { 
//              Console.Clear();
//              Console.WriteLine();
//              Console.WriteLine(String.Format("{0," + Console.WindowWidth / 2 + "}", "***Club Member***"));
//              Console.WriteLine();

//              Console.WriteLine("Please select an option:");
//              Console.WriteLine("\t1. Add");
//              Console.WriteLine("\t2. Edit");
//              Console.WriteLine("\t3. Delete");
//              Console.WriteLine("\t4. Search");
//              Console.WriteLine("\t5. Attach transaction");
//              Console.WriteLine("\t6. Remove attached transaction");
//              Console.WriteLine("\t7. See all Club Members");
//              Console.WriteLine("\t8. Back to main menu");
 
//              cmd = Console.ReadLine();
//              Console.Clear(); //clear the screen 
  
//              switch (cmd)
//              {
//                  case "1":
//                      AddClubMember();
//                      break;

//                  case "2":
//                      EditClubMember();
//                      break;

//                  case "3":
//                      DeleteClubMember();
//                      break;

//                  case "4":
//                      SearchClubMember();
//                      break;

//                  case "5":
//                      AttachTransaction();
//                      break;

//                  case "6":
//                      RemoveAttachedTransaction();
//                      break;

//                  case "7":
//                      SeeAllClubMember();
//                      break;

//                  case "8":
//                      return;
//                      break;

//                  default:
//                      Console.WriteLine("\nThat was an invalid command. Press enter to continue.\n\n");
//                      Console.ReadLine();
//                      break;
//              }
//            }
//        }

//        private void SearchClubMember()
//        {
//            string cmd;
//            while (true)
//            {
//                Console.Clear();
//                Console.WriteLine();
//                Console.WriteLine(String.Format("{0," + Console.WindowWidth / 2 + "}", "***Search Club Member***"));
//                Console.WriteLine();

//                Console.WriteLine("Please select an option:");
//                Console.WriteLine("\t1. Search by ID");
//                Console.WriteLine("\t2. Search by First Name");
//                Console.WriteLine("\t3. Search by Last Name");
//                Console.WriteLine("\t4. Search by Gender");
//                Console.WriteLine("\t5. Back to 'Club Member menu'");
//                string ans = "";
//                bool found = false;
//                cmd = Console.ReadLine();
//                Console.Clear();

//                switch (cmd)
//                {
//                    case "2":
//                        while (!found)
//                        {
//                            Console.Clear();
//                            Console.WriteLine("\nEnter first name to search or press 0 to return:");
//                            cmd = Console.ReadLine();
//                            Console.Clear();
//                            if (cmd.Equals("0"))
//                                return;
//                            else
//                            {
//                                ans = itsClubMemberBL.SearchByFirstName(cmd);
//                                if (ans.Length == 0)
//                                {
//                                    Console.WriteLine("\nSuch club member does not exist! Press enter to try again.");
//                                    Console.ReadLine();
//                                }
//                                else
//                                {
//                                    found = true;
//                                }
//                            }
//                        }
//                        break;

//                    case "3":
//                        while (!found)
//                        {
//                            Console.Clear();
//                            Console.WriteLine("\nEnter last name to search or press 0 to return:");
//                            cmd = Console.ReadLine();
//                            Console.Clear();
//                            if (cmd.Equals("0"))
//                                return;
//                            else
//                            {
//                                ans = itsClubMemberBL.SearchByLastName(cmd);
//                                if (ans.Length == 0)
//                                {
//                                    Console.WriteLine("\nSuch club member does not exist! Press enter to try again.");
//                                    Console.ReadLine();
//                                }
//                                else
//                                {
//                                    found = true;
//                                }
//                            }
//                        }
//                        break;

//                    case "1":
//                        while (!found)
//                        {
//                            Console.Clear();
//                            Console.WriteLine("\nEnter ID to search or press 0 to return:");
//                            cmd = Console.ReadLine();
//                            Console.Clear();
//                            if (cmd.Equals("0"))
//                                return;
//                            else
//                            {
//                                int idSearch;
//                                if (int.TryParse(cmd, out idSearch))
//                                {
//                                    ans = itsClubMemberBL.SearchByID(idSearch);
//                                    if (ans.Length == 0)
//                                    {
//                                        Console.WriteLine("\nSuch club member does not exist! Press enter to try again.");
//                                        Console.ReadLine();
//                                    }
//                                    else
//                                    {
//                                        found = true;
//                                    }
//                                }
//                                else
//                                {
//                                    Console.WriteLine("\nInvalid input. Press enter to try again.");
//                                    Console.ReadLine();
//                                }
//                            }
//                        }
//                        break;

//                    case "4":
//                        while (!found)
//                        {
//                            Console.Clear();
//                            Console.WriteLine("\nEnter gender to search or press 0 to return:");
//                            cmd = Console.ReadLine();
//                            Console.Clear();
//                            if (cmd.Equals("0"))
//                                return;
//                            else
//                            {
//                                ans = itsClubMemberBL.SearchByGender(cmd);
//                                if (ans.Length == 0)
//                                {
//                                    Console.WriteLine("\nSuch club member does not exist! Press enter to try again.");
//                                    Console.ReadLine();
//                                }
//                                else
//                                {
//                                    found = true;
//                                }
//                            }
//                        }
//                        break;

//                    case "5":
//                        return;
//                        break;

//                    default:
//                        Console.WriteLine("\nInvalid input. Press enter to try again.");
//                        Console.ReadLine();
//                        break;
//                }
//                if (found)
//                {
//                    found = false;
//                    while (!found)
//                    {
//                        Console.Clear();
//                        Console.WriteLine("\n" + ans);
//                        Console.WriteLine("\nChoose an ID to edit or delete, or press 0 to return:");
//                        cmd = Console.ReadLine();
//                        Console.Clear();
//                        if (cmd.Equals("0"))
//                            return;
//                        int id = 0;
//                        if (int.TryParse(cmd, out id) && itsClubMemberBL.Exist(id))
//                        {
//                            Console.WriteLine("\nChoose an option:");
//                            Console.WriteLine("\t1. Edit");
//                            Console.WriteLine("\t2. Delete");
//                            cmd = Console.ReadLine();
//                            Console.Clear();
//                            if (cmd.Equals("1"))
//                            {
//                                found = true;
//                                EditClubMember(id);
//                            }
//                            else if (cmd.Equals("2"))
//                            {
//                                found = true;
//                                itsClubMemberBL.Delete(id);
//                                Console.WriteLine("\nClub member deleted! Press enter to continue.\n");
//                                Console.ReadLine();
//                            }
//                            else
//                            {
//                                Console.WriteLine("\nInvalid input. Press enter to try again.");
//                                Console.ReadLine();
//                            }

//                        }
//                        else
//                        {
//                            Console.WriteLine("\nInvalid input. Press enter to try again.");
//                            Console.ReadLine();
//                        }
//                    }
//                }
//            }
//        }

//        private void AddClubMember()
//        {
//           while (true)
//           {
//             string cmd;
//             string[] temp = new string[4];
//             bool firstCondition = false;
//             bool secondCondition = false;
//             DateTime birthday = DateTime.Now;
//             while (!firstCondition)
//             {
//                  Console.Clear();
//                  Console.WriteLine();
//                  Console.WriteLine(String.Format("{0," + Console.WindowWidth / 2 + "}", "***Add Club Member***"));
//                  Console.WriteLine();
//                 Console.WriteLine("Enter information in the following format, or press 1 to return:");
//                 Console.WriteLine("ID,FirstName,LastName,Gender");
//                 cmd = Console.ReadLine();
//                 Console.Clear();
//                 if(cmd.Equals("1"))
//                     return;
//                 temp = cmd.Split(',');
//                 if (temp.Length == 4)
//                 {
//                     String comments = "";
//                     if (!IsID(temp[0])) comments = comments + "ID must contain 9 digits.\n";
//                     if (!IsName(temp[1]) || !IsName(temp[2])) comments = comments + "Name must contain only letters.\n";
//                     if (!temp[3].Equals("Male") && !temp[3].Equals("Female")) comments = comments + "Gender must be Male or Female.\n";
//                     if (comments.Length == 0)
//                         firstCondition = true;
//                     else
//                     {
//                         Console.WriteLine("\n" + comments + "\nPress enter to try again.");
//                         Console.ReadLine();
//                     }
//                 }
//                 else
//                 {
//                     Console.WriteLine("\nInvalid input. Press enter to try again.");
//                     Console.ReadLine();
//                 }
//             }
//            while(!secondCondition)
//            {
//                Console.Clear();
//                Console.WriteLine("\nEnter birthday in the following format, or press 1 to return:");
//                Console.WriteLine("day/month/year");
//                cmd = Console.ReadLine();
//                Console.Clear();
//                if (cmd.Equals("1"))
//                    return;
//                try
//                {
//                    birthday = DateTime.Parse(cmd, new System.Globalization.CultureInfo("fr-FR", true), System.Globalization.DateTimeStyles.AssumeLocal);
//                    if(birthday<DateTime.Today)
//                    secondCondition = true;
//                    else
//                    {
//                        Console.WriteLine("\nIllegal birthday. Press enter to try again.");
//                        Console.ReadLine();
//                    }
//                }
//                catch
//                {
//                    Console.WriteLine("\nInvalid input. Press enter to try again.");
//                    Console.ReadLine();
//                }
//            }
//            String ans = itsClubMemberBL.Add(temp[0], temp[1],temp[2],temp[3],birthday);
//            if (ans.Length == 0)
//            {
//                Console.WriteLine("\nClub member added! Press enter to continue");
//                Console.ReadLine();
//            }
//            else
//            {
//                Console.WriteLine("\n"+ ans +"\nPress enter to try again");
//                Console.ReadLine();
//            }              
//          }
//        }
        
//        private bool IsName(string s)
//        {
//            foreach(char c in s)
//            {
//                if(!Char.IsLetter(c) && c!=' ')
//                {
//                    return false;
//                }
//            }
//            return s.Length > 0;
//        }

//        private bool IsValidString(string s)
//        {
//            int count =0;
//            foreach (char c in s)
//            {
//                if (Char.IsLetter(c) || Char.IsNumber(c))
//                {
//                    count = count + 1;
//                }

//                if (c == ',')
//                    return false;
//            }
//            return count >= 3;
//        }

//        private bool IsID(string s)
//        {
//            foreach (char c in s)
//            {
//                if (!Char.IsDigit(c))
//                {
//                    return false;
//                }
//            }
//            return s.Length == 9;
//        }

//        private void EditClubMember()
//        {
//            while (true)
//            {
//                Console.Clear();
//                Console.WriteLine();
//                Console.WriteLine(String.Format("{0," + Console.WindowWidth / 2 + "}", "***Edit Club Member***"));
//                Console.WriteLine();
//                String cmd;
//                Console.WriteLine("Enter id to edit or press 0 to return:");
//                cmd = Console.ReadLine();
//                Console.Clear();
//                if (cmd.Equals("0"))
//                    return;
//                else
//                {
//                    int toEdit;
//                    if (int.TryParse(cmd, out toEdit))
//                    {
//                        if (!itsClubMemberBL.Exist(toEdit))
//                        {
//                            Console.WriteLine("\nSuch club member does not exist! Press enter to try again.");
//                            Console.ReadLine();
//                        }
//                        else
//                        {
//                            EditClubMember(toEdit);
//                        }
//                    }
//                    else
//                    {
//                        Console.WriteLine("\nInvalid input. Press enter to try again.");
//                        Console.ReadLine();
//                    }
//                }
//            }
//        }

//        private void EditClubMember(int toEdit)
//        {
//            while (true)
//            {
//                Console.Clear();
//                string cmd;
//                Console.WriteLine("\n"+itsClubMemberBL.SearchByID(toEdit));
//                Console.WriteLine("Select an option:");
//                Console.WriteLine("\t1. Change ID");
//                Console.WriteLine("\t2. Change First Name");
//                Console.WriteLine("\t3. Change Last Name");
//                Console.WriteLine("\t4. Change Gender");
//                Console.WriteLine("\t5. Change Birthday");
//                Console.WriteLine("\t6. Back to Club Member menu");
//                cmd = Console.ReadLine();
//                Console.Clear();
//                switch (cmd)
//                {
//                    case "6":
//                        return;
//                        break;

//                    case "1":
//                        Console.WriteLine("\nEnter new ID:");
//                        cmd = Console.ReadLine();
//                        Console.Clear();
//                        if (IsID(cmd))
//                        {
//                            String ans = itsClubMemberBL.EditID(toEdit, cmd);
//                            if (ans.Length == 0)
//                            {
//                                Console.WriteLine("\nChange accepted! Press enter to continue.");
//                                Console.ReadLine();
//                            }
//                            else
//                            {
//                                Console.WriteLine("\n"+ans + "\nPress enter to try again.");
//                                Console.ReadLine();
//                            }
//                        }
//                        else
//                        {
//                            Console.WriteLine("\nID must contain 9 digits. Press enter to try again.");
//                            Console.ReadLine();
//                        }

//                        break;

//                    case "2":
//                        Console.WriteLine("\nEnter new First Name:");
//                        cmd = Console.ReadLine();
//                        Console.ReadLine();
//                        if (IsName(cmd))
//                        {
//                            String ans = itsClubMemberBL.EditFName(toEdit, cmd);
//                            if (ans.Length == 0)
//                            {
//                                Console.WriteLine("\nChange accepted! Press enter to continue.");
//                                Console.ReadLine();
//                            }
//                            else
//                            {
//                                Console.WriteLine("\n"+ans+ "\nPress enter to try again.");
//                                Console.ReadLine();
//                            }
//                        }
//                        else
//                        {
//                            Console.WriteLine("\nName must conatain only letters. Press enter to try again.");
//                            Console.ReadLine();
//                        }
//                        break;

//                    case "3":
//                        Console.WriteLine("\nEnter new Last Name:");
//                        cmd = Console.ReadLine();
//                        Console.ReadLine();
//                        if (IsName(cmd))
//                        {
//                            String ans = itsClubMemberBL.EditLName(toEdit, cmd);
//                            if (ans.Length == 0)
//                            {
//                                Console.WriteLine("\nChange accepted! Press enter to continue.");
//                                Console.ReadLine();
//                            }
//                            else
//                            {
//                                Console.WriteLine("\n"+ans+ "\nPress enter to try again.");
//                                Console.ReadLine();
//                            }
//                        }
//                        else
//                        {
//                            Console.WriteLine("\nName must conatain only letters. Press enter to try again.");
//                            Console.ReadLine();
//                        }
//                        break;

//                    case "4":
//                        Console.WriteLine("\nEnter new Gender:");
//                        cmd = Console.ReadLine();
//                        Console.Clear();
//                        if (cmd.Equals("Male") || cmd.Equals("Female"))
//                        {
//                            String ans = itsClubMemberBL.EditGender(toEdit, cmd);
//                            if (ans.Length == 0)
//                            {
//                                Console.WriteLine("\nChange accepted! Press enter to continue.");
//                                Console.ReadLine();
//                            }
//                            else
//                            {
//                                Console.WriteLine("\n" + ans + "\nPress enter to try again.");
//                                Console.ReadLine();
//                            }
//                        }
//                        else
//                        {
//                            Console.WriteLine("\nGender must be 'Male' or 'Female'. Press enter to try again.");
//                            Console.ReadLine();
//                        }
//                        break;

//                    case "5":
//                        Console.WriteLine("\nEnter new birthday in the format of dd/mm/yyyy:");
//                            cmd = Console.ReadLine();
//                            Console.Clear();
//                                try
//                                {
//                                    DateTime date = DateTime.Parse(cmd, new System.Globalization.CultureInfo("fr-FR", true), System.Globalization.DateTimeStyles.AssumeLocal);
//                                    if (date < DateTime.Today)
//                                    {
//                                        String ans = itsClubMemberBL.EditBirthday(toEdit, date);
//                                        if (ans.Length == 0)
//                                        {
//                                            Console.WriteLine("\nChange accepted! Press enter to continue.");
//                                            Console.ReadLine();
//                                        }
//                                        else
//                                        {
//                                            Console.WriteLine("\n" + ans + "\nPress enter to try again.");
//                                            Console.ReadLine();
//                                        }
//                                    }
//                                    else
//                                    {
//                                        Console.WriteLine("\nIllegal birthday. Press enter to try again.");
//                                        Console.ReadLine();
//                                    }
//                                }
//                                catch
//                                {
//                                    Console.WriteLine("\nInvalid input. Press enter to try again.");
//                                    Console.ReadLine();
//                                }
//                        break;

//                    default:
//                        {
//                            Console.WriteLine("\nInvalid input. Press enter to try again.");
//                            Console.ReadLine();
//                        }
//                        break;
//                }
//            }
//        }

//        private void DeleteClubMember()
//        {
//            while (true)
//            {
//                Console.Clear();
//                Console.WriteLine();
//                Console.WriteLine(String.Format("{0," + Console.WindowWidth / 2 + "}", "***Delete Club Member***"));
//                Console.WriteLine();
//                String cmd;
//                Console.WriteLine("*Note that all related information will be deleted.");
//                Console.WriteLine("Enter id to delete or press 0 to return:");
//                cmd = Console.ReadLine();
//                Console.Clear();
//                if (cmd.Equals("0"))
//                    return;
//                else
//                {
//                    int toDelete;
//                    if (int.TryParse(cmd, out toDelete) && (itsClubMemberBL.Delete(toDelete)))
//                    {
//                        Console.WriteLine("\nClub member deleted! Press enter to continue.");
//                        Console.ReadLine();
//                    }
//                    else
//                    {
//                        Console.WriteLine("\nInvalid input. Please try again.");
//                        Console.ReadLine();
//                    }
//                }
//            }
//        }

//        private void AttachTransaction()
//        {
//            while (true)
//            {
//                Console.Clear();
//                Console.WriteLine();
//                Console.WriteLine(String.Format("{0," + Console.WindowWidth / 2 + "}", "***Attach Transaction To Club Member***"));
//                Console.WriteLine();
//                String cmd;
//                Console.WriteLine("Enter club member ID and transaction ID in the following format or press 0 to return:");
//                Console.WriteLine("ClubMemberID,TransactionID");
//                cmd = Console.ReadLine();
//                Console.Clear();
//                if (cmd.Equals("0"))
//                    return;
//                else
//                {
//                    String[] input = cmd.Split(',');
//                    int cm,tran;
//                    if (input.Length == 2 && int.TryParse(input[0], out cm) && int.TryParse(input[1], out tran))
//                    {
//                        String str = itsClubMemberBL.AttachTransaction(cm,tran);
//                        if (str.Length == 0)
//                        {
//                            Console.WriteLine("\nTransaction attached! Press enter to continue.");
//                            Console.ReadLine();
//                        }
//                        else
//                        {
//                            Console.WriteLine("\n"+str+"\nPress enter to try again.");
//                            Console.ReadLine();
//                        }
//                    }
//                    else
//                    {
//                        Console.WriteLine("\nInvalid input. Press enter to try again.");
//                        Console.ReadLine();
//                    }
//                }
//            }
//        }

//        private void RemoveAttachedTransaction()
//        {
//            while (true)
//            {
//                Console.Clear();
//                Console.WriteLine();
//                Console.WriteLine(String.Format("{0," + Console.WindowWidth / 2 + "}", "***Remove Attached Transaction From Club Member***"));
//                Console.WriteLine();
//                String cmd;
//                Console.WriteLine("Enter club member ID and transaction ID in the following format or press 0 to return:");
//                Console.WriteLine("ClubMemberID,TransactionID");
//                cmd = Console.ReadLine();
//                Console.Clear();
//                if (cmd.Equals("0"))
//                    return;
//                else
//                {
//                    String[] input = cmd.Split(',');
//                    int cm, tran;
//                    if (input.Length == 2 && int.TryParse(input[0], out cm) && int.TryParse(input[1], out tran))
//                    {
//                        String str = itsClubMemberBL.RemoveAttachedTransaction(cm, tran);
//                        if (str.Length == 0)
//                        {
//                            Console.WriteLine("\nTransaction removed from member's history! Press enter to continue.");
//                            Console.ReadLine();
//                        }
//                        else
//                        {
//                            Console.WriteLine("\n" + str + "\nPress enter to try again.");
//                            Console.ReadLine();
//                        }
//                    }
//                    else
//                    {
//                        Console.WriteLine("\nInvalid input. Press enter to try again.");
//                        Console.ReadLine();
//                    }
//                }
//            }
//        }

//        private void SeeAllClubMember()
//        {
//            Console.Clear();
//            Console.WriteLine();
//            Console.WriteLine(String.Format("{0," + Console.WindowWidth / 2 + "}", "***See All Club Members***"));
//            Console.WriteLine();
//            Console.WriteLine(itsClubMemberBL.GetAll());
//            Console.WriteLine("\nPress enter to return.");
//            Console.ReadLine();
//            return;
//        }

//        public void Run()
//        {
//          string cmd;
//          while (true) 
//          {
//              Console.Clear();
//              Console.WriteLine();
//              Console.WriteLine(String.Format("{0," + Console.WindowWidth / 2 + "}", "***WELCOME TO E-MART!***"));
//              Console.WriteLine();
//              Console.WriteLine("Please select an option:"); 
//              Console.WriteLine("\t1. Login"); 
//              Console.WriteLine("\t2. Sign Up"); 
//              Console.WriteLine("\t3. Exit");

//              cmd = Console.ReadLine(); 
//              Console.Clear(); //clear the screen 

//              switch (cmd)
//              {
//                  case "1":
//                      login();
//                      break;
                  
//                  case "2": 
//                      SignUp();
//                      break; 
                  
//                  case "3":
//                       return;
                  
//                  default: 
//                      Console.WriteLine("\nThat was an invalid command. Press enter to continue.\n\n"); 
//                      Console.ReadLine();
//                      break;
//                 }
//              }
//        }
//    }
//}